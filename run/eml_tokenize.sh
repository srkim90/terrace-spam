#!/bin/bash

EXEC_PATH="../src/mail_parsor"
PYTHON_BIN="/usr/bin/env python"
PYTHON_TOOL="do_eml_tokenize.py"

cd ${EXEC_PATH}
${PYTHON_BIN} ${PYTHON_TOOL} $@
