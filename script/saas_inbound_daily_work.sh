#!/bin/bash

#1. run python script for qs --> json
YYYYMMDD=`date "+%Y%m%d" --date '1 days ago'`
cd /home/mailadm/terrace-spam/src/synap_helper
echo "1. run python script for qs --> json"
python parser_attachment.py ${YYYYMMDD}


#2. aging old reports
AGING_DAY=30
YYYYMMDD=`date "+%Y%m%d" --date "${AGING_DAY} days ago"`
RESULT_BASE="/home/mailadm/data/report"
REMOVE_TARGET="${RESULT_BASE}/${YYYYMMDD}"

echo "2. aging old report (${AGING_DAY} ago)"
if [ -d $REMOVE_TARGET ]; then
    rm -rf ${REMOVE_TARGET}
else
    echo "not exist aging target : ${REMOVE_TARGET}"
fi
