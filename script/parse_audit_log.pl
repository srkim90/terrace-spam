#!/opt/TerraceWatcher/3rd/perl/bin/perl -w -I ./ -I /opt/TerraceWatcher/libexec/lib-perl

use Getopt::Std;
getopts('d:');

$date = $opt_d if defined $opt_d;
if(! defined $date){
	print "useage : ./parse_audit_log.pl -d yyyymmdd\n";
	exit;
}

if($date !~ /^(2020)[0-9]{4}$/){
	print "date format is not yyyymmdd : $date\n";
	exit;
}

my $mta_log = "/tmwdata/log/wmtad";

my $logfile = sprintf "%s/%s.log", $mta_log, $date;
my $gzlogfile = sprintf "%s.gz", $logfile;
if( -e $gzlogfile) {
	system("gzip -d $logfile");
}

if(! -e $logfile) {
	print "log file is not exsit: $logfile\n";
	exit;
}


my %audit_hash;
my %msg_hash;

parse();

foreach my $k (keys %audit_hash) {
	if(defined $msg_hash{$k}){
		print "RESULT: $audit_hash{$k} $msg_hash{$k}\n";
	}
}

sub parse {
	open FD, $logfile;
	while(<FD>){
		my $line = $_;
		chomp $line;
		if($line =~ /MC:audit/) {
			my @args = split /\s+/, $line;
			my %msg_data = log2hash(\@args);
			my $sid = $msg_data{sid};

			$audit_hash{$sid} = $msg_data{qfile};

		} elsif($line =~ /SUCCESS MC:/) {
			my @args = split /\s+/, $line;
                	my %msg_data = log2hash(\@args);
                	my $sid = $msg_data{sid};
                	my $mc = $msg_data{mc};
                	my $rn;
                	if(defined $msg_data{rn}){
                        	$rn = $msg_data{rn};
                	}else{
                        	$rn = "normal";
                	}
			my $str = sprintf "%s %s", $mc, $rn;
			$msg_hash{$sid} = $str;
		}

	
	}
}
sub log2hash
{
	my ($r_args) = @_;
	my @args = @$r_args;
	my %msg;
	my %lh;

	my $key;
	my $val;
	for(my $i = 0; $i < $#args + 1; $i++) {
		if($args[$i] =~ /SID:/ || $args[$i] =~ /QF:/ || $args[$i] =~ /MC:/ || $args[$i] =~ /RN:/) {
			($key, $val) = split /:/, $args[$i], 2;
			$lh{$key} = $val;
		}
	}
	$msg{mc} = $lh{MC};
	$msg{sid} = $lh{SID};
	$msg{rn} = $lh{RN};
	$msg{qfile} = $lh{QF};
	
	return %msg;

}
