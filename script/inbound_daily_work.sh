#!/bin/bash

function help()
{
    echo run as root
    echo /maildata/eml_analazer/base/terrace-spam/script/inbound_daily_work.sh 4 days ago
    echo /maildata/eml_analazer/base/terrace-spam/script/inbound_daily_work.sh 20200410
}

function main()
{
    is_yyyymmdd=`echo $1 | grep 202 | wc -l`
    if [ "$is_yyyymmdd" == 1 ]; then
        YYYYMMDD=$1
        echo YYYYMMDD:${YYYYMMDD}
    elif [ "$1" != "" ]; then
        DAY_AGO="$1 $2 $3 $4 $5"
        echo  ${DAY_AGO}
        YYYYMMDD=`date "+%Y%m%d" --date "${DAY_AGO}"`
    else
        DAY_AGO="2 days ago"
        echo  ${DAY_AGO}
        YYYYMMDD=`date "+%Y%m%d" --date "${DAY_AGO}"`
    fi

    source ~/__bash_profile
    if [ "${PYTHON_INSTALL_PATH}" == "" ]; then
        echo Not exist environment variable : PYTHON_INSTALL_PATH
        return
    fi
    cd  ${PYTHON_INSTALL_PATH}/terrace-spam/src/mail_parsor
    python do_eml_tokenize.py --log ${YYYYMMDD}

    scp -r /maildata/eml_analazer/result/parsed_mails/terracehamadm/${YYYYMMDD} mailadm@172.22.1.138:/home/mailadm/data/parsed_mails/inbound.daou/terracehamadm/.
    scp -r /maildata/eml_analazer/result/parsed_mails/terracespamadm/${YYYYMMDD} mailadm@172.22.1.138:/home/mailadm/data/parsed_mails/inbound.daou/terracespamadm/.
}

export -f main
su mailadm -c bash -c 'main "'$1\ $2\ $3\ $4\ $5'"'
