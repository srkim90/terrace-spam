#!/bin/bash

function do_dir_aging()
{
    full_dir="$1/$2"

    if [ ! -d ${full_dir} ]; then
        return
    fi
    echo rm -rf ${full_dir}
    rm -rf ${full_dir}
}

function main()
{
    source ~/__bash_profile
    if [ "${MAIL_DATA_HOME}" == "" ]; then
        echo Not exist environment variable : PYTHON_INSTALL_PATH
        return
    fi

    is_yyyymmdd=`echo $1 | grep 202 | wc -l`
    if [ "$is_yyyymmdd" == 1 ]; then
        YYYYMMDD=$1
        echo YYYYMMDD:${YYYYMMDD}
    elif [ "$1" != "" ]; then
        DAY_AGO="$1 $2 $3 $4 $5"
        echo  ${DAY_AGO}
        YYYYMMDD=`date "+%Y%m%d" --date "${DAY_AGO}"`
    else
        DAY_AGO="10 days ago"
        echo  ${DAY_AGO}
        YYYYMMDD=`date "+%Y%m%d" --date "${DAY_AGO}"`
    fi

    HAM_ROOT="${MAIL_DATA_HOME}/parsed_mails/terracehamadm"
    SPAM_ROOT="${MAIL_DATA_HOME}/parsed_mails/terracespamadm"

    if [ -d ${HAM_ROOT} ]; then
        do_dir_aging ${HAM_ROOT} ${YYYYMMDD}
    else
        #echo not exist HAM_ROOT : ${HAM_ROOT}
        sleep 0
    fi

    if [ -d ${SPAM_ROOT} ]; then
        do_dir_aging ${SPAM_ROOT} ${YYYYMMDD}
    else
        #echo not exist SPAM_ROOT : ${SPAM_ROOT}
        sleep 0
    fi

}

export -f main
export -f do_dir_aging
su mailadm -c bash -c 'main "'$1\ $2\ $3\ $4\ $5'"'
