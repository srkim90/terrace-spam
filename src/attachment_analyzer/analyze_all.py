#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator
from datetime import date, timedelta

# import common
from common import *
from ole_analyzer import *

sys.path.append(".")
sys.path.append("../3th")
sys.path.append("../synap_helper")
sys.path.append("./content_handler")

#/home/mailadm/data/attachment/index
base_dir = os.environ['MAIL_DATA_HOME']

class AttachmentAnalyzer():
    def __init__(self, yyyymmdd, extension_name):
        self.list_dict = []
        self.yyyymmdd = yyyymmdd
        self.extension_name = extension_name

    def __load_file(self):
        list_file = "%s/attachment/index/index_%s_%s.json" % (base_dir, self.yyyymmdd, self.extension_name)

        if os.path.exists(list_file) != True:
            return None

        with open(list_file, "r") as fd:
            list_json = fd.read()
        list_json = list_json.split("}")
        for json_at in list_json:
            if len(json_at) < 20:
                continue
            json_at += "}"
            #print("===========\n%s\n======\n" % json_at)
            self.list_dict.append(json.loads(json_at))
        return list_file

    def do_analize(self):
        if self.__load_file() == None:
            return False
        if self.extension_name == "doc":
            analizerFactory = OleAnalyzer
        else:
            print("unsupported attachment type : %s" % (self.extension_name,))
            return None
        for item in self.list_dict:
            file_name = item["SavePath"]
            analizer = analizerFactory(file_name)
            #print(file_name)
            analizer.handle_vba()
        return True

def get_parameter():
    yyyymmdd        = []
    params          = sys.argv[1:]
    extension_name  = None
    for idx, item in enumerate(params):
        if idx == 0:
            extension_name = item
        else:
            yyyymmdd.append(item)

    if len(yyyymmdd) == 2:
        sdate = date(int(yyyymmdd[0][0:4]), int(yyyymmdd[0][4:6]), int(yyyymmdd[0][6:8]))
        edate = date(int(yyyymmdd[1][0:4]), int(yyyymmdd[1][4:6]), int(yyyymmdd[1][6:8]))
        delta = edate - sdate  
        yyyymmdd = []
        for idx in range(delta.days + 1):
            day = sdate + timedelta(days=idx)
            day = "%s" % (day,)
            day = day.replace("-", "")
            yyyymmdd.append(day)
    return yyyymmdd, extension_name

def main():
    # 1. Init LOG
    base_dir = os.environ['MAIL_DATA_HOME']
    common_init_log(base_dir, "ole_analyzer", INFO)

    yyyymmdd_list, extension_name = get_parameter()

    if len(yyyymmdd_list) == 0 or extension_name == None:
        print("./analyze_all.py [extension_name] [yyyymmdd_1] [yyyymmdd_2]")
        return None

    # 2. Init OleAnalyzer & run
    for yyyymmdd in yyyymmdd_list:
        print("day : %s" % (yyyymmdd,))
        analyzer = AttachmentAnalyzer(yyyymmdd, extension_name)
        analyzer.do_analize()
    return

if __name__ == "__main__":
    main()


