#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import common
from common import *

sys.path.append(".")
sys.path.append("../3th")
sys.path.append("../3th/oletools")
sys.path.append("../synap_helper")
sys.path.append("./content_handler")

import io
import olefile
import olemeta
import chardet
import oletools
import synap_helper
import oletools.oleid
from oletools.olevba import VBA_Parser, VBA_Parser_CLI, TYPE_OLE, TYPE_OpenXML, TYPE_Word2003_XML, TYPE_MHTML, filter_vba, VBA_Scanner

base_dir = os.environ['MAIL_DATA_HOME']

class VBA_Parser_Terrace(VBA_Parser):
    def analyze_macros_a_file(self, vba_code):
        if self.detect_vba_macros():
            # Analyze the whole code at once:
            if vba_code == None:
                return None
            try:
                scanner = VBA_Scanner(vba_code)
            except TypeError as e:
                return None
            analysis_results = scanner.scan(False, False)
        else:
            return None
        if len(analysis_results) == 0:
            return None
        return analysis_results
 
class OleAnalyzer():
    def __init__(self, file_name=None):
        self.file_name = file_name
        if file_name != None:
            self.report_name = self.file_name.split("/")[-1].split(".")[0]
            with open(self.file_name, "rb") as fd:
                self.binary_data = fd.read()
            try:
                self.yyyymmdd = file_name.split("/")[-2]
            except:
                self.yyyymmdd = "NO_YYYYMMDD"
            self.save_dir = None

    def __make_save_dir(self, base_dir, report_name):
        try:
            yyyymmdd = self.yyyymmdd
        except AttributeError as e:
            return None
        save_dir  = "%s/attachment/report" % base_dir
        if os.path.exists(save_dir) == False:
            os.makedirs(save_dir)
        save_dir += "/%s" % (yyyymmdd,)
        if os.path.exists(save_dir) == False:
            os.makedirs(save_dir)
        save_dir += "/%s" % (report_name,)
        if os.path.exists(save_dir) == False:
            os.makedirs(save_dir)
        return save_dir

    def __save_object(self, file_name, data):
        if self.save_dir == None:
            return None
        save_name = "%s/%s" % (self.save_dir, file_name)
        with open(save_name, "w") as fd:
            fd.write(data)
        return save_name

    def __save_report(self, vba_result):
        save_name = "%s/report.json" % (self.save_dir)
        try:
            data = json.dumps(vba_result, indent=4, ensure_ascii=False)
        except Exception as e:
            print("Error. json dump fail : %s" % e)
            return None
        with open(save_name, "w") as fd:
            fd.write(data)
        return save_name

    def handle_vba(self):
        binary_data = self.binary_data
        save_dir    = self.save_dir
        report_name = self.report_name
        file_name   = self.file_name

        self.in_handle_vba(binary_data, save_dir, report_name, file_name)

    def in_handle_vba(self, binary_data, save_dir=None, report_name=None, file_name=None):
        # 1. ole-vba
        try:
            vbaparser = VBA_Parser_Terrace(io.BytesIO(binary_data), data=binary_data)
        except Exception as e:
            LOG(LOG_INF, "Fail in VBA_Parser", exception=e)
            print(e)
            return None
        vba_detected = []
        extract_macro_set = vbaparser.extract_macros()
        try:
            for (filename, stream_path, vba_filename, vba_code) in extract_macro_set:
                if save_dir == None:
                    self.save_dir = self.__make_save_dir(base_dir, report_name)
                results = vbaparser.analyze_macros_a_file(vba_code)
                if results != None:
                    in_script = {}
                    for kw_type, keyword, description in results:
                        if kw_type not in in_script.keys():
                            #in_script[kw_type] = {
                            #    "keyword" : []
                            #}
                            in_script[kw_type] = []
                        #in_script[kw_type]["keyword"].append(keyword)
                        in_script[kw_type].append(keyword)
                    if len(in_script.keys()) == 0:
                        continue
                    item = {
                        "detected"      : in_script,
                        "vba_filename"  : vba_filename,
                        "sz_script"     : len(vba_code),
                        "md5_sum"       : md5sum(vba_code),
                        "vba_save_path" : self.__save_object(vba_filename, vba_code)
                    }
                    vba_detected.append(item)
        except TypeError as e:
            return None

        # 2. ole-meta
        ole_meta = {}
        try:
            oleparser = olefile.OleFileIO(io.BytesIO(binary_data))
        except Exception as e:
            LOG(LOG_INF, "Fail in OLE_META_Parser", exception=e)
            return None
        meta = oleparser.get_metadata()
        for prop in meta.SUMMARY_ATTRIBS + meta.DOCSUM_ATTRIBS:
            value = getattr(meta, prop)
            if type(value) == bytes:
                try:
                    value = value.decode("utf-8")
                except UnicodeDecodeError as e:
                    chdt = chardet.detect(value)["encoding"]
                    try:
                        value = value.decode(chdt)
                    except (UnicodeDecodeError, TypeError) as e:
                        value = None
            if type(value) == str and len(value) == 0:
                value = None
            #print("%s : %s" % (prop, value))
            ole_meta[prop] = "%s" % value

        oleparser.close()

        # 3. synap helper
        synap_data = synap_helper.synap_get_document_info(binary_data)
        synap_parsed = []
        if synap_data != None:
            doc_text = synap_data["text"]
            doc_attachment_list = synap_data["attachment_list"]
            for item in doc_attachment_list:
                #synap_parsed = item
                #print(item)
                dict_at = {}
                for key in item.keys():
                    if key == "data":
                        dict_at["md5sum"] = md5sum(item["data"])
                        continue
                    dict_at[key] = item[key]
                synap_parsed.append(dict_at)
                #print("key = %s" % ())
            #print(doc_text)

        result = {
            "ole_meta"                  : ole_meta,
            "vba_detected"              : vba_detected,
            "origen_docname"            : file_name,
            "internal_document_data"    : synap_parsed,
        }
        #for item in vba_detected:
        #    item = json.dumps(item, indent=4, ensure_ascii=False)
        #    print(item)

        if save_dir == None:
            return result

        self.__save_report(result)
        return result


def get_parameter():
    params = sys.argv[1:]
    #for idx, item in enumerate(params):
    return params

def main():

    # 1. Init LOG
    base_dir = os.environ['MAIL_DATA_HOME']
    common_init_log(base_dir, "ole_analyzer", INFO)

    ole_files = get_parameter()

    # 2. Init OleAnalyzer & run
    for file_name in ole_files:
        ole = OleAnalyzer(file_name)
        ole.handle_vba()
        print("save_at : %s/report.json" % (ole.save_dir,))
    return

if __name__ == "__main__":
    main()



