#!/usr/bin/python
# -*- coding: utf-8 -*-import os
import os
import sys
import time
import commands

arr = ['image/png', 'image/jpg', 'image/jpeg', 'image/gif', 'image/bmp', 'application/x-iso9660-image', 'application/msword', 'application/pdf', 'application/x-rar']

(exitstatus, file_cnt) = commands.getstatusoutput('find %s* -name \'*.qs.gz\' | wc -l'%sys.argv[1])

def through(filename):
    ipaddr_ct = "zgrep \"Received: from\" %s | wc -l"%filename
    (exitstatus, cnt) = commands.getstatusoutput(ipaddr_ct)

    return cnt
    #print('[%s] Pass Count :: %d'%(filename, int(cnt)))
    
def attached(filename):
    con_type = "zgrep \"Content-Type\" %s"%filename
    (exitstatus, con_out) = commands.getstatusoutput(con_type)
    for i in arr:
        if i in con_out:
           return True 

    return None

def search(dirname):
    try:
        filenames = os.listdir(dirname)
        for filename in filenames:
            full_filename = os.path.join(dirname, filename)
            if os.path.isdir(full_filename):
                search(full_filename)
            else:
                ext = os.path.splitext(full_filename)[-1]
                if ext == '.gz': 
                    # Pass Through Count Function
                    t_cnt = through(full_filename)
                    # Attached Check
                    Ret = attached(full_filename)

                    if Ret == True:
                        print('%s     O   %d'%(filename, int(t_cnt)) )
                    else:
                        print('%s     X   %d'%(filename, int(t_cnt)))
    except:
        pass

def main():
    for path in sys.argv:
        search(path)

def total_count():
    print('-----File Count[%d]-----'%(int(file_cnt)))

if __name__ == "__main__":
    print('FILENAME                            ATT VIA    ')
    main()
    total_count()
