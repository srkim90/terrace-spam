import os
import sys
import gzip
import email

sys.path.append("../3th")
sys.path.append("../3th/chilkat-9.5.0-python-3.8-x86_64-linux")

import chilkat

# /home/mailadm/data/raw_mails/twbr/terracespamadm/20200506/1150

base_dir = os.environ['MAIL_DATA_HOME'] + "/raw_mails/twbr"

class eml2Html_Converter:
    def __init__(self, eml_path, result_dir):
        self.eml_path      = self.__check_eml_path(eml_path)
        self.result_dir    = result_dir
        self.eml_file_name = self.eml_path.split("/")[-1].split(".")[0]
        self.tmp_file_name = "/tmp/.__mail_tmp_%s.tmp" % (self.eml_file_name,)

    def __check_eml_path(self, eml_path):
        if os.path.exists(eml_path) == True:
            return eml_path

        try:
            eml_name = eml_path.split("/")[-1] # 2020-05-06-11:51:57:657629.qs
            yyyy = eml_name.split("-")[0]
            mm   = eml_name.split("-")[1]
            dd   = eml_name.split("-")[2]
            _HH  = eml_name.split(":")[0].split("-")[-1]
            _MM  = eml_name.split(":")[1][0] + "0" 
            _SS  = eml_name.split(":")[2]
        except IndexError as e:
            return eml_path

        check_dir = "%s/terracespamadm/%s%s%s/%s%s/%s" % (base_dir, yyyy, mm, dd, _HH, _MM, eml_name)
        print(check_dir)
        if os.path.exists(check_dir) == True:
            return check_dir

        check_dir = "%s/terracehamadm/%s%s%s/%s%s/%s" % (base_dir, yyyy, mm, dd, _HH, _MM, eml_name)
        print(check_dir)
        if os.path.exists(check_dir) == True:
            return check_dir

        return eml_path

    def __save_as_file(self, str_data):
        new_file_name = "%s/%s.html" % (self.result_dir, self.eml_file_name)
        try:
            with open(new_file_name, "w") as fd:
                fd.write(str_data)
        except Exception as e:
            print(e)
            return None
        return new_file_name

    def do_convert(self):
        is_qs = False
        if os.path.exists(self.eml_path) == False:
            return None

        if ".gz" == self.eml_path[-3:]:
            fd = gzip.open(self.eml_path, "rb")
        else:
            fd = open(self.eml_path, "rb")

        if ".qs" in self.eml_path:
            is_qs = True

        str_mail = fd.read()

        fd.close()

        mail_lines = str_mail.split(b"\n")
        str_mail   = b""
        for line in mail_lines:
            if is_qs == True and b"%trqfile2% qv" in line:
                continue
            if is_qs == True and b"^^^^^^^^+_~!spacelee@$%^&!@#)_,$^^^^^^^^^^" in line:
                break
            str_mail += line + b"\n"

        with open(self.tmp_file_name, "wb") as fd:
            fd.write(str_mail)

        email = chilkat.CkEmail()
        #  Load an email from a .eml
        success = email.LoadEml(self.tmp_file_name)
        os.remove(self.tmp_file_name)
        if (success != True):
            #print(email.lastErrorText())
            #sys.exit()
            return None

        #  If an HTML body is present, display the HTML source:
        bHtml = email.HasHtmlBody()
        if (bHtml == True):
            #print(email.getHtmlBody())
            return self.__save_as_file(email.getHtmlBody())

        #  If a plain-text body is present, display it:
        bText = email.HasPlainTextBody()
        if (bText == True):
            #print(email.getPlainTextBody())
            return self.__save_as_file(email.getPlainTextBody())

        return None

def main():
    test_eml="/home/mailadm/data/raw_mails/twbr/terracespamadm/20200506/1150/2020-05-06-11:51:57:657629.qs"
    e = eml2Html_Converter(test_eml,"/tmp")
    html_file = e.do_convert()
    if html_file != None:
        print("success : %s --> %s" % (test_eml, html_file))
    else:
        print("fail    : %s" % (test_eml))
    return  

main()
