# -*- coding: utf-8 -*-

import json
import sys
import base64
import socket
import time
import gzip
import os
import subprocess

sys.path.append("../synap_helper")
from cyren_helper import *

total_n = 0
ck_n = 0

def main():
    file_path = sys.argv[1]
    e = CyrenHelper()

    print(file_path)

    cmd = ['find',file_path,'-name','*.gz']
    print("%s\n\n"%cmd)

    f = open("./output.txt", 'w')

    fd_popen = subprocess.Popen(cmd, stdout=subprocess.PIPE).stdout
    output = fd_popen.read().strip()

    output = output.decode('utf-8')
    f.write(output)
    f.close()

    f = open("./output.txt", 'rb')

    for i in f.readlines():
        i = bytes.decode(i)
        i = i.rstrip('\n')
        global total_n
        total_n+=1

#        n_found_virus, virus_attachment = e.queryCyran("/data/maildata/attribute_data/ysw/ole_path/twbr/spam/2019-08-21-08:42:06:267836.qs.gz")
#        n_found_virus, virus_attachment = e.queryCyran("/data/maildata/attribute_data/ysw/ole_path/twbr/spam/2019-12-02-16:17:29:837030.qs.gz")
        n_found_virus, virus_attachment = e.queryCyran(i)
        if n_found_virus == 1:
            global ck_n
            ck_n+=1

        print("n_found_virus=%d, virus_attachment=%s file_path=[%s]" %(n_found_virus, virus_attachment, i))
    print("Total Count[%d] Cyren Block[%d]"%(total_n, ck_n))
#    if i.find("inbound") :
#        print("Total Count[%d] Cyren Block[%d]"%(total_n, ck_n))
#    else :
#        print("Total Count[%d] Cyren Block[%d]"%(total_n, ck_n))

    f.close()
if __name__ == "__main__":
    main()

