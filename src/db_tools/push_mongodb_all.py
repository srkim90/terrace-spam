#!/bin/env python
import os
import sys
import gzip
import json 
import pymongo
import threading
from multiprocessing.pool import Pool

sys.path.append("../mail_parsor")
from email_search import *

# python -m pip install pymongo

# [컬랙션 명령]
# db.createCollection("attribute_nation", { capped: true, size: 6142800, max: 10000 })
# show collections 
# db.all.insert({})
# db.all.find()
# db.all.find({"_id": "2020-04-21-08:31:22:968210"}, {"_id":true})
# db.all.remove({})
# db.all.remove({"_id" : "2020-04-21-08:30:41:694873.qs"})
# db.all.find({"head-items.Received.country" : {$eq:"KR;South Korea"}}, {"head-items.Received.country" : true}).pretty()
# db.all.find({"head-items.Subject.token"    : {$eq: ["register","now","pandemics","financial","inclusion","part"]}, {"head-items.Subject.token" : true}).pretty()

g_db_conn = None
class mongo_db_loeader:
    def __init__(self, mongo_server, mongo_port, mongo_dns, collection_name):
        global g_db_conn
        self.mongo_dns       = mongo_dns
        self.mongo_port      = mongo_port
        self.mongo_server    = mongo_server
        self.collection_name = collection_name
        if g_db_conn == None:
            g_db_conn = self.__mongodb_conn()

    def raw_json_insert_mongodb(self, json_path):
        h_collection = g_db_conn
        if ".json" != json_path[-5:].lower():
            return
        if ".gz" == json_path[-3:].lower():
            fd = gzip.open(json_path, "r")
        else:
            fd = open(json_path, "r")
        jsonDump = fd.read()
        fd.close()
        try:
            jsonDict = json.loads(jsonDump)
        except:
            #print("sasa %s" % _id)
            return False

        b_have_attachment = False
        if jsonDict["body-items"] == None:
            return False
        for item in jsonDict["body-items"]:
            if item == None:
                continue
            if item["Category"] != 1000:
                b_have_attachment = True
        if b_have_attachment == False:
            print(" --> skip : Not exist attachment")
            return False

        #synap_vba = jsonDict["body-items"]
        #for item in synap_vba:
        #    if 'content' not in item.keys():
        #        continue
        #    content = item["content"]
        #    if "synap_vba" in content.keys():
        #        print(content["synap_vba"])
        #        sleep(1)

        # 파일이름이 _id이다. : ex> 2020-04-21-08:30:41:694873.qs
        try: 
            doc_id = jsonDict["mail-feature"]["file-name"]
            if len(doc_id) > 5 and doc_id[-3:].lower() == ".gz":
                doc_id = doc_id.split("/")[-1]
        except Exception as e:
            print("Error. Fail to found file name in json : mail-feature->file-name")
            return False

        _id = {
            "_id" : doc_id
        }
        # _id가 이미 있으면 제거하고 다시 쓰도록 하자.
        if h_collection.find_one(_id) != None:
            #print("%s" % _id)
            h_collection.delete_one(_id)
        jsonDict["_id"] = doc_id
        try:
            h_collection.insert_one(jsonDict)
        except:
            print("Error. fail to del key : %s" % (_id))
            pass
        return True

    def __mongodb_conn(self):
        h_collection = None
        try:
            h_db  = pymongo.MongoClient(self.mongo_server, self.mongo_port)
            h_dns = h_db[self.mongo_dns] # select dns
            h_collection = h_dns[self.collection_name]
        except Exception as e:
            print("Error. Fail to load db : %s" % (e,))
            return None
        return h_collection

g_now_idx    = 0
g_list_all   = None
g_async_lock = threading.Semaphore(1)
def get_next_file():
    global g_now_idx 
    global g_list_all
    g_async_lock.acquire()
    if g_now_idx >= len(g_list_all):
        g_async_lock.release()    
        return None
    g_now_idx += 1
    g_async_lock.release()    
    return g_list_all[g_now_idx-1]

def insert_fn(mongo_server, mongo_port, mongo_dns, collection_name, json_file):
    db = mongo_db_loeader(mongo_server, mongo_port, mongo_dns, collection_name)
    db.raw_json_insert_mongodb(json_file)

def __insert_db_th(mongo_server, mongo_port, mongo_dns, collection_name):
    while True:
        eml_pair = get_next_file()
        if eml_pair == None:
            return
        json_file, yyyymmdd = eml_pair
        hProc = proc_pool.apply_async(insert_fn, [mongo_server, mongo_port, mongo_dns, collection_name, json_file])
        ret = hProc.get()
        print("[%d/%d] %s" % (g_now_idx, len(g_list_all), json_file, ))

N_WORK_THREAD=20

def main():
    global g_now_idx 
    global g_list_all
    global proc_pool
    mongo_server    = "127.0.0.1"
    mongo_port      = 27017
    mongo_dns       = "terrace-spam"
    collection_name = "attribute"
    path            = os.environ['MAIL_DATA_HOME'] + "/parsed_mails"

    end_yyyymmdd   = None
    start_yyyymmdd = None
    for idx,item in enumerate(sys.argv[1:]):
        if idx == 0:
            start_yyyymmdd = item
        elif idx == 1:
            end_yyyymmdd = item

    base_dir = emailSearch.get_raw_mails_dir(path)
    print("base_dir : %s" % base_dir)
    e = emailSearch(base_dir, start_yyyymmdd=start_yyyymmdd, end_yyyymmdd=end_yyyymmdd)
    g_list_all = e.list_files()
    #for item in g_list_all:
    #    print(item[0])

    if N_WORK_THREAD == 1:
        while True:
            eml_pair = get_next_file()
            json_file, yyyymmdd = eml_pair
            if json_file == None:
                return
            print("[%d/%d] %s" % (g_now_idx, len(g_list_all), json_file, ))
            insert_fn(mongo_server, mongo_port, mongo_dns, collection_name, json_file)
    else:
        lt_thread = []
        proc_pool = Pool(processes=N_WORK_THREAD)
        for idx in range(N_WORK_THREAD):
            hThread = threading.Thread(target=__insert_db_th, args=(mongo_server, mongo_port, mongo_dns, collection_name))
            hThread.daemon = True
            hThread.start()
            lt_thread.append(hThread)
        for item in lt_thread:
            item.join()
main()
