#!/bin/env python
import os
import sys
import gzip
import json 
import pymongo

MAX_COUNTRY_MAXRIX = 9
#python -m pip install pymongo

# [컬랙션 명령]
# db.createCollection("attribute_nation", { capped: true, size: 6142800, max: 10000 })
# show collections 
# db.attribute.insert({})
# db.attribute.find()
# db.attribute.find({"_id": "2020-04-21-08:31:22:968210"}, {"_id":true})
# db.attribute.remove({})
# db.attribute.remove({"_id" : "2020-04-21-08:30:41:694873.qs"})
# db.attribute.find({"head-items.Received.country" : {$eq:"KR;South Korea"}}, {"head-items.Received.country" : true}).pretty()
# db.attribute.find({"head-items.Subject.token"    : {$eq: ["register","now","pandemics","financial","inclusion","part"]}, {"head-items.Subject.token" : true}).pretty()

def insert_mongodb(h_dns, collection_name, jsonDump):
    h_collection = h_dns[collection_name]

    if type(jsonDump) == str:
        jsonDict = json.loads(jsonDump)

    # 파일이름이 _id이다. : ex> 2020-04-21-08:30:41:694873.qs
    try: 
        doc_id = jsonDict["mail-feature"]["file-name"]
        if len(doc_id) > 5 and doc_id[-3:].lower() == ".gz":
            doc_id = doc_id.split("/")[-1]
    except Exception as e:
        print("Error. Fail to found file name in json : mail-feature->file-name")
        return False

    _id = {
        "_id" : doc_id
    }
    # _id가 이미 있으면 제거하고 다시 쓰도록 하자.
    if h_collection.find_one(_id) != None:
        h_collection.delete_one(_id)
    jsonDict["_id"] = doc_id
    h_collection.insert_one(jsonDict)
    return True

def query_country_matrix(h_dns, collection_name):
    h_collection = h_dns[collection_name]
    _query_cond   = {"head-items.Received.country" : {"$eq" :"KR;South Korea"}}
    _query_target = {"head-items.Received.country" : True}
    query_result  = h_collection.find(_query_cond, _query_target)
    for idx,pos in enumerate(query_result):
        print("%d %s" % (idx, pos))


def query_subject_matrix(h_dns, collection_name):
    h_collection = h_dns[collection_name]
    #_query_cond   = {"head-items.Subject.token" : {"$eq" : ["register","now","pandemics","financial","inclusion","part"]}}
    _query_target = {"head-items.Subject.token" : True}
    query_result  = h_collection.find(_query_cond, _query_target)
    for idx,pos in enumerate(query_result):
        print("%d %s" % (idx, pos))


def main():
    mongo_server    = "127.0.0.1"
    mongo_port      = 27017
    mongo_dns       = "terrace-spam"
    collection_name = "attribute"
    file_list = [] #os.listdir(path)

    try:
        h_db  = pymongo.MongoClient(mongo_server, mongo_port)
        h_dns = h_db[mongo_dns]
    except Exception as e:
        print("Error. Fail to load db : %s" % (e,))
        return

    for file_name in sys.argv[1:]:
        file_list.append(file_name)

    for item in file_list:
        if ".json" != item[-5:].lower():
            continue
        full_path = item#"%s/%s" % (path, item)
        if ".gz" == full_path[-3:].lower():
            fd = gzip.open(full_path, "r")
        else:
            fd = open(full_path, "r")
        jsonDump = fd.read()
        fd.close()
        bErr = insert_mongodb(h_dns, collection_name, jsonDump)
        print("is success : %s" % bErr)
    #query_country_matrix(h_dns, collection_name)
    #query_subject_matrix(h_dns, collection_name)

main()
