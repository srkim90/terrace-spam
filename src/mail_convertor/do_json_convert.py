#!/bin/env python
# -*- coding: utf-8 -*-

'''
  Filename : do_json_convert.py
  Release  : 1
  Date     : 2020-03-30
   
  Description : json mail convertor of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/03/30 created 
'''

# common package import
import re
import os
import sys
import time
import gzip
import copy
import json
import traceback
import datetime 
import threading
from multiprocessing.pool import Pool

# local package import
from common import *

def main():
    print("Hello World!!")
    return
        
if __name__ == "__main__":
    main()
