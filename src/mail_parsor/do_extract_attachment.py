#!/bin/env python
# -*- coding: utf-8 -*-

'''
  Filename : do_extract_attachment.py at mail_parsor
  Release  : 1
  Date     : 2020-02-19
   
  Description : attachment parsor of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/02/19 created 
'''

# common package import
import re
import os
import sys
import time
import gzip
import copy
import json
import pickle
import traceback
import datetime 

# local package import
from email_parser import *
from email_search import *
from email_content_parser import *

base_dir = None

class ExtractAttachment:
    def __init__(self, base_dir, start_yyyymmdd, end_yyyymmdd):
        __base_dir = emailSearch.get_raw_mails_dir(base_dir)
        print(__base_dir)
        if type(start_yyyymmdd) == str and os.path.exists(start_yyyymmdd) == True:
            self.search_result = []
            with open(start_yyyymmdd, "r") as fd:
                list_all = fd.read().split("\n")
            for item in list_all:
                if ".qs" not in item and ".eml" not in item:
                    continue
                #n_found_virus=1, virus_attachment=['/mime/Scan 2019_10_17.doc'] file_path=[/.../2019-10-17-11:30:51:035830.qs.gz]
                qs_name    = item.split("[")[-1].split("]")[0]
                #item_split = qs_name.split("/")
                now_yyyymmdd = qs_name.split("/")[-1][0:10].replace("-", "")
                #for spli_at in item_split:
                #    if len(spli_at) == 8 and spli_at[0:2] == "20":
                #        now_yyyymmdd = spli_at
                key_file = start_yyyymmdd.split("/")[-1].split(".")[0]
                pair = (qs_name, key_file)
                print("%s %s" % pair)
                self.search_result.append(pair)
        else:
            search = emailSearch(__base_dir, start_yyyymmdd=start_yyyymmdd, end_yyyymmdd=end_yyyymmdd)
            self.search_result = search.list_files()

        self.ext_list = [
         "doc", "docx", "xls", "xlsx", "ppt", "pptx" , "pdf", "hwp", "txt", 
         "zip", "rar", "gz", "tar", 
         "exe", "so", 
         "png", "jpg", "gif", "bmp", 
        ]
        self.ext_list = [ "doc", "xls", "ppt", "hwp" ]
        self.save_base = "%s/attachment" % (base_dir,)
        #self.fd_dict   = {}
        #for ext_type in self.ext_list:
        #    index_file_name = self.__get_index_file_name(ext_type)
        #    fd = open(index_file_name, "w")
        #    self.fd_dict[ext_type] = fd
        self.known_index = {}

    def __get_index_file_name(self, ext_type, yyyymmdd):
        index_dir = "%s/%s" % (self.save_base, "index")
        if os.path.exists(index_dir) == False:
            os.makedirs(index_dir)
        return "%s/index_%s_%s.json" % (index_dir, yyyymmdd, ext_type)

    def __get_data_file_name(self, yyyymmdd, ext_type):
        #/home/mailadm/data/attachment/[data]/[type]/[yyyymmdd]
        if yyyymmdd not in self.known_index.keys():
            self.known_index[yyyymmdd] = 0
        self.known_index[yyyymmdd] += 1
        now_idx = self.known_index[yyyymmdd]
        file_name = "%04d.%s" % (now_idx, ext_type)
        dir_check = [self.save_base, "data", yyyymmdd]
        dir_parh  = ""
        for item in dir_check:
            dir_parh += "%s/" % (item,)
            if os.path.exists(dir_parh) == False:
                os.makedirs(dir_parh)
        file_name = "%s%s" % (dir_parh, file_name )

        return file_name

    def __save_attachment(self, eml_at, data, ext, data_save_path, yyyymmdd):
        #fd = self.fd_dict[ext]
        json_file_name = self.__get_index_file_name(ext, yyyymmdd)
        fd = open(json_file_name, "a")
        index_json = json.dumps(eml_at, indent=4, ensure_ascii=False)
        fd.write("%s\n" % (index_json,))
        fd.close()
        with open(data_save_path, "wb") as data_fd:
            try:
                data_fd.write(data)
            except Exception as e:
                print(e)

    def marge_result(self):
        #cmds = ["cd %s" % (self.save_base,),]
        cmds = []
        for ext in self.ext_list:
            cmd = 'find %s/index -name "index_*_%s.json" | xargs cat > %s/index_%s.json' % (self.save_base, ext, self.save_base, ext,)
            cmds.append(cmd)
        for cmd in cmds:
            os.system(cmd)

    def do_work(self):
        for idx,file_set in enumerate(self.search_result):
            ps_path   = file_set[0]
            yyyymmdd  = file_set[1]
            print("[%s/%s] %s" % (idx, len(self.search_result), ps_path))
            if idx % 200 == 0 or idx == len(self.search_result) - 1:
                self.marge_result()
            eml       = emailParser(ps_path)
            if eml.load_all() == None:
                continue
            report_list = eml.extract_attachment(self.ext_list)
            if len(report_list) == 0:
                continue

            is_spam = False

            if "terracespamadm" in ps_path or ".eml" in ps_path or "/twbr/spam" in ps_path:
                is_spam = True

            for item in report_list:
                ContentName     = item["ContentName"]
                Data            = item["Data"]
                Ext             = item["Ext"]
                data_save_path  = self.__get_data_file_name(yyyymmdd, Ext)
            
                eml_at = {
                    "qsPath"        : ps_path,
                    "isSpam"        : is_spam,
                    "ContentName"   : ContentName,
                    "SavePath"      : data_save_path,
                    "Size"          : len(Data),
                }
                self.__save_attachment(eml_at, Data, Ext, data_save_path, yyyymmdd)

def get_param():
    start_yyyymmdd  = None
    end_yyyymmdd    = None

    for idx,item in enumerate(sys.argv[1:]):
        if idx == 0:
            start_yyyymmdd = item
        elif idx == 1:
            end_yyyymmdd = item
    if end_yyyymmdd == None:
        end_yyyymmdd = start_yyyymmdd

    return start_yyyymmdd, end_yyyymmdd

def main():
    base_dir = os.environ['MAIL_DATA_HOME']
    common_init_log(base_dir, "email_parser", INFO)
    start_yyyymmdd, end_yyyymmdd = get_param()
    e = ExtractAttachment(base_dir, start_yyyymmdd, end_yyyymmdd)
    e.do_work()

if __name__ == "__main__":
    main()
