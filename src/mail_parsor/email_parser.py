#!/bin/env python
# -*- coding: utf-8 -*-

'''
  Filename : email_parser.py at mail_parsor
  Release  : 1
  Date     : 2020-02-19
   
  Description : mime extractor of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/02/19 created 
'''

# common package import
import re
import os
import sys
import time
import gzip
import copy
import json
import quopri
import base64
import codecs
import chardet
import traceback

# email package import
import email

# local package import
from common import *
from word_split import *
from email_qs_parser import *
from email_header_parser import *
from email_content_parser import *

sys.path.append("../synap_helper")
from cyren_helper import *

'''
{
    mail-feature : {
        file-name  : "2020-02-06-16:09:10:557574.qs.gz"
        qs-info    : {...}
    }
    body-items : [
        {
            "Category"          : CONTENT_CAT_TEXT,
            "Sub-Category"      : CONTENT_SUBCAT_TEXT_HTML,
            "Content-Type"      : "text/plain" or "text/html", ...
            "Data"              : {
                "Word-List"     : ['Secure', 'your', 'web', 'assets', 'with', ... ]
                "URL-List"      : ['https://gallery.mailchimp.com/79cb120fb8178d/images/3a5cdd8832de85.png', ...]
                "Domin-List"    : ['www.acunetix.com', 'www.acunetix.com', ...]
            }
        }, ...
    ]
}
'''
class emailParser:
    def __init__(self, ps_path):
        self.ps_path      = ps_path
        self.email_obj    = None
        self.mail_feature = None
        self.qs_info      = None
        self.sz_mail      = None
        self.hdr_parsed   = None
        self.body_parsed  = None
        self.spam_type    = None
        self.rawAll       = None
        if "?" in ps_path:
            self.ps_path   = ps_path.split("?")[0]
            self.spam_type = ps_path.split("?")[1]

        debug_dict = {
            "ps_path" : self.ps_path,
        }
        SET_DEBUGINFO(debug_dict)

    def load_all(self):
        try:
            nRet = self.__load_text()
            if nRet == None:
                return None
            self.__get_header_item()
        except Exception as e:
            LOG(LOG_ERR, "Fail to load all, ps_path=%s", exception=e)
            return None
        return nRet

    def __load_text(self):
        f_start = False
        fd = None
        qs_tails = None
        if self.ps_path[-3:].lower() == ".gz":
            fd = gzip.open(self.ps_path , 'rb')
        else:
            fd = open(self.ps_path , 'rb')

        rawAll = fd.read()
        fd.close()

        # .qs 파일일 경우, 메타데이터 제거
        if ".qs" in self.ps_path:
            for idx, at in enumerate(rawAll):
                if at == ord('\n'):
                    if b'%trqfile2%' in rawAll[:idx]:
                        rawAll = rawAll[idx+1:]
                    break
                elif idx == 1000:
                    break
            for idx in range(100, 1024 * 4):
                subline = rawAll[idx * -1:]
                start_word = b'^^^^^^^^+_~!spacelee@$%^&!@#)_,$^^^^^^^^^^'
                if start_word in subline:
                    qs_tails = rawAll[(idx * -1) + len(start_word) + 1: ]
                    rawAll   = rawAll[:idx * -1]
                    #print(qs_tails)
                    break
        self.sz_mail = len(rawAll)
        self.rawAll = rawAll
        try:
            self.email_obj = email.message_from_bytes(rawAll, policy=email.policy.SMTP)
        except Exception as e:
            LOG(LOG_ERR, "TODO: Add", exception=e)
            return None

        #print(qs_tails)
        if qs_tails != None:
            tail_lines = qs_tails.split(b'\n')
            qs_tails = b''
            #print(tail_lines)
            for item in tail_lines:
                #print(item)
                encoding = chardet.detect(item)['encoding']
                if encoding != "ascii":
                    continue
                qs_tails += item + b'\r\n'
            encoding = "ascii"

            qs_tails = qs_tails.decode(encoding)
            obj = qsHeaderParse(qs_tails)
            self.qs_info = obj.qs_header_parse()
            #except Exception as e:
            #    print("Error. qs : %s, %s" % (self.ps_path, e))
            #    pass
        return True

    def __get_part_content(self, part):
        body = None
        try:
            body = part.get_content()
        except (KeyError, LookupError) as e: # ython3.7/email/message.py 에서 MIME 자동 type detect 가 실패 할 경우, 수동으로 풀어준다.
            try:
                raw_body = part.get_payload().encode("UTF-8")
            except Exception as e2:
                LOG(LOG_INF, "TODO: Fail to decode content payload as utf-8", exception=e2)
                return None
            tr_encode = part["Content-Transfer-Encoding"]
            if tr_encode == None:
                tr_encode = part["content-transfer-encoding"]
            if tr_encode != None:
                tr_encode = tr_encode.lower()
            if tr_encode == "base64":
                try:
                    body = base64.b64decode(raw_body)
                except:
                    body = raw_body
            elif tr_encode == "quoted-printable":
                #raw_body = raw_body.replace(b"\r",b"")
                #raw_body = raw_body.replace(b"\n",b"")
                body = quopri.decodestring(raw_body)#.decode("utf-8")
            else:
                body = raw_body
        return body

    def extract_attachment(self, ext_list):
        #parser = emailContentParser()
        report_list = []
        for idx,part in enumerate(self.email_obj.walk()):
            try:
                part_item = part.items()
            except Exception as e:
                #utf8_string = remove_non_ascii_char(part)
                #pass
                LOG(LOG_INF, "TODO: Add", exception=e)
                continue
            #parsed = parser.detect_content_category(part_item)
            parsed = emailHeaderParser(part_item) 
            ContentName = parsed.get_value_from_header_name("Content-Type", tag="Name")
            if ContentName == None:
                continue
            body = self.__get_part_content(part)
            #print("type : %s" % (type(body)))
            ext = ContentName.split(".")[-1].lower()
            if ext not in ext_list:
                continue
            attachment_report = {
                "ContentName"   : ContentName,
                "Data"          : body,
                "Ext"           : ext,
            }
            report_list.append(attachment_report)
            #print(body)
        return report_list

    def __get_body_content(self):
        def __get_content_type_value(part):
            for head_key in part.keys():
                lower_key = ("%s" % head_key).lower()
                if lower_key == "content-type":
                    return part[head_key]
            return None
        parser = emailContentParser()
        self.body_parsed = parser

        result_list = []
        for idx,part in enumerate(self.email_obj.walk()):
            if part.is_multipart() == True:
                continue
            
            #parsed = parser.detect_content_category(__get_content_type_value(part))
            try:
                part_item = part.items()
            except Exception as e:
                #utf8_string = remove_non_ascii_char(part)
                #pass
                LOG(LOG_INF, "TODO: Add", exception=e)
                continue
            #print("part_item : %s" % (part_item,))
            #except UnicodeDecodeError as e:
            #    continue
            #except AttributeError as e:
            #    continue
            #except ValueError as e:
            #    continue 
            #except TypeError as e:
            #    utf8_string = remove_non_ascii_char(part)
            #    print(utf8_string)
            #    continue 
            parsed = parser.detect_content_category(part_item)
                
            #print("%s %s" % (parsed,part.items()))
            if parsed == None:
                continue
            #print(part["Content-Type"])
            CATEGORY, SUB_CATEGORY, ContentType, ContentName = parsed
            #print("%s 1!!" % ContentType)

            headers = list(part.keys())

            #hdr = email.headerregistry.ContentTypeHeader()
            #hdr.init('text/plain; charset="cp-850"')
            #part["Content-Type"] = hdr;
            body = self.__get_part_content(part)

            if body == None:
                continue

            parsedBody = parser.do_content_parser(headers, body, CATEGORY, SUB_CATEGORY, ContentType, ContentName, part_item)

            result_list.append(parsedBody)

            parser.append_body_header(part_item)
        if len(result_list) == 0:
            return None
        return result_list

    def __get_header_item(self):
        try:
            headers = self.email_obj.items()
        except Exception as e:
            LOG(LOG_INF, "TODO: Add", exception=e)
            return None
        parsed = emailHeaderParser(headers)
        if parsed == None:
            return None
        self.hdr_parsed = parsed
        return True
        #return parsed.get_values()

    def __get_sip_country_code(self, clientip):
        if self.hdr_parsed != None:
            return self.hdr_parsed.get_ipaddr_country(clientip)
        return None

    def __get_sip(self, header_items):
        sip = None
        if header_items == None:
            return None
        if "Received" not in header_items.keys():
            return None
        for key in header_items.keys():
            if "Received" != key:
                continue
            received_list = header_items[key]
            if received_list == None:
                continue
            for item in received_list:
                try:
                    _by         = item["by"]
                    _for        = item["for"]
                    _ipaddr     = item["ipaddr"]
                    _from_form  = item["from_form"]
                    _by_ipaddr  = item["by_ipaddr"]
                except KeyError as e:
                    continue
                if item["feature"] != None:
                    _prefix_len = item["feature"]["_prefix_len"]
                else:
                    _prefix_len = 24

                #PRINT("_by        : %s" % (_by,))
                #PRINT("_for       : %s" % (_for,))
                #PRINT("_ipaddr    : %s" % (_ipaddr,))
                #PRINT("_from_form : %s" % (_from_form,))
                #PRINT("_by_ipaddr : %s" % (_by_ipaddr,))

                if _for == None or _by == None or _ipaddr == None or _by_ipaddr == None:
                    continue

                if "terracespamadm" in _for or "terracehamadm" in _for:
                    # 1. for가 terracespamadm, terracehamadm이면 제외
                    continue
                if is_same_network_range(_ipaddr, _prefix_len, _by_ipaddr) == True:
                    # 2. from과 by의 아이피 대역이 같으면( 제외
                    continue
                if is_ipaddr_is_private(_ipaddr) == True:
                    # 3. from이 127.0.0.1 또는 사설 제외
                    continue
                if _from_form == False:
                    # 4. from이 [ip형식] ([ip형식]) 아니면 제외
                    continue
                #print("_ipaddr : %s" % _ipaddr)
                return _ipaddr
        return None

    def __get_mail_source(self, header_items):
        if header_items == None:
            return None, None
        if "Received" not in header_items.keys():
            return None, None
        recv_list = header_items["Received"]
        if recv_list == None or len(recv_list) == 0:
            return None, None
        if "by" not in recv_list[0].keys():
            return None, None
        source_host = recv_list[0]["by"]
        if source_host == "mail.terracetech.com" or source_host == "twbr.terracetech.com":
            source_site = "twbr"
        elif source_host == "tmse.daou.co.kr" or source_host == "tmse.daou.com":
            source_site = "inbound.daou"
        else:
            source_site = "cloud"
        return source_site, source_host

    def mk_mail_feature(self, header_items, body_items):
        feature = {}

        body_sequence   = None
        header_sequence = None

        if self.hdr_parsed != None:
            header_sequence = self.hdr_parsed.get_header_name_list()
        if self.body_parsed != None:
            body_sequence = self.body_parsed.get_body_info_list()

        cyren_result = None
        e = CyrenHelper()
        n_found_virus, virus_attachment = e.queryCyran(self.rawAll)
        if n_found_virus >= 1:
            cyren_result = {
                "n_found_virus"         : n_found_virus,
                "virus_attachment"      : virus_attachment,           
            }

        feature["file-name"]        = self.ps_path
        feature["qs-info"]          = self.qs_info
        feature["mail-size"]        = self.sz_mail
        feature["header-sequence"]  = header_sequence
        feature["body-sequence"]    = body_sequence
        feature["source_site"], feature["source_host"] = self.__get_mail_source(header_items)
        feature["sip"]              = self.__get_sip(header_items)
        feature["sip-country"]      = self.__get_sip_country_code(feature["sip"])
        feature["cyren-result"]     = cyren_result
    
        if "terracespamadm" in self.ps_path or ".eml" in self.ps_path:
            feature["is-spam"] = True
        else:
            feature["is-spam"] = False

        self.mail_feature = feature

    def mk_mail_report(self):
        body_items = self.__get_body_content()

        header_items = None
        if self.hdr_parsed != None:
            header_items = self.hdr_parsed.get_values()
            if "Content-Type" in header_items.keys():
                del header_items["Content-Type"]

        self.mk_mail_feature(header_items, body_items)
        report_dict = {
                "mail-feature"  : self.mail_feature,
                "head-items"    : header_items,
                "body-items"    : body_items,
            }
        return report_dict

    @staticmethod
    def make_save_file_name(base_path, ps_path, do_gzip=False, __spam_type=None):
        spam_type = None
        yyyymmdd  = None
        hhmm      = None

        if "inbound" in ps_path:
            ext_type = ps_path.split(".")[-1]
            if ext_type == "eml":
                __spam_type = "terracespamadm"
            else:
                __spam_type = "terracehamadm"

        if "?" in ps_path:
            __spam_type = ps_path.split("?")[1]
            ps_path     = ps_path.split("?")[0]

        if os.path.exists(base_path) == False:
            try:
                os.makedirs(base_path)
            except Exception as e:
                LOG(LOG_WAR, "TODO: Add", exception=e)
                print("Not exist base_path : %s" % (base_path,))
                return None

        if __spam_type != None:
            spam_type = __spam_type

        for item in ps_path.split("/"):
            if ("terracespamadm" == item or "terracehamadm" == item or "logmonitoring" == item) and spam_type == None:
                spam_type = item
            elif spam_type != None and len(item) == 8 and yyyymmdd == None and item[:2] == "20":
                yyyymmdd = item
            elif yyyymmdd != None and hhmm == None and len(item) == 4:
                hhmm = item
        if spam_type == None or yyyymmdd == None or hhmm == None:
            last_depth = ps_path.split("/")[-1]
            org_name = ""
            toks     = last_depth.split(".")
            for idx,name_tok in enumerate(toks):
                #print("idx=%d, len=%d, name_tok=%s" % (idx, len(toks), name_tok))
                if idx+2 >= len(toks) and (name_tok == "eml" or name_tok == "qm" or name_tok == "qs" or name_tok == "txt" or name_tok == "gz"):
                    break
                if idx+1 == len(toks):
                    break
                if idx != 0:
                    org_name += "."
                org_name += name_tok
            if org_name == "":
                org_name = ps_path.split("/")[-1] 
            full_name = "%s/%s.json" % (base_path, org_name)
            if do_gzip == True:
                full_name += ".gz"
            return full_name

        if "raw_mails/" in ps_path:
            site_name = ps_path.split("raw_mails/")[1].split("/")[0]
            path = "%s/%s/%s/%s/%s" % (base_path, site_name, spam_type, yyyymmdd, hhmm)
        else:
            path = "%s/%s/%s/%s" % (base_path, spam_type, yyyymmdd, hhmm)

        path_at = ""
        path_split = path.split("/")

        for item in path_split:
            if item == '':
                continue
            path_at += "/%s" % (item,)
            if not os.path.exists(path_at):
                try:
                    os.makedirs(path_at)
                except Exception as e:
                    if not os.path.exists(path_at):
                        LOG(LOG_WAR, "TODO: Add", exception=e)
                        print("[save_parsed_mail_info] Fail to make dir : Exception=%s" % (e,))
                        return None

        last_depth = ps_path.split("/")[-1]
        org_name = ""
        toks     = last_depth.split(".")
        for idx,name_tok in enumerate(toks):
            #print("idx=%d, len=%d, name_tok=%s" % (idx, len(toks), name_tok))
            if idx+2 >= len(toks) and (name_tok == "eml" or name_tok == "qm" or name_tok == "qs" or name_tok == "txt" or name_tok == "gz"):
                break

            if idx+1 == len(toks):
                break
            if idx != 0:
                org_name += "."
            org_name += name_tok
        if org_name == "":
            org_name = ps_path.split("/")[-1] 

        new_name = org_name#ps_path.split("/")[-1].split('.')[0]
        full_name = "%s/%s.json" % (path_at, new_name)
        if do_gzip == True:
            full_name += ".gz"
        return full_name

    def save_parsed_mail_info(self, base_path, report, do_gzip=False):
        full_name = emailParser.make_save_file_name(base_path, self.ps_path, do_gzip, self.spam_type)
        try:
            jDumps = json.dumps(report, indent=4, ensure_ascii=False)
        except Exception as e:
            LOG(LOG_WAR, "TODO: Add", exception=e)
            print("[save_parsed_mail_info] Fail to dump to json report : Exception=%s" % (e,))
        try:
            if do_gzip == True:
                fd = gzip.open(full_name, "wb")
                jDumps = jDumps.encode('utf-8')
            else:
                fd = open(full_name, "w")
            fd.write(jDumps)
            fd.close()
        except Exception as e:
            LOG(LOG_WAR, "TODO: Add", exception=e)
            print("[save_parsed_mail_info] Fail to save json data : Exception=%s" % (e,))
            return None

        #print("mail report saved st : %s" % (full_name,))
        return full_name

    def show_result(self, report):
        try:
            jDumps = json.dumps(report, indent=4, ensure_ascii=False)
        except Exception as e:
            LOG(LOG_WAR, "TODO: Add", exception=e)
            print("[save_parsed_mail_info] Fail to dump to json report : Exception=%s" % (e,))       
            return
        print(jDumps)

def load_mail_report(report_path):
    report_dict = None
    try:
        if ".gz" in report_path:
            fd = gzip.open(report_path, "rb")
        else:
            fd = open(report_path, "rb")
        json_data = fd.read()
        fd.close()
        report_dict = json.loads(json_data)
    except Exception as e:
        LOG(LOG_WAR, "TODO: Add", exception=e)
        print("Error in load_mail_report : %s" % (e,))
        traceback.print_exc()
    return report_dict

def main():
    try:
        base_dir = os.environ['MAIL_DATA_HOME']
    except Exception as e:
        LOG(LOG_WAR, "TODO: Add", exception=e)
        print("Not exist environment : 'MAIL_DATA_HOME'")
        return None
    common_init_log(base_dir, "email_parser", INFO)

    for ps_path in sys.argv[1:]:
        e = emailParser(ps_path)
        if e.load_all() == None:
            print("Fail to load mail: %s" % (ps_path,))
            continue
        report = e.mk_mail_report()
        e.save_parsed_mail_info(base_dir + '/parsed_mails', report, do_gzip=True)
        jDumps = json.dumps(report, indent=4, ensure_ascii=False)
        print(jDumps)

if __name__ == "__main__":
    main()


