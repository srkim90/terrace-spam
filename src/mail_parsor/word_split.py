# -*- coding: utf-8 -*-

'''
  Filename : word_split.py
  Release  : 1
  Date     : 2019-11-26
 
  Description : mime extractor of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2019/11/26 created 
'''
# common package import
import re
import os
import sys
import time
import codecs
import chardet
from numba import jit

from common import *
from stopword_data import *
#from nltk.stem import WordNetLemmatizer

#nltk.download('wordnet')

min_word_len      = 3
max_word_len      = 12
remove_char       = ['\r', '\n']
split_char        = ['\t']
split_char_inc    = ['.', ',', '"', '\'', '[', ']', '<', '>', '*', '(', ')', ';'] # split char include separator
delete_if_include = [ '<tr>', '</tr>', '<td>', '</td>', '<table', '</table', '/p>', '<p>', '=",' ] # delete if include
special_char      = ['~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', ',', '.', '?', '`', '=', '/', '\\']

WORD_TYPE_NUMBER        = 1
WORD_TYPE_HANGUL        = 2
WORD_TYPE_ALPHABET      = 4

g_re_hangule    = re.compile("^((?!\u3131-\u318E\uAC00-\uD7A3).)*$")
g_re_alphabet   = re.compile("^((?!a-zA-Z).)*$")
g_re_number     = re.compile("^((?!0-9).)*$")

g_sub_hangule   = re.compile("[^\u3131-\u318E\uAC00-\uD7A3]")
g_sub_alphabet  = re.compile("[^a-zA-Z]")
g_sub_number    = re.compile("[^0-9]")

#g_Lemmatizer    = WordNetLemmatizer()

def remove_stopword(origin_word): # 불용어 제거
    word_classify = 0
    result_word   = origin_word

    if g_re_hangule.match(origin_word) != None:
        word_classify = word_classify | WORD_TYPE_HANGUL

    if g_re_alphabet.match(origin_word) != None:
        word_classify = word_classify | WORD_TYPE_ALPHABET
 
    if g_re_number.match(origin_word) != None:
        word_classify = word_classify | WORD_TYPE_NUMBER
 
    if word_classify & WORD_TYPE_HANGUL != 0:
        pure_words = re.split(g_sub_hangule, origin_word)
        for item in pure_words:
            if len(item) == 0:
                continue
            if item in g_korean_stop_words:
                result_word = result_word.replace(item, "")

    if word_classify & WORD_TYPE_ALPHABET != 0:
        pure_words = re.split(g_sub_alphabet, origin_word)
        for item in pure_words:
            if len(item) == 0:
                continue
            #if item in g_english_stop_words:
            #    result_word = result_word.replace(item, "")
            #    continue
            #new_word = g_Lemmatizer.lemmatize(item) 
            #if new_word != item:
            #    result_word = result_word.replace(item, new_word)

    return result_word

def listup_urls(decoded_body):
    url_skip    = ['\r', '\n']
    url_stop    = [',', '"', '\'', ' ', '\t', '>', ')', ']']
    url_schemes = ["http://", "https://", "mailto:", "ftp://", "sftp://", ]
    decoded_low = decoded_body.lower()
    len_body    = len(decoded_body)

    url_listup = []
    url_listup_org = []
    domain_listup = []

    for scheme in url_schemes:
        #print("%s:%s" % (type(decoded_low), decoded_low))
        if type(decoded_low) != str:
            continue
        start_idxs = [m.start() for m in re.finditer(scheme, decoded_low)]
        for start_idx in start_idxs:
            for idx in range(start_idx,len_body):
                char_at = decoded_body[idx]
                if char_at in url_skip:
                    continue
                if char_at in url_stop:
                    break
                if ord(char_at) < 32 or ord(char_at) > 126:
                    break # ASCII 이외 제거
            url_text = decoded_body[start_idx:idx]
            if len(url_text) < 8:
                continue
            #print(url_text)
            #url_text = url_text.split('?')[0] # 파라미터 제거
            url_listup_org.append(url_text)
            for skip_at in url_skip:
                url_text = url_text.replace(skip_at,"")
            try:
                doamin_text = url_text.split(scheme)[1]
                doamin_text = doamin_text.replace(':', ' ')
                doamin_text = doamin_text.replace('/', ' ')
                doamin_text = doamin_text.split(' ')[0]
                doamin_text = doamin_text.split('@')[-1]
                domain_listup.append(doamin_text)
            except Exception as e:
                #LOG(LOG_INF, "TODO: Add", exception=e)
                pass
            if len(url_text) < 8:
                continue
            url_listup.append(url_text)
    #print(decoded_body)
    for idx, url_at in enumerate(url_listup_org):
        #print("!![%d/%d] %d %s" % (idx, len(url_listup_org), len(decoded_body), url_at))
        if ' ' in url_at:
            continue
        decoded_body = decoded_body.replace(url_at, ' ')

    return decoded_body, url_listup, domain_listup

#@jit
def do_word_split(decoded_body):
    re_hangule  = re.compile("^[\u3131-\u318E\uAC00-\uD7A3]*$")
    re_alphabet = re.compile("^[a-zA-Z]*$")
    re_number   = re.compile("^[0-9]*$")
    re_bar      = re.compile("^[-_]*$")

    re_must     = [re_hangule, re_alphabet, re_number]
    re_list     = [re_hangule, re_alphabet, re_number, re_bar]
    re_language = [{"language": "hangule", "data" : re_hangule}, {"language" : "alphabet", "data" : re_alphabet}]

    result_list = []

    if type(decoded_body) != str:
        return [], [], []

    # 1. URL 분리
    decoded_body, url_listup, domain_listup = listup_urls(decoded_body)

    decoded_body = decoded_body.lower()
    
    #print(decoded_body)

    # 2. 개행 제거
    for char in remove_char:
        decoded_body = decoded_body.replace(char, '')

    # 3. 텝 치환
    for char in split_char:
        decoded_body = decoded_body.replace(char, ' ')

    # 4. 구분자 치환
    for char in split_char_inc:
        decoded_body = decoded_body.replace(char, char + ' ')
 
    # 5. 문자열 토큰화
    decoded_body = decoded_body.split(' ')
    for word in decoded_body:
        if word in delete_if_include:
            continue
        ln_special = 0
        # 5.1 특수문자로만 이루어져있으면 skip
        for special_at in special_char:
            if special_at in word:
                ln_special += 1
        if len(word) == ln_special:
            continue
    
        # 5.2 너무 잛은 문자 재외
        if len(word) < min_word_len:
            continue

        # 5.3 알파뱃, 한글, 숫자가 아닌것 기준으로 split
        f_skip        = False
        sub_text      = ""
        sub_text_list = []
        for idx, char in enumerate(word):
            n_checked = 0
            for re_item in re_list:
                if re_item.match(char) != None:
                    n_checked += 1
            if n_checked == 0:
                f_skip = True
            if f_skip == False:
                sub_text += char
            if f_skip == True or idx + 1 == len(word):
                f_skip = False
                if len(sub_text) != 0:    
                    #print("word: %s , sub_text: %s" % (word, sub_text))
                    if len(sub_text) >= min_word_len and len(sub_text) <= max_word_len:
                        #result_list.append(sub_text)
                        sub_text_list.append(sub_text)
                    sub_text = ""

        # 5.4 언어별로 분리 한다.
        sub_text       = ""
        old_language   = None
        sub_text_list2 = []
        for word in sub_text_list:
            sub_text     = ""
            old_language = None
            #print("-------------------")
            for idx, char in enumerate(word):
                for language_dict in re_language:
                    language = language_dict["language"]
                    re_item = language_dict["data"]
                    #print("idx=%d char=%s %s %s" % (idx, char, language, re_item.match(char)))
                    if re_item.match(char) != None:
                        if old_language != language and idx != 0:
                            #print(sub_text)
                            if len(sub_text) >= min_word_len and len(sub_text) <= max_word_len:
                                sub_text_list2.append(sub_text)
                            sub_text = ""
                            old_language = language
                            break
                        elif idx == 0:
                            old_language = language
                sub_text += char
        if sub_text != "":
            if re_bar.match(sub_text) == None:
                if len(sub_text) >= min_word_len and len(sub_text) <= max_word_len:
                    sub_text_list2.append(sub_text)

        # 6. 불용어를 제거한다.
        #sub_text_list = []
        #for word in sub_text_list2:
        #    word = remove_stopword(word)
        #    if len(word) > 1:
        #        sub_text_list.append(word)

        result_list += sub_text_list

    return result_list, url_listup, domain_listup


def main():
    result_word = remove_stopword("123454한글2입 을  is the of man organization Top the니다ABCD")
    print(result_word)

if "__main__" == __name__:
    main()



