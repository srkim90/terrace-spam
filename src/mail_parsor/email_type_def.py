'''
  Filename : email_type_def.py
  Release  : 1
  Date     : 2020-04-01
 
  Description : type define of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/04/01 created 
'''


# CATEGORY <같은 CATEGORY내에서는 출력 형식이 동일하다.>
CONTENT_CAT_TEXT                = 1000 # text/plain, text/html
CONTENT_CAT_IMAGE               = 2000 # image/jpeg, image/png
CONTENT_CAT_DOC                 = 3000 # application/msword, application/pdf ...
CONTENT_CAT_ARCHIVE             = 4000 # application/zip, application/x-rar-compressed
CONTENT_CAT_UNKNOWN             = 9999 # default, all of not defined

# SUB-CATEGORY
CONTENT_SUBCAT_TEXT_PLAIN       = CONTENT_CAT_TEXT      + 1  # text/plain
CONTENT_SUBCAT_TEXT_HTML        = CONTENT_CAT_TEXT      + 2  # text/html <Text만 추출 한다.>

CONTENT_SUBCAT_IMAGE_JPGE       = CONTENT_CAT_IMAGE     + 1  # *.jpeg, *.jpg
CONTENT_SUBCAT_IMAGE_PNG        = CONTENT_CAT_IMAGE     + 2  # *.png
CONTENT_SUBCAT_IMAGE_GIF        = CONTENT_CAT_IMAGE     + 3  # *.gif
CONTENT_SUBCAT_IMAGE_BMP        = CONTENT_CAT_IMAGE     + 4  # *.bmp
CONTENT_SUBCAT_IMAGE_ISO9660    = CONTENT_CAT_IMAGE     + 5  # *.img (?)

CONTENT_SUBCAT_DOC_MSWORD_COMP  = CONTENT_CAT_DOC       + 1  # *.doc  <MS Compound>
CONTENT_SUBCAT_DOC_MSWORD_ZIP   = CONTENT_CAT_DOC       + 2  # *.docx <ZIP>
CONTENT_SUBCAT_DOC_PDF          = CONTENT_CAT_DOC       + 3  # *.pdf

CONTENT_SUBCAT_ARCHIVE_ZIP      = CONTENT_CAT_ARCHIVE   + 1  # *.zip
CONTENT_SUBCAT_ARCHIVE_TAR      = CONTENT_CAT_ARCHIVE   + 2  # *.tar
CONTENT_SUBCAT_ARCHIVE_RAR      = CONTENT_CAT_ARCHIVE   + 3  # *.rar
CONTENT_SUBCAT_ARCHIVE_TAR_GZ   = CONTENT_CAT_ARCHIVE   + 4  # *.tar.gz

# Content Type parsing rule table
content_type_map = [
    # CATEGORY            # SUB-CATEGORY                   # type of mime header            # *.xxx
## Text
    ( CONTENT_CAT_TEXT,     CONTENT_SUBCAT_TEXT_PLAIN,      "text/plain"                    ,  None             ),
    ( CONTENT_CAT_TEXT,     CONTENT_SUBCAT_TEXT_HTML,       "text/html"                     ,  ".html"          ),

## Images
    ( CONTENT_CAT_IMAGE,    CONTENT_SUBCAT_IMAGE_PNG,       "image/png"                     ,  ".png"           ),
    ( CONTENT_CAT_IMAGE,    CONTENT_SUBCAT_IMAGE_JPGE,     ("image/jpg",\
                                                            "image/jpeg")                   , (".jpg", ".jpeg") ),
    ( CONTENT_CAT_IMAGE,    CONTENT_SUBCAT_IMAGE_GIF,       "image/gif"                     ,  ".gif"           ),
    ( CONTENT_CAT_IMAGE,    CONTENT_SUBCAT_IMAGE_BMP,       "image/bmp"                     ,  ".bmp"           ),
   #( CONTENT_CAT_IMAGE,    CONTENT_SUBCAT_IMAGE_ISO9660,   "application/x-iso9660-image"   ,  None             ),

## Documents
    ( CONTENT_CAT_DOC,      CONTENT_SUBCAT_DOC_MSWORD_COMP, None                            , (".doc", ".xls", ".ppt", ".hwp")        ),
    ( CONTENT_CAT_DOC,      CONTENT_SUBCAT_DOC_MSWORD_ZIP,  None                            , (".docx", ".xlsx" , ".pptx", "pdf",)       ),
    ( CONTENT_CAT_DOC,      CONTENT_SUBCAT_DOC_PDF,         "application/pdf"               ,  None         ),

## Archives
    ( CONTENT_CAT_ARCHIVE,  CONTENT_SUBCAT_ARCHIVE_ZIP,    ("application/zip",
                                                            "application/x-zip-compressed") ,  ".zip"       ),
    ( CONTENT_CAT_ARCHIVE,  CONTENT_SUBCAT_ARCHIVE_TAR_GZ,  None                            ,  ".tar.gz"    ),
    ( CONTENT_CAT_ARCHIVE,  CONTENT_SUBCAT_ARCHIVE_TAR,     None                            ,  ".tar"       ),
    ( CONTENT_CAT_ARCHIVE,  CONTENT_SUBCAT_ARCHIVE_RAR,    ("application/x-rar",\
                                                            "application/x-rar-compressed") ,  None         ),
]

embedding_name_list = [
    "Word-List" ,
    "URL-List"  ,
    "Domin-List",
]

def synap_image_type_to_sub_cat(synap_type):

    type_mapper = {
                    "png"   : CONTENT_SUBCAT_IMAGE_PNG, 
                    "tiff"  : None, 
                    "gif"   : CONTENT_SUBCAT_IMAGE_GIF, 
                    "jpg"   : CONTENT_SUBCAT_IMAGE_JPGE, 
                    "bmp"   : CONTENT_SUBCAT_IMAGE_BMP, 
                    "wmf"   : None, 
                    "emf"   : None, 
                    "wdp"   : None,
                    }

    synap_type = synap_type.lower()
    synap_type = synap_type.replace(".","")
    if synap_type in type_mapper.keys():
        return type_mapper[synap_type]
    return None


