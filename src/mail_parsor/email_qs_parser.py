#!/bin/env python
# -*- coding: utf-8 -*-

'''
  Filename : email_qs_parser.py at mail_parsor
  Release  : 1
  Date     : 2020-03-30
   
  Description : qs extractor of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/03/30 created 
'''

# common package import
import re
import os
import sys
import time
import gzip
import copy
import json
import quopri
import base64
import codecs
import chardet
import traceback

from common import *

example_data = '''
head=#
	qid=:1583574797.816721.140660501878528.mail
	crtime=:1583574797
	mftime=:1583574797
	subject=:Order Inquiry
	orgsubject=\\
		*Order Inquiry
		*
	end-orgsubject
	mailhub=:mail.terracetech.com
	clientip=:175.115.94.137
	rcv_cnt=:1
	subj_cnt=:1
	msgid=:20200305203412.D03ABA56A9D67750@ml1.net
	mimever=:1.000000
	headlen=:1358
	mimetype=:1
	mimesubtype=:103
	mimeencoding=:4
	spamrate=:0.000000
	encodedsubj=:Order Inquiry
	subjcharset=:US-ASCII
	subjcsnum=:401
	valid=:5
	decsubjcs=:EUC-JP
	viruschecked=:1
	submission=:1
	msgtype=:109
end-head
msginfoes=#
	no-msginfo=:1
	msginfo=#
		subjectvaluep=:1062
		subjectvaluelen=:15
		dummysubjp=:1077
		dummysubjlen=:80
	end-msginfo
end-msginfoes
sndr=#
	address=:vtru101@ml1.net
	realaddress=:vtru101@ml1.net
	headfrom=:vtru101@ml1.net
	personal_part=:Valerie Trumbull.
end-sndr
rcpts=#
	no-r=:1
	r=#
		address=:terracespamadm@terracetech.com
		realaddr=:terracespamadm@terracetech.com
		rcvedaddr=:terracespamadm@terracetech.com
		isqueried=:1
		exist=:1
		mailhost=:mail.terracetech.com
		mailhostid=:TMUzAwMDAwMEEtMDY1
		msgstore=:/opt/TerraceTims/mindex/1/73/73
		forwardmode=:2
		delivery_mode=:1
		quota_warnmsg=:1
		quota_warnrto=:90
		quota_violate_mode=:1
		quota_overlook_rto=:10
		delivery_noti=:1
		routingport=:7777
		userseq=:73
		domseq=:1
		orgaddr=:terracespamadm@terracetech.com
		company_Id=:1
		quota=#
			mesgsize=:10240
		end-quota
	end-r
end-rcpts
stats=#
	no-st=:0
end-stats
'''

qsHdrTypedef = {
        # head
        "qid"               : str,      # "1583574797.816721.140660501878528.mail",
        "crtime"            : int,      # "1583574797",
        "mftime"            : int,      # "1583574797",
        "subject"           : str,      # "Order Inquiry",
        "orgsubject"        : str,      # "Order Inquiry",
        "mailhub"           : str,      # "mail.terracetech.com",
        "clientip"          : str,      # "175.115.94.137",
        "rcv_cnt"           : int,      # "1",
        "subj_cnt"          : int,      # "1",
        "msgid"             : str,      # "20200305203412.D03ABA56A9D67750@ml1.net",
        "mimever"           : float,    # "1.000000",
        "headlen"           : int,      # "1358",
        "mimetype"          : int,      # "1",
        "mimesubtype"       : int,      # "103",
        "mimeencoding"      : int,      # "4",
        "spamrate"          : float,    # "0.000000",
        "encodedsubj"       : str,      # "Order Inquiry",
        "subjcharset"       : str,      # "US-ASCII",
        "subjcsnum"         : int,      # "401",
        "valid"             : int,      # "5",
        "decsubjcs"         : str,      # "EUC-JP",
        "viruschecked"      : int,      # "1",
        "submission"        : int,      # "1",
        "msgtype"           : int,      # "109"

        # msginfoes
        "no-msginfo"        : int,      # "1",
        "subjectvaluep"     : int,      # "1062",
        "subjectvaluelen"   : int,      # "15",
        "dummysubjp"        : int,      # "1077",
        "dummysubjlen"      : int,      # "80"

        # sndr
        "address"           : str,      # "vtru101@ml1.net",
        "realaddress"       : str,      # "vtru101@ml1.net",
        "headfrom"          : str,      # "vtru101@ml1.net",
        "personal_part"     : str,      # "Valerie Trumbull."
        "realaddr"          : str,

        # rcpts
        "no-r"              : int,      # "1"
        #"address"          : str,      # "terracespamadm@terracetech.com",
        #"realaddr"         : str,      # "terracespamadm@terracetech.com",
        "rcvedaddr"         : str,      # "terracespamadm@terracetech.com",
        "isqueried"         : int,      # "1",
        "exist"             : int,      # "1",
        "mailhost"          : str,      # "mail.terracetech.com",
        "mailhostid"        : str,      # "TMUzAwMDAwMEEtMDY1",
        "msgstore"          : str,      # "/opt/TerraceTims/mindex/1/73/73",
        "forwardmode"       : int,      # "2",
        "delivery_mode"     : int,      # "1",
        "quota_warnmsg"     : int,      # "1",
        "quota_warnrto"     : int,      # "90",
        "quota_violate_mode": int,      # "1",
        "quota_overlook_rto": int,      # "10",
        "delivery_noti"     : int,      # "1",
        "routingport"       : int,      # "7777",
        "userseq"           : int,      # "73",
        "domseq"            : int,      # "1",
        "orgaddr"           : str,      # "terracespamadm@terracetech.com",
        "company_Id"        : int,      # "1",
        "mesgsize"          : int,      # "10240"

        # stats
        "no-st"             : int,      # "0"

        # ???
        "spamlevel"         : int,      # 3
        "spamrulename"      : str,      # heuristic_filtered
        "qrtype"            : int,      # 112
        "qrreason"          : str,      # a:h
        "spamcheckedby"     : int,      # 104
        "taggedspam"        : int,      # 1
        "noreturn"          : int,      # 1
        "hsttp"             : int,      # 1
}

class qsHeaderParse:
    def __init__(self, qs_lines):
        self.idx = 0
        if type(qs_lines) == str:
            qs_lines = qs_lines.replace("\r","")
            qs_lines = qs_lines.split("\n")
        self.qs_lines = qs_lines

    def __get_next_line(self):
        if self.idx >= len(self.qs_lines):
            return None
        self.idx += 1
        return self.qs_lines[self.idx-1]

    def __check_line_info(self, line):
        level           = 0
        tag             = None
        value           = None
        is_group        = False
        is_multi_line   = False
        is_end          = False
        if len(line) < 2:
            return None
        for char in line:
            if char != "\t":
                break
            level += 1
        line  = line[level:]
        line  = line.split("=")
        tag   = line[0]
        try:
            value = line[1]
        except Exception as e: 
            #LOG(LOG_DBG, "TODO: Add", exception=e)
            value = None
        
        if value == '#':
            is_group = True
            return level, tag, value, is_group, is_multi_line, is_end
        elif value == '\\':
            is_multi_line = True
            value = None
            return level, tag, value, is_group, is_multi_line, is_end
        elif value == None and tag[0:4] == 'end-':
            is_end = True
            return level, tag, value, is_group, is_multi_line, is_end
        elif value== None or len(value) == 0:
            return None
        elif value[0] == ':':
            value = value[1:]
            return level, tag, value, is_group, is_multi_line, is_end
        else:
            tag = None
            value = line[0]
            return level, tag, value, is_group, is_multi_line, is_end

    def __in_qs_parse(self):
        qs_header  = {}
        group_name = None
        while True:
            line = self.__get_next_line()
            if line == None:
                break
            #print (line)
            result = self.__check_line_info(line)
            if result == None:
                continue
            level, tag, value, is_group, is_multi_line, is_end = result
            if is_end == True:
                break
            if is_group == True:
                value = self.__in_qs_parse()
            if is_multi_line == True:
                value = self.__in_qs_parse()
                continue
            if type(value) == str:
                if tag in qsHdrTypedef.keys():
                    value = qsHdrTypedef[tag](value)
                else:
                    #print("%s : %s" % (tag, value))
                    pass
            qs_header[tag] = value
                    
        return qs_header

    def qs_header_parse(self):
        qs_header = {}
        while self.idx != len(self.qs_lines):
            line = self.__get_next_line()
            if line == None:
                continue
            result = self.__check_line_info(line)
            if result == None:
                continue
            level, tag, value, is_group, is_multi_line, is_end = result
            if is_group == False:
                continue
            value = self.__in_qs_parse()
            qs_header[tag] = value
        return qs_header

def main():
    e = qsHeaderParse(example_data)
    result = e.qs_header_parse()
    jDumps = json.dumps(result, indent=4, ensure_ascii=False)
    print(jDumps)

if __name__ == "__main__":
    main()
