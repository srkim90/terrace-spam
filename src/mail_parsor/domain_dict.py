#!/bin/env python
# -*- coding: utf-8 -*-

'''
  Filename : email_parser.py at mail_parsor
  Release  : 1
  Date     : 2020-03-30
   
  Description : mime extractor of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/03/30 created 
'''

# common package import
import re
import os
import sys
import time
import gzip
import copy
import json
import socket
import traceback
import datetime 
import threading
import geoip2.database

# local package import
from common import *
from email_search import *

N_WORK_THREAD   = 32

class domainDict:
    def __init__(self):
        self.base_dir   = get_directory_path("parsed_mails")
        self.async_lock = threading.Semaphore(1)
        if self.base_dir == None:
            return None
        self.domain_dict = {
                # {
                #   "count"         : 1,
                #   "ipaddr"        : "11.222.55.89"
                #   "country"       : "JP"
                #   "continent"     : "Asia"
                #   "_prefix_len"   : 16
                # }
            }
        self.domain_list = None
        self.now_idx = 0
        self.objGeoIP = geoip2.database.Reader(get_directory_path() + "/geoip/GeoLite2-Country/GeoLite2-Country.mmdb") 

    def get_domain_ipaddr(self, string_domain):
        if string_domain in self.domain_dict.keys():
            try:
                if self.domain_dict[string_domain]["ipaddr"] != None:
                    return self.domain_dict[string_domain]["ipaddr"]
            except:
                pass
        try:
            ipaddr = socket.gethostbyname(string_domain)
        except:
            ipaddr = None
        return ipaddr

    def __query_domain_th(self, idx=0):
        while True:
            continent     = None
            country       = None
            prefix_len    = None
            string_domain = None
            self.async_lock.acquire()
            if len(self.domain_list) > self.now_idx:
                string_domain = self.domain_list[self.now_idx]
                self.now_idx += 1
            self.async_lock.release()
            if string_domain == None:
                break
            ipaddr = self.get_domain_ipaddr(string_domain)
            if ipaddr != None:
                try:
                    geo_result = self.objGeoIP.country(ipaddr)
                    continent  = geo_result.continent.code
                    country    = geo_result.country.iso_code
                    prefix_len = geo_result.traits._prefix_len
                except:
                    pass
            self.async_lock.acquire()
            self.domain_dict[string_domain]["ipaddr"]       = ipaddr
            self.domain_dict[string_domain]["continent"]    = continent
            self.domain_dict[string_domain]["country"]      = country
            self.domain_dict[string_domain]["_prefix_len"]  = prefix_len
            print("[%d/%d] %s --> %s" % (self.now_idx, len(self.domain_list), string_domain, ipaddr))
            self.async_lock.release()
        return

    def do_update_ipaddr_parsed_mails(self, start_yyyymmdd=None, end_yyyymmdd=None, file_list=None):
        insert_pos = ["mail-feature", "url-analyze"]
        file_list  = self.get_file_list(start_yyyymmdd, end_yyyymmdd, file_list)
        for json_path in file_list:
            info_dict = {}
            json_odj = open_json_file(json_path)
            if json_odj == None:
                continue
            if json_odj["body-items"] != None:
                for content_dict in json_odj["body-items"]:
                    if content_dict["Category"] != 1000:
                        continue
                    if "Domin-List" not in content_dict or content_dict["Domin-List"] == None or len(content_dict["Domin-List"]) == 0:
                        continue
                    for string_domain in content_dict["Domin-List"]:
                        try:
                            info_dict[string_domain]["count"] += 1
                        except:
                            info_dict[string_domain] = {}
                            info_dict[string_domain]["count"]       = 1
                            #info_dict[string_domain]["ipaddr"] = self.get_domain_ipaddr(string_domain)  
                            info_dict[string_domain]["ipaddr"]      = None
                            info_dict[string_domain]["continent"]   = None
                            info_dict[string_domain]["country"]     = None
                            info_dict[string_domain]["_prefix_len"] = None
                            try:
                                info_dict[string_domain]["ipaddr"]      = self.domain_dict[string_domain]["ipaddr"]
                                info_dict[string_domain]["continent"]   = self.domain_dict[string_domain]["continent"]
                                info_dict[string_domain]["country"]     = self.domain_dict[string_domain]["country"]
                                info_dict[string_domain]["_prefix_len"] = self.domain_dict[string_domain]["_prefix_len"]
                            except:
                                pass
            json_odj[insert_pos[0]][insert_pos[1]] = info_dict
            if save_json_file(json_path, json_odj) == True:
                print("Add domain ipaddr : %s" % (json_path,))
        return

    def get_file_list(self, start_yyyymmdd=None, end_yyyymmdd=None, file_list=None):
        if file_list != None:
            return file_list
        elif start_yyyymmdd != None and end_yyyymmdd == None:
            search = emailSearch(self.base_dir, start_yyyymmdd, start_yyyymmdd)
        elif start_yyyymmdd != None and end_yyyymmdd != None:
            search = emailSearch(self.base_dir, start_yyyymmdd, end_yyyymmdd)
        else:
            return []
        file_list = []
        search_result = search.list_files()
        for item in search_result:
            file_list.append(item[0])
        return file_list

    def mk_domaon_dict(self, start_yyyymmdd=None, end_yyyymmdd=None, file_list=None):
        file_list = self.get_file_list(start_yyyymmdd, end_yyyymmdd, file_list)
        for json_path in file_list:
            #print(json_path)
            json_odj = open_json_file(json_path)
            if json_odj == None:
                continue
            if json_odj["body-items"] == None:
                continue
            #print(json_odj)
            for content_dict in json_odj["body-items"]:
                if content_dict["Category"] != 1000:
                    continue
                if "Domin-List" not in content_dict or content_dict["Domin-List"] == None or len(content_dict["Domin-List"]) == 0:
                    continue
                for string_domain in content_dict["Domin-List"]:
                    try:
                        self.domain_dict[string_domain]["count"] += 1
                    except:
                        self.domain_dict[string_domain] = {}
                        self.domain_dict[string_domain]["count"]  = 1
                        self.domain_dict[string_domain]["ipaddr"] = None #self.get_domain_ipaddr(string_domain)  
        self.domain_list = list(self.domain_dict.keys())
        lt_thread = []
        self.now_idx = 0
        for idx in range(N_WORK_THREAD):
            if N_WORK_THREAD == 1:
                self.__query_domain_th()
                break
            else:
                hThread = threading.Thread(target=self.__query_domain_th, args=(idx,))
                hThread.daemon = True
                hThread.start()
                lt_thread.append(hThread)

        for item in lt_thread:
            item.join()
        return



def main():
    obj = domainDict()
    obj.mk_domaon_dict(start_yyyymmdd="20200330", end_yyyymmdd="20200330")
    obj.do_update_ipaddr_parsed_mails(start_yyyymmdd="20200330", end_yyyymmdd="20200330")
        
if __name__ == "__main__":
    main()
