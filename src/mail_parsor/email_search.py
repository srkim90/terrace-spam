#!/bin/env python
# -*- coding: utf-8 -*-

'''
  Filename : email_parser.py at mail_parsor
  Release  : 1
  Date     : 2020-02-19
   
  Description : mime extractor of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/02/19 created 
'''

# common package import
import re
import os
import sys
import time
import gzip
import copy
import json
import traceback
#import datetime 

from common import *

# email package import

'''
{
    yyyymmdd : {
        terracespamadm : ["aaa.eml", "bbb.eml", ...]
        terracehamadm  : ["ccc.eml", "ddd.eml", ...]
    }
}
'''

class emailSearch:
    def __init__(self, base_dir, start_yyyymmdd=None, end_yyyymmdd=None, list_file_path=None):
        self.base_dir   = base_dir
        if list_file_path != None and os.path.exists(list_file_path):
            self.search_result = {None:{"terracespamadm":[]}}
            with open(list_file_path, "r") as fd:
                lines = fd.readlines()
                for line in lines:
                    line = line.split("#")[0]
                    line = line.replace("\r", "")
                    line = line.replace("\n", "")
                    line = line.replace("\t", "")
                    line = line.strip()
                    if os.path.exists(line) == False:
                        continue
                    self.search_result[None]["terracespamadm"].append(line)
            if len(self.search_result[None]["terracespamadm"]) == 0:
                print("Error. Not exist email file in : %s" % (list_file_path,))
                raise
            return
        if start_yyyymmdd == None:
            start_date  = (datetime.now() - timedelta(2)).date()
        else:
            yyyy = int(start_yyyymmdd[0:4])
            mm   = int(start_yyyymmdd[4:6])
            dd   = int(start_yyyymmdd[6:8])
            start_date      = date(yyyy, mm, dd) # 20190818
            start_date      = (start_date - timedelta(1))
        if end_yyyymmdd == None:
            end_date    = (datetime.now() - timedelta(1)).date()
        else:
            yyyy = int(end_yyyymmdd[0:4])
            mm   = int(end_yyyymmdd[4:6])
            dd   = int(end_yyyymmdd[6:8])
            end_date    = date(yyyy, mm, dd) # 20190818
        self.date_list  = [(end_date - timedelta(days=x)).strftime("%Y%m%d") for x in range((end_date - start_date).days)] 
        #print(self.date_list)
        self.search_result = self.search()
         
    def list_files(self, check_type=None, param_yyyymmdd=None): # spam_type : terracespamadm or terracehamadm
        total_list = []
        for yyyymmdd in self.search_result.keys():
            if param_yyyymmdd != None and param_yyyymmdd != yyyymmdd:
                continue
            in_dict = self.search_result[yyyymmdd]
            for spam_type in in_dict.keys():
                if check_type != None and check_type != spam_type:
                    continue
                for file_name in in_dict[spam_type]:
                    total_list.append((file_name, yyyymmdd))
        return total_list

    def list_days(self):
        return list(self.search_result.keys())

    def search(self):
        eml_dict    = {}
        check_list  = ["terracespamadm", "terracehamadm"]
        base_dir_list = self.base_dir
        if type(base_dir_list) != list:
            base_dir_list = [base_dir_list,]
        for __base_dir in base_dir_list:
            for yyyymmdd in self.date_list:
                in_dict = {}
                ln_file = 0
                for item in check_list:
                    try:
                        eml_list = []
                        base_dir = "%s/%s/%s" % (__base_dir, item, yyyymmdd)
                        in_dict[item] = self.__search(base_dir, eml_list)
                        ln_file += len(eml_list)
                        #print(in_dict[item])
                    except Exception as e:
                        #LOG(LOG_INF, "TODO: fail to search : %s" % e, exception=e)
                        #print("Error. %s" % (e,))
                        continue
                try:
                    others_list = os.listdir(__base_dir,)
                except NotADirectoryError as e:
                    continue
                #for yyyymmdd in others_list:
                #    if yyyymmdd in check_list or len(yyyymmdd) != 8:
                #        continue
                #    now_dir = "%s/%s" % (__base_dir, yyyymmdd)
                #    if os.path.isdir(now_dir) == False:
                #        continue
                if yyyymmdd in others_list and ln_file == 0:
                    eml_list = [] 
                    base_dir = "%s/%s" % (__base_dir, yyyymmdd)
                    tmp_result = self.__search(base_dir, eml_list)
                    in_dict["terracehamadm"]  = []
                    in_dict["terracespamadm"] = []
                    for qs_name in tmp_result:
                        ext_type = qs_name.split(".")[-1].lower()
                        if ext_type == "qs":
                            in_dict["terracehamadm"].append(qs_name)
                        elif ext_type == "eml":
                            in_dict["terracespamadm"].append(qs_name)
                        else:
                            print("Error.!!!!! Unknown type of qs name")
                    ln_file += len(eml_list)

                print("base_dir=%s yyyymmdd=%s ln_file=%d" % (base_dir, yyyymmdd, ln_file))
                try:
                    eml_dict[yyyymmdd]["terracespamadm"] += in_dict["terracespamadm"]
                    eml_dict[yyyymmdd]["terracehamadm"]  += in_dict["terracehamadm"]
                    #print("BBBBB" % eml_dict[yyyymmdd])
                except KeyError as e:
                    if yyyymmdd not in eml_dict.keys():
                        eml_dict[yyyymmdd] =  in_dict
                    pass
        return eml_dict

    def __search(self, base_dir, eml_list):
        filenames = os.listdir(base_dir)
        for filename in filenames:
            full_filename = os.path.join(base_dir, filename)
            if os.path.isdir(full_filename) == True:
                self.__search(full_filename, eml_list)
            elif ".qs" in filename or ".json" in filename or ".eml" in filename:
                ext = filename[-3:].lower()
                if ext == ".qs" or ext == ".gz" or ext == "son" or ext == "eml":
                    eml_list.append(full_filename)
        return eml_list

    @staticmethod
    def get_raw_mails_dir(base_dir):
        new_dir  = "%s/raw_mails" % (base_dir,)
        new_list = []
        if os.path.exists(new_dir) == False:
            is_hamadm   = os.path.isdir(base_dir + "/terracehamadm")
            is_spamadm  = os.path.isdir(base_dir + "/terracespamadm")
            if "parsed_mails" in base_dir and is_hamadm == False and is_spamadm == False:
                new_dir = base_dir
            else:
                return base_dir
        filenames = os.listdir(new_dir)
        for filename in filenames:
            full_filename = os.path.join(new_dir, filename)
            new_list.append(full_filename)
        return new_list

def main():
    #base_dir = "/srkim/mnt/hdd250G/maildata/parsed_mails"
    try:
        base_dir = os.environ['MAIL_DATA_HOME']
    except Exception as e:
        LOG(LOG_WAR, "TODO: Add", exception=e)
        print("Not exist environment : 'MAIL_DATA_HOME'")
        return None
    e = emailSearch(base_dir, start_yyyymmdd=None, end_yyyymmdd=None)
    for eml_file in e.list_files():
        print(eml_file)
    #print("result : %s" % result)
    return


        
if __name__ == "__main__":
    main()
