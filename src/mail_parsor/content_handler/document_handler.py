'''
  Filename : document_handler.py
  Release  : 1
  Date     : 2020-04-06
 
  Description : document handler of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/04/06 created 
'''
# common package import
import os
import io
import sys
import json

# image handing package import
sys.path.append(".")
sys.path.append("../3th")
sys.path.append("../synap_helper")
sys.path.append("./content_handler")
sys.path.append("../attachment_analyzer")
import oletools
import ole_analyzer
import oletools.oleid
from oletools.olevba import VBA_Parser, TYPE_OLE, TYPE_OpenXML, TYPE_Word2003_XML, TYPE_MHTML # need 'python -m pip install colorclass'
from synap_helper import *

# local package import 
from common import *
from handler_base import *
from email_type_def import *

class documentHandler(handlerBase):
    def __init__(self, binary_data, sub_category, content_name):
        super(documentHandler,self).__init__(binary_data, sub_category, content_name)

        # document info
        self.document_info = {}
 
    def do_analyze_all(self):
        route_items = [
                [ self.__do_analyze_msdoc_comp  , CONTENT_SUBCAT_DOC_MSWORD_COMP    , None ],
                [ self.__do_analyze_msdoc_zip   , CONTENT_SUBCAT_DOC_MSWORD_ZIP     , None ],
                [ self.__do_analyze_pdf         , CONTENT_SUBCAT_DOC_PDF            , None ],
            ]
        if super(documentHandler,self).do_analyze_all(route_items) == None:
            self.document_info = None


    def __do_analyze_msdoc_comp(self):
        ole = ole_analyzer.OleAnalyzer()
        result = ole.in_handle_vba(self.binary_data)
        #synap_doc_info = synap_get_document_info(self.binary_data)
        synap_vba_info = synap_get_vbs_info(self.binary_data)
        self.document_info = {}
        self.document_info["content"]   = result
        self.document_info["synap_vba"] = synap_vba_info
        return result

    def __do_analyze_msdoc_zip(self):
        document_info = synap_get_document_info(self.binary_data)
        self.document_info["content"] = document_info

    def __do_analyze_pdf(self):
        document_info = synap_get_document_info(self.binary_data)
        self.document_info["content"] = document_info

    def get_result(self):
        return self.document_info

def main():
    file_name = "example_0043.doc"
    #file_name = "example.doc"
    with open(file_name, "rb") as fd:
        rawAll = fd.read()
    e = documentHandler(rawAll, CONTENT_SUBCAT_DOC_MSWORD_COMP, file_name)
    e.do_analyze_all()
    result = e.get_result()

    print(json.dumps(result, indent=4, ensure_ascii=False))

if __name__ == "__main__":
    main()


