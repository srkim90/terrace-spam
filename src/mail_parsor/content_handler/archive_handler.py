'''
  Filename : archive_handler.py
  Release  : 1
  Date     : 2020-04-01
 
  Description : archive handler of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/04/01 created 
'''
# common package import
import os
import io
import sys
sys.path.append("../3th/librarfile")

# archive handing package import
import zipfile
import rarfile
import tarfile

# local package import 
from email_type_def import *

MAX_CHECK_FILE_CNT = 32

class archiveHandler:
    def __init__(self, binary_data, sub_category, content_name):
        self.extension2        = None
        self.sub_category      = sub_category
        self.binary_data       = binary_data
        self.content_name      = content_name
        self.extension         = content_name.split(".")[-1].lower()
        self.archive_file_list = None
        if len(content_name.split(".")) >= 3:
            self.extension2    = content_name.split(".")[-2].lower()
 
    def do_analyze_all(self):
        bRet         = None
        is_try_zip   = False
        extension    = self.extension
        sub_category = self.sub_category

        # 우선, 확장자를 본다.
        if extension == "zip":
            is_try_zip = True
            bRet = self.__do_analyze_zip()
        elif extension == "rar":
            bRet = self.__do_analyze_rar()
        elif extension == "tar":
            bRet = self.__do_analyze_tar()
        elif extension == "gz" and self.extension2 == "tar":
                self.binary_data = gzip.decompress(self.binary_data)
                bRet = self.__do_analyze_tar()
        else: # 확장자에 지원 하는 아카이브 형식이 없을 경우, sub_category 코드를 본다.        
            if sub_category == CONTENT_SUBCAT_ARCHIVE_ZIP:
                is_try_zip = True
                bRet = self.__do_analyze_zip()
            elif sub_category == CONTENT_SUBCAT_ARCHIVE_TAR:
                bRet = self.__do_analyze_tar()
            elif sub_category == CONTENT_SUBCAT_ARCHIVE_TAR_GZ:
                self.binary_data = gzip.decompress(self.binary_data)
                bRet = self.__do_analyze_tar()
            elif sub_category == CONTENT_SUBCAT_ARCHIVE_RAR:
                bRet = self.__do_analyze_rar()
            else:
                return None
        # 모든 시도가 실패 하였을 경우, zip을 시도해본다.
        if bRet != True and is_try_zip == False:
            is_try_zip = True
            bRet = self.__do_analyze_zip()

        return True

    def __do_analyze_zip(self):
        try:
            stories_zip = zipfile.ZipFile(io.BytesIO(self.binary_data), "r")
        except Exception as e:
            return False
        self.archive_file_list = []
        for idx,info in enumerate(stories_zip.infolist()):
            if len(self.archive_file_list) >= MAX_CHECK_FILE_CNT:
               break 
            info_dict = {
                "filename"          : info.filename,
                #"compress_type"     : info.compress_type,
                "file_size"         : info.file_size,
                "compress_size"     : info.compress_size,
            }
            self.archive_file_list.append(info_dict)
        stories_zip.close()
        return True

    def __do_analyze_tar(self):
        try:
            stories_tar = tarfile.open(fileobj=io.BytesIO(self.binary_data))
        except Exception as e:
            return False
        self.archive_file_list = []
        for idx,info in enumerate(stories_tar.getmembers()):
            #print(dir(info))
            #continue
            if len(self.archive_file_list) >= MAX_CHECK_FILE_CNT:
               break 
            info_dict = {
                "filename"          : info.name,
                "file_size"         : info.size,
                "compress_size"     : None,
            }
            self.archive_file_list.append(info_dict)
        stories_tar.close()
        return True

    def __do_analyze_rar(self):
        try:
            stories_rar = rarfile.RarFile(io.BytesIO(self.binary_data), errors="strict")
        except Exception as e:
            return False
        self.archive_file_list = []
        for idx,info in enumerate(stories_rar.infolist()):
            if len(self.archive_file_list) >= MAX_CHECK_FILE_CNT:
               break 
            info_dict = {
                "filename"          : info.filename,
                "file_size"         : info.file_size,
                "compress_size"     : info.compress_size,
            }
            self.archive_file_list.append(info_dict)
        stories_rar.close()
        return True

    def get_result(self):
        return self.archive_file_list
