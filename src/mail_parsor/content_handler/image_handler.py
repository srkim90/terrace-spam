'''
  Filename : image_handler.py
  Release  : 1
  Date     : 2020-04-02
 
  Description : image handler of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/04/02 created 
'''
# common package import
import os
import io
import sys

# image handing package import
#from PIL import *
from PIL import Image

# local package import 
from common import *
from handler_base import *
from email_type_def import *

class imageHandler(handlerBase):
    def __init__(self, binary_data, sub_category, content_name):
        super(imageHandler,self).__init__(binary_data, sub_category, content_name)

        # image info
        self.image_info = {}
 
    def do_analyze_all(self):
        route_items = [
                [ self.__do_analyze_jpge  ,   CONTENT_SUBCAT_IMAGE_JPGE   , None ], 
                [ self.__do_analyze_png   ,   CONTENT_SUBCAT_IMAGE_PNG    , None ],
                [ self.__do_analyze_gif   ,   CONTENT_SUBCAT_IMAGE_GIF    , None ],
                [ self.__do_analyze_bmp   ,   CONTENT_SUBCAT_IMAGE_BMP    , None ],
            ]
        super(imageHandler,self).do_analyze_all(route_items)

        try:
            #print(self.content_name)
            #print("%s" % (self.binary_data,))
            im = Image.open(io.BytesIO(self.binary_data))
        except Exception as e:
            LOG(LOG_INF, "fail in Image.open", exception=e)
            self.image_info = None
            return
 
        self.image_info["width"]  = im.size[0]
        self.image_info["height"] = im.size[1]
        
    def __do_analyze_jpge(self):
        pass

    def __do_analyze_png(self):
        pass

    def __do_analyze_gif(self):
        pass

    def __do_analyze_bmp(self):
        pass

    def get_result(self):
        return self.image_info
