'''
  Filename : handler_base.py
  Release  : 1
  Date     : 2020-04-02
 
  Description : base class of handler of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/04/02 created 
'''
# common package import
import os
import io
import sys
from abc import * 

# local package import 
from email_type_def import *

class handlerBase(metaclass = ABCMeta):
    def __init__(self, binary_data, sub_category, content_name):
        self.extension2        = None
        self.sub_category      = sub_category
        self.binary_data       = binary_data
        self.content_name      = content_name
        self.extension         = content_name.split(".")[-1].lower()
        self.archive_file_list = None
        if len(content_name.split(".")) >= 3:
            self.extension2    = content_name.split(".")[-2].lower()

    def do_analyze_all(self, route_items):
        for route in route_items:
            func                = route[0]
            check_sub_category  = route[1]
            check_extension     = route[2]
            if check_sub_category != None and check_sub_category == self.sub_category:
                return func()
            if check_extension != None and self.extension == check_extension:
                return func()
        return None

    def get_result(self):
        pass
