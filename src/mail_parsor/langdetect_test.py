#!/bin/env python
# -*- coding: utf-8 -*-

'''
  Filename : langdetect_test.py
  Release  : 1
  Date     : 2020-02-19
   
  Description : mime extractor of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/02/19 created 
'''

# common package import
import re
import os
import sys
import time
import gzip
import copy
import json
import quopri
import base64
import codecs
import chardet
import traceback
import langdetect

#need "python -m pip install langdetect"

def main():
    test_set = [
                "AAA1",
                "BBB2",
                "cCC3",
                "111hang",
                "김성래",
                "가나다라밤",
                "特殊文字のみ構成されている場合、",
                "shshrka001@gmail.com  shshrka001@gmail.com",
                "https://www.geeksforgeeks.org/check-if-email-address-valid-or-not-in-python/ https://www.geeksforgeeks.org/check-if-email-address-valid-or-not-in-python/"
            ]



    re_dict = {
        "re_ko"   : re.compile('[\u1100-\u11FF\u3130-\u318F\uAC00-\uD7AF]'),
        "re_jp"   : re.compile('[\u3040-\u309F\u30A0-\u30FF\u31F0-\u31FF]'),
        "re_ch"   : re.compile('[\u2e80-\u2eff\u31c0-\u31ef\u3200-\u32ff\u3400-\u4dbf\u4e00-\u9fbf\uf900-\ufaff]'),
        "re_num"  : re.compile('[0-9]'),
        "re_en"   : re.compile('[a-zA-Z]'),
        "re_urls" : re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)'),
        "re_mail" : re.compile('(\w+[.|\w])*@(\w+[.])*\w+')
    }    

    for item in test_set:
        #report = "%s : %s" % (langdetect.detect(item), item)
        report = ""
        print(item)
        for re_type in re_dict.keys():
            re_obj = re_dict[re_type]
            report += " ,%s=%s" % (re_type, len(re_obj.findall(item)))
        print(report)


if __name__ == "__main__":
    main()


