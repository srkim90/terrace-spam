#!/bin/env python
# -*- coding: utf-8 -*-

'''
  Filename : email_header_parser.py at mail_parsor
  Release  : 1
  Date     : 2020-02-23
   
  Description : mime extractor of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/02/23 created 
'''

# common package import
import re
import os
import sys
import time
import json
import quopri
import base64
import codecs
import chardet
import traceback
import langdetect
import geoip2.database
#python -m pip install geoip2

# local package import
from common import *
from word_split import *

'''
{
    # 동일한 header가 2개 이상 일 경우, value를 list로 한다.
    "received"      : [ (from, ["110.9.209.145", "110.9.209.142,"... ]), (by, [...]), (with, [...]), (for, [...]), (date, []) ]
    "content-type"  : [ (None, "text/plain"), ("name", "aaa.png") ]
}
'''

class emailHeaderParser:
    def __init__(self, header_list, check_list=None):
        # header_list : [(key1, values), (key2, values), ..]
        # e.g.        : [, .., ("Content-Type", "text/plain; charset=utf-8; format=flowed; DelSp=Yes"), ..]
        global bash_path
        #STACK()    
        #print(header_list)

        try:
            bash_path = os.environ['MAIL_DATA_HOME']
        except:
            print("Not exist environment : 'MAIL_DATA_HOME'")
            raise

        self.check_list  = check_list
        self.header_list = header_list
        self.result_dict = None
        self.header_origin = {
            "Content-Type"      : self.__handle_content_type,   # tags: None, name, ...
            "Received"          : self.__handle_received,       # tags: from, by, with, for, date
            "Subject"           : self.__handle_subject,        # tags: None
            "From"              : self.__handle_mail_id,
            "To"                : self.__handle_mail_id,
            "Cc"                : self.__handle_mail_id,
            "Bcc"               : self.__handle_mail_id,
            "Reply-To"          : self.__handle_mail_id,
            "Return-Path"       : self.__handle_mail_id,
            "Return-Receipt-To" : self.__handle_mail_id,
            "User-Agent"        : self.__handle_user_agent,
            "X-Mailer"          : self.__handle_x_mailer,
            "Message-Id"        : self.__handle_message_id,
            "Date"              : self.__handle_date,
            "DKIM-Signature"    : self.__handle_dkim_sig,
            "Received-SPF"      : self.__handle_received_spf,
        }
        self.header_dict = {}
        for hdr_name in self.header_origin.keys():# 소문자로 변환하여 넣는다.
            self.header_dict[hdr_name.lower()] = self.header_origin[hdr_name]
        self.objGeoIP = None
        self.__do_value_parsing()

    def __hdr_result_add_to_dict(self, header, parsed):
        if header not in self.result_dict.keys():
            self.result_dict[header] = self.__hdr_result_to_dict(parsed)
        else:
            result_at = self.__hdr_result_to_dict(parsed)
            item = self.result_dict[header]
            for item_key in item:
                if type(item[item_key]) != list:
                    item[item_key] = [item[item_key],]
                if result_at[item_key] != list:
                    result_at[item_key] = [result_at[item_key],]
                item[item_key] += result_at[item_key]

    def __hdr_result_to_dict(self, parsed):
        if parsed == None or len(parsed) == 0:
            return None
        elif len(parsed) == 1 and hasattr(parsed[0], '__iter__') and parsed[0][0] == None:
            return parsed[0][1]
        else:
            result_dict = {}
            for pair in parsed:
                key_name = pair[0]
                value    = pair[1]
                if key_name == None:
                    key_name = "__defaule__"
                result_dict[key_name] = value
            return result_dict

    def __convert_lower_hdrname_to_origin(self, lower_name):
        for origin_name in self.header_origin.keys(): # 소문자로 변환 된 헤더 이름으로 원 이름을 알아낸다.
            if origin_name.lower() == lower_name:
                return origin_name
        return lower_name

    def get_header_name_list(self):
        hdr_names = []
        for header_set in self.header_list:
            header, str_value = header_set
            hdr_names.append(header)
        return hdr_names

    def get_values(self):
        return self.result_dict

    def get_value_from_header_name(self, header, tag=None):
        if self.result_dict == None:
            return None
        lower_header = header.lower()
        #print(": %s" % lower_header)
        real_header = None
        for __real_header in self.result_dict.keys():
            if __real_header.lower() == lower_header:
                real_header = __real_header
                break
        if real_header == None or self.result_dict[real_header] == None:
            return None
        find_value = self.result_dict[real_header] 
        if type(find_value) != dict and (tag == None or tag == "__defaule__"):
            return find_value
        elif type(find_value) != dict and tag != None:
            return None
        if tag != None:
            for __tag in find_value.keys():
                value     = find_value[__tag]
                if tag.lower() == __tag.lower():
                    return value
            return None
        return find_value

    def __parser_tag_vaule(self, str_value):
        result_list = []
        str_value = str_value.replace("\r", " ")
        str_value = str_value.replace("\n", " ")
        str_value = str_value.replace("\t", " ")
        str_value = str_value.strip()
        str_value = str_value.split(";")
        for item in str_value:
            item = item.strip()
            if item.count('=') == 1:
                item  = item.split("=")
                tag   = item[0].strip().lower()
                value = item[1]
            else:
                tag   = None
                value = item
            value = value.strip()
            value = value.replace("\"", "")
            value = value.replace("'", "")
            result_list.append((tag, value))
        return result_list

    def __handle_mail_id(self, header, str_value):
        header  = self.__convert_lower_hdrname_to_origin(header.lower()) # e.g. "Yong Yu 于勇 <Yong.Yu@hongzhuangyuan.com>"
        # Visi Vertikal <visi.vertikal@gmail.com>, Wahyu Darwin <darwin@piwar.co.id>, Tsubee (Soebchansyah) <tsubee@caturteknikmandiri.co.id>
        org_value = str_value
        str_value = str_value.replace("\"", "")
        str_value = str_value.replace("\'", "")
        name_list   = []
        domain_list = []
        uid_list    = []

        str_value_list = str_value.split(",")
        for str_value in str_value_list:
            name        = None
            domain      = None
            uid         = None
            if "<" not in str_value or ">" not in str_value: 
                uid  = str_value # 메일 주소만 있을 때
            else:
                uid  = str_value.split("<")[1].split(">")[0]
                name = str_value.replace(uid, "").replace("<","").replace(">","").strip()
            if uid != None and '@' in uid:
                    domain = uid.split('@')[-1]
            if name == "":
                name = None
            name_list.append(name)
            domain_list.append(domain)
            uid_list.append(uid)

        if len(name_list) == 0:
            name_list = None
        elif len(name_list) == 1:
            name_list = name_list[0]
        if len(domain_list) == 0:
            domain_list = None
        elif len(domain_list) == 1:
            domain_list = domain_list[0]
        if len(uid_list) == 0:
            uid_list = None
        elif len(uid_list) == 1:
            uid_list = uid_list[0]

        self.__hdr_result_add_to_dict(header, [("all", org_value), ("name", name_list), ("uid", uid_list), ("domain", domain_list)])
        

    def __handle_subject(self, header, str_value):
        re_dict = { 
            "n_include_num"   : re.compile('[0-9]'),
            "n_include_en"    : re.compile('[a-zA-Z]'),
            "n_include_ko"    : re.compile('[\u1100-\u11FF\u3130-\u318F\uAC00-\uD7AF]'),
            "n_include_jp"    : re.compile('[\u3040-\u309F\u30A0-\u30FF\u31F0-\u31FF]'),
            "n_include_ch"    : re.compile('[\u2e80-\u2eff\u31c0-\u31ef\u3200-\u32ff\u3400-\u4dbf\u4e00-\u9fbf\uf900-\ufaff]'),
            "n_include_url"   : re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)'),
            "n_include_email" : re.compile('[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}'),
        } 
        header = self.__convert_lower_hdrname_to_origin(header.lower())
        detected_charset = ""
        str_value = str(str_value)
        string_token = do_word_split(str_value)[0]
        total_result = [("all", str_value), ("charset", detected_charset), ("token", string_token)]
        for re_type in re_dict.keys():
            re_obj = re_dict[re_type]
            total_result.append((re_type ,len(re_obj.findall(str_value))))
        self.result_dict[header] = self.__hdr_result_to_dict(total_result)

    def __handle_dkim_sig(self, header, str_value):
        header = self.__convert_lower_hdrname_to_origin(header.lower())
        str_value = str_value.replace("\t","")
        str_value = str_value.replace(" ","")
        tag_vals  = str_value.split(";")
        result_list = []
        for item in tag_vals:
            tag   = item.split("=")[0]
            value = item[len(tag)+1:]
            result_list.append((tag, value))
        self.result_dict[header] = self.__hdr_result_to_dict(result_list)

    def __handle_user_agent(self, header, str_value):
        header = self.__convert_lower_hdrname_to_origin(header.lower())
        self.result_dict[header] = self.__hdr_result_to_dict([(None, str_value)])

    def __handle_received_spf(self, header, str_value):
        header = self.__convert_lower_hdrname_to_origin(header.lower())
        str_value = str_value.strip().split(" ")[0].lower()
        checked_val = None
        spf_unknown = "Unknown"
        received_spf = ["Fail", "None", "Pass", "Softfail", "Neutral", spf_unknown]
        for item in received_spf:
            if item.lower() == str_value:
                checked_val = item
                break
        if checked_val == None:
            checked_val = spf_unknown
        self.result_dict[header] = self.__hdr_result_to_dict([(None, checked_val)])

    def __handle_x_mailer(self, header, str_value):
        header = self.__convert_lower_hdrname_to_origin(header.lower())
        self.result_dict[header] = self.__hdr_result_to_dict([(None, str_value)])

    def __handle_message_id(self, header, str_value):
        header = self.__convert_lower_hdrname_to_origin(header.lower())
        str_value = str_value.replace("<","")
        str_value = str_value.replace(">","")
        self.result_dict[header] = self.__hdr_result_to_dict([(None, str_value)])

    def __handle_date(self, header, str_value):
        # 25.01.2020 11:12:15 (AST)
        # 23 Feb 2020 17:44:13 -0800
        # Wed, 19 Feb 2020 07:45:16 +0900
        # Fri, 21 Feb 2020 13:59:41 +0900 (KST)
        # 날짜 포맷 찍어주는방법이 여러가지임.. 귀찮게.
        str_value = str_value.replace(",", "")
        str_value = str_value.replace(".", " ")
        str_value = str_value.replace(":", " ")
        header = self.__convert_lower_hdrname_to_origin(header.lower())
        time_list = []
        for item in str_value.split(" "):
            if len(item) < 2:
                continue
            if item.isdigit() == True:
                item = int(item)
            else:
                item = item.lower()
                month_list = ["jan","feb","mar","apr","may","jun","jul","aug","sep","oct","nov","dec"]
                try:
                    item = month_list.index(item) + 1
                except:
                    continue
            time_list.append(item)
                    
        if len(time_list) != 6:
            str_value = None
        else:
            str_value = "%d%02d%02d_%02d%02d%02d" % (time_list[2], time_list[1], time_list[0], time_list[3], time_list[4], time_list[5])
        self.result_dict[header] = self.__hdr_result_to_dict([(None, str_value)])

    def __handle_content_type(self, header, str_value):
        header      = self.__convert_lower_hdrname_to_origin(header.lower())
        result_list = self.__parser_tag_vaule(str_value)
        if result_list != None:
            self.result_dict[header] = self.__hdr_result_to_dict(result_list)

    def __handle_received(self, header, str_value):
        def __parser_by_addrs(str_line):
            separator = ["(", ")"]
            if "([" in str_line:
                separator = ["([", "])"]
            elif "[" in str_line:
                separator = ["[", "]"]
            elif separator[0] not in str_line:
                return None
            return str_line.split(separator[0])[1].split(separator[1])[0]

        def __parser_received_addrs(str_line):
            result_list = []
            if '[' not in str_line or ']' not in ']':
                if '(' in str_line and ')' in str_line:
                    str_line = str_line.replace("(", "[")
                    str_line = str_line.replace(")", "]")
                else:
                    return None
            str_line = str_line.split("[")
            for str_st in str_line:
                ipaddr = str_st.split("]")[0].strip()
                result_list.append(ipaddr)

            return result_list
        #print("-----------\n%s\n--------------" % str_value)
        header      = self.__convert_lower_hdrname_to_origin(header.lower())
        result_list = self.__parser_tag_vaule(str_value)
        if result_list == None:
            return
        separators = ["from", "by", "with", "for"]
        parsed_dict = self.__split_as_items(result_list[0][1])
        #print(parsed_dict)
        #parsed_dict = {'from': 'KEIT-DDEI (unknown [127.0.0.1])', 'by': 'DDEI (Postfix)', 'with': 'SMTP id 130051B65852', 'for': '<parkorea@keit.re.kr>'}

        result_dict = {}
        for key in separators:
            value = None
            if key not in parsed_dict.keys():
                result_dict[key] = None
                continue
            if key == "from":
                value = __parser_received_addrs(parsed_dict[key])
                if value != None:
                    value = value[-1]
                    value = self.__get_geoip_info(value)
                    #print("IPADDR=%s" % (value,))
                #if value == None:
                #    print(str_value)
                result_dict["from_form"] = False
                if parsed_dict[key].count("[") == 2 and parsed_dict[key].count("([") == 1:
                    result_dict["from_form"] = True
            elif key == "by":
                value = parsed_dict[key]
                if value != None and len(value) != 0:
                    by_ipaddr = __parser_by_addrs(value)
                    value = value.split(" ")[0]
                    result_dict["by_ipaddr"] = by_ipaddr
            elif key == "with":
                pass
            elif key == "for":
                value = parsed_dict[key]
                #print("value=%s" % (value,))
                if "<" in value and ">" in value:
                    value = value.split("<")[1].split(">")[0]
                else:
                    value = None
                #result_dict["for"] = value
                #continue
            else:
                pass
            result_dict[key] = value
        if result_dict["from"] == None:
            return
        if result_dict["from"] != None and result_dict["by"] != None:
            result_dict["from"]["by"] = result_dict["by"]
            result_dict["from"]["from_form"] = result_dict["from_form"]
        if "for" in result_dict.keys():
            result_dict["from"]["for"] = result_dict["for"]
        if "by_ipaddr" in result_dict.keys():
            result_dict["from"]["by_ipaddr"] = result_dict["by_ipaddr"]
        if header not in self.result_dict.keys():
            result_hdr = []
            #for key in separators:
            #    result_hdr.append([result_dict[key]))
            result_hdr.append(result_dict["from"])
            self.result_dict[header] = result_hdr
        else:
            result_hdr = self.result_dict[header]
            result_hdr.append(result_dict["from"])
        return

    def __handle_defaule(self, header, str_value):
        pass
 
    def get_ipaddr_country(self, ipaddr):
        geoip_info = self.__get_geoip_info(ipaddr)
        if geoip_info == None:
            return None
        return geoip_info["country"]

    def __get_geoip_info(self, ipaddr):
        contry = None
        if ipaddr == None:
            return contry
        contry = {"ipaddr"     : ipaddr,
                  "feature"    : None,
                  "continent"  : None, 
                  "country"    : None}

        # https://geoip2.readthedocs.io/en/latest/#geoip2.records.Traits
        traits_list = ["autonomous_system_number",
                "autonomous_system_organization",
                "connection_type",
                "domain",
                "is_anonymous",
                "is_anonymous_proxy",
                "is_anonymous_vpn",
                "is_hosting_provider",
                "is_legitimate_proxy",
                "is_public_proxy",
                "is_satellite_provider",
                "is_tor_exit_node",
                "isp",
                "organization",
                "static_ip_score",
                "user_type",
                "user_count",
                "ip_address",
                "_network",
                "_prefix_len",]

        try:
            if self.objGeoIP == None:
                self.objGeoIP = geoip2.database.Reader(bash_path + "/geoip/GeoLite2-Country/GeoLite2-Country.mmdb")
            geo_result      = self.objGeoIP.country(ipaddr)
            #print(geo_result)
            continent_code   = geo_result.continent.code
            country_code     = geo_result.country.iso_code
            continent_name  = geo_result.continent.names["en"]
            country_name    = geo_result.country.names["en"]
            contry["country"]    = country_code   #"%s;%s" % (country_code, country_name)
            contry["continent"]  = continent_code #"%s;%s" % (continent_code, continent_name) 
            contry["feature"] = {}
            for traits_item in traits_list:
                try:
                    traits = getattr(geo_result.traits, traits_item)
                except:
                    traits = None
                contry["feature"][traits_item] = traits
        except geoip2.errors.AddressNotFoundError as e:
            pass
        except:
            pass
        return contry

    def __split_as_items(self, string):
        # [input]
        #   string      : from SN6PR08MB4287.com ([fe80::c07c:52f2:dc36:42a7]) by SN6PR7.namprd08.com ([fe80::c07c:52f2:dc36:42a7%4]) with mapi id 15.20.2474.022
        #                 from unknown (HELO mail.kpsec.co.kr) (172.20.10.230) by 172.20.10.231 with ESMTP
        #                 from [172.20.10.231] ([172.20.10.231]) by mail.kpsec.co.kr ([172.20.10.230]) with ESMTP id 1585142898.662895.mail for <leo.great@kakaopaysec.com>
        #   separators  : ["from", "by", "with", "for"]
        # [output]
        #   {
        #        from    : 'SN6PR08MB4287.com ([fe80::c07c:52f2:dc36:42a7])',
        #        by      : 'SN6PR7.namprd08.com ([fe80::c07c:52f2:dc36:42a7%4])',
        #        with    : 'mapi id 15.20.2474.022',
        #   }
        result_dict = {}
        separators  = ["from", "by", "with", "for"]
        for separ_at in separators:
            if "%s " % separ_at not in string:
                continue
            string_at = " %s" % string
            string_at = string_at.split("%s " % separ_at)[1]
            for separ_in in separators:
                if separ_in == separ_at:
                    continue
                if separ_in in string_at:
                    string_at = string_at.split("%s " % separ_in)[0]
            string_at = string_at.strip()
            result_dict[separ_at] = string_at
        #print(result_dict)
        return result_dict


    def __do_value_parsing(self):
        self.result_dict = {}
        for header_set in self.header_list:
            header, str_value = header_set
            header_lower = header.lower()
            handle_fn = self.__handle_defaule
            #print("%s %s" % (header_lower, type(str_value)))
            if self.check_list != None:
                if header_lower not in self.check_list:
                    continue
            if header_lower in self.header_dict.keys():
                handle_fn = self.header_dict[header_lower] 

            #print("41 %s %s" % (header, str_value))
            handle_fn(header, str_value)
            #print("42")
        for header in self.header_origin.keys():
            if header not in self.result_dict.keys():
                self.result_dict[header] = None

        return self.result_dict









