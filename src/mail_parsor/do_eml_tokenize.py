#!/bin/env python
# -*- coding: utf-8 -*-

'''
  Filename : email_parser.py at mail_parsor
  Release  : 1
  Date     : 2020-02-19
   
  Description : mime extractor of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/02/19 created 
'''

# common package import
import re
import os
import sys
import time
import gzip
import copy
import json
import psutil
import datetime 
import traceback
import threading
from multiprocessing.pool import Pool
from struct import unpack
from socket import AF_INET, inet_pton

# local package import
from common import *
from domain_dict import *
from email_parser import *
from email_search import *

base_dir        = None
g_local_addrs   = None
g_result_list   = []
N_WORK_THREAD   = get_env_value("N_WORK_PROC") # default : 2

def get_filelist_from_log(base_path, yyyymmdd, classfy):
    result_all = []
    full_path = "%s/%s.log" % (base_path, yyyymmdd)
    if os.path.exists(full_path) == False:
        full_path += ".gz"
        if os.path.exists(full_path) == False:
            return None
    if ".gz" == full_path[-3:]:
        fd = gzip.open(full_path, "rb")
    else:
        fd = open(full_path, "rb")
    lines = fd.readlines()
    fd.close()
    
    for line in lines:
        line = line.replace(b"\r", b"")
        line = line.replace(b"\n", b"")

        if "wmss-routed" in base_path:
            if b"MC:normal" in line and b"ACT:delivered" in line:
                # 사설 & invalid 한 ip일 경우 스킵
                sip = line.split(b"SIP:")[1].split(b" ")[0]
                sip = sip.decode("utf-8")
                if is_ipaddr_is_internal(sip) == True:
                    continue
                line = line.split(b" ")
                qs_filename = None
                for item in line:
                    if len(item) < 10:
                        continue
                    if item[:3] != b"QF:":
                        continue
                    qs_filename  = item[3:].decode("utf-8")
                    if os.path.exists(qs_filename) == False:
                        continue
                    qs_filename += "?%s" % classfy #terracehamadm
                    result_all.append(qs_filename)
        elif "wmtad" in base_path:
            # cat /tmwdata/log/wmtad/2020042*.log | grep "! MC:" | grep "spam\|suspected\|virus" | grep logmonitoring | awk -F "QF:" '{print $2}' | cut -d ' ' -f 1
            check_list = [b":spam", b":suspected-spam", b":virus"]
            if b"! MC:" not in line:
                continue
            is_find = False
            for check_item in check_list:
                if check_item in line:
                    is_find = True
                    break
            if is_find == False:
                continue
            if b"logmonitoring" not in line or b"QF:" not in line:
                continue
            sip = line.split(b"SIP:")[1].split(b" ")[0]
            sip = sip.decode("utf-8")
            if is_ipaddr_is_internal(sip) == True:
                continue
            qs_filename  = line.split(b"QF:")[1].split(b" ")[0]
            qs_filename  = qs_filename.decode("utf-8")
            if os.path.exists(qs_filename) == False:
                continue
            qs_filename += "?%s" % classfy #terracespamadm
            if ".eml" not in qs_filename:
                continue
            result_all.append(qs_filename)
    return result_all

def mail_tokenize_job(idx, eml_file, is_visual=False) :
    base_dir = os.environ['MAIL_DATA_HOME']
    common_init_log(base_dir, "email_parser", INFO)

    e = emailParser(eml_file)
    if e.load_all() == None:
        return None
    report = e.mk_mail_report()
    if is_visual == True:
        e.show_result(report)
    else:
        full_name = e.save_parsed_mail_info('%s/parsed_mails' % base_dir, report, do_gzip=get_env_value("IS_RESULT_GZ")) 
    return True

def print_progress(now_idx, ln_search_result,  yyyymmdd, eml_file, sz_mail, result_file):
        eml_file = eml_file.split("?")[0]
        print("[%d/%d] : %s %s (%dKB) --> %s" % (now_idx, len(search_result), yyyymmdd, eml_file, sz_mail, result_file))

def _mail_tokenize_th(idx):
    while True:
        eml_file, yyyymmdd = get_search_result_item()
        if eml_file == None:
            break
        result_file = emailParser.make_save_file_name("%s/parsed_mails" % base_dir, eml_file, get_env_value("IS_RESULT_GZ"))
        _eml_file = eml_file.split("?")[0]
        sz_mail     = int(get_mail_size(_eml_file) / 1024)
        print_progress(now_idx, len(search_result), yyyymmdd, eml_file, sz_mail, result_file)
        async_lock.acquire()
        g_result_list.append(result_file)
        async_lock.release()
        hProc = proc_pool.apply_async(mail_tokenize_job, [idx, eml_file])
        ret = hProc.get()
    PRINT("END Thread : idx=%d" % (idx,))
    return

def get_remain_count():
    async_lock.acquire()
    n_result = len(search_result) - now_idx
    async_lock.release()
    return n_result

def get_search_result_item():
    global now_idx
    async_lock.acquire()
    if now_idx >= len(search_result):
        async_lock.release()
        return None, None
    eml_file = search_result[now_idx][0]
    yyyymmdd = search_result[now_idx][1]
    now_idx += 1
    async_lock.release()
    return eml_file, yyyymmdd


def show_help():
    print("./eml_tokenize.sh [start_yyyymmdd] [end_yyyymmdd] --N-WORK-THREAD=4")
    print("./eml_tokenize.sh [start_yyyymmdd] [end_yyyymmdd]")
    print("./eml_tokenize.sh [start_yyyymmdd]")
    print("./eml_tokenize.sh xxxxxx.qs")
    print("./eml_tokenize.sh xxxxxx.eml")
    print("./eml_tokenize.sh xxxxxx.qs.gz")
    print("./eml_tokenize.sh xxxxxx.eml.gz")
    print("./eml_tokenize.sh --help")
    print("./eml_tokenize.sh -h")
    print("./eml_tokenize.sh")

def main():
    global now_idx
    global base_dir
    global proc_pool
    global async_lock
    global search_result
    global N_WORK_THREAD
    #common_init_log(base_dir, "do_eml_tokenize", INFO)
    start_yyyymmdd  = None
    end_yyyymmdd    = None
    now_idx         = 0
    async_lock      = threading.Semaphore(1)
    s_time          = datetime.now()
    base_dir = get_directory_path()
    if base_dir == None:
        return None
    list_file_name = None
    argvs = []
    list_from_log = None
    for idx,item in enumerate(sys.argv[1:]):
        if "-" != item[0]:
            argvs.append(item)
            continue
        if "-l" == item or "--log" == item:
            if "=" in item:
                log_yyyymmdd = item.split("=")[-1] 
            elif len(sys.argv) > 2+idx:
                log_yyyymmdd = sys.argv[2+idx]
            else:
                print("Invalid option : %s" % (item,))
                return None
            list_from_log  = []
            list_from_log += get_filelist_from_log("/tmwdata/log/wmss-routed", log_yyyymmdd, "terracehamadm")
            list_from_log += get_filelist_from_log("/tmwdata/log/wmtad", log_yyyymmdd, "terracespamadm")
            if list_from_log == None or len(list_from_log) == 0:
                print("file not exist : %s" % (log_yyyymmdd,))
                return None
        if "-f" == item or "--file" == item:
            if "=" in item:
                list_file_name = item.split("=")[-1] 
            elif len(sys.argv) > 2+idx:
                list_file_name = sys.argv[2+idx]
            else:
                print("Invalid option : %s" % (item,))
                return None
            break
        if "--help" == item or '-h' == item:
            return show_help()
        elif "--N-WORK-THREAD" in item:
            try:
                N_WORK_THREAD = int(item.split('=')[-1])
            except:
                print("Invalid option : %s" % (item,))
                return None
    __base_dir = emailSearch.get_raw_mails_dir(base_dir)
    if list_file_name == None and list_from_log == None:
        if len(argvs) >= 2:
            if len(argvs[0]) == 8 and len(argvs[1]) == 8:
                start_yyyymmdd  = argvs[0]
                end_yyyymmdd    = argvs[1]
        elif len(argvs) == 1:
            if os.path.exists(argvs[0]) == True:
                eml_file = argvs[0]   
                return mail_tokenize_job(0, eml_file, is_visual=True)
            else:
                start_yyyymmdd  = argvs[0]
                end_yyyymmdd    = argvs[0]

        search = emailSearch(__base_dir, start_yyyymmdd, end_yyyymmdd)
        search_result = search.list_files()
    elif list_from_log != None:
        search_result = []
        for item in list_from_log:
            search_result.append((item, log_yyyymmdd))
    else:
        search = emailSearch(__base_dir, list_file_path=list_file_name)
        search_result = search.list_files()
    lt_thread     = []
    if N_WORK_THREAD == 1:
        while True:
            eml_file, yyyymmdd = get_search_result_item()
            if eml_file == None:
                break
            _eml_file = eml_file.split("?")[0]
            sz_mail     = int(get_mail_size(_eml_file) / 1024)
            result_file = emailParser.make_save_file_name("%s/parsed_mails" % base_dir, eml_file, get_env_value("IS_RESULT_GZ"))
            print_progress(now_idx, len(search_result), yyyymmdd, eml_file, sz_mail, result_file)
            mail_tokenize_job(0, eml_file)
    else:
        proc_pool = Pool(processes=N_WORK_THREAD)
        for idx in range(N_WORK_THREAD):
            hThread = threading.Thread(target=_mail_tokenize_th, args=(idx,))
            hThread.daemon = True
            hThread.start()
            lt_thread.append(hThread)

        #for item in lt_thread:
        #    item.join()
        while True:
            if get_remain_count() <= 0:
                break
            else:
                sleep(1.0)
        sleep(1.0)
    e_time = datetime.now()
    PRINT("END Process : time=%s" % (e_time-s_time))

    #objDomain = domainDict()    
    #objDomain.mk_domaon_dict(file_list=g_result_list)
    #objDomain.do_update_ipaddr_parsed_mails(file_list=g_result_list)
    return
        
if __name__ == "__main__":
    main()

