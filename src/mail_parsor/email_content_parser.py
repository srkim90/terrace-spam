#!/bin/env python
# -*- coding: utf-8 -*-

'''
  Filename : email_content_parser.py at mail_parsor
  Release  : 1
  Date     : 2020-02-19
   
  Description : mime extractor of spam detect module
  
  Notes :
  ===================
  History
  ===================
  2020/02/19 created 
'''

# common package import
import re
import os
import sys
import time
import json
import quopri
import base64
import codecs
import chardet
import traceback

# email package import
import email
from email import policy
from email.header import decode_header

# 3th package import
sys.path.append("../3th")
sys.path.append("../synap_helper")
sys.path.append("./content_handler")
import html2text

# local package import
from common import *
from word_split import *
from email_type_def import *
from image_handler import *
from archive_handler import *
from document_handler import *
from email_header_parser import *

class emailContentParser:
    def __init__(self):
        self.body_mime_hdrs = []

    def detect_content_category(self, header_list):
        # header의 Text값을 보고 CATEGORY 를 찾는다.
        ContentName = None
        parsed      = emailHeaderParser(header_list)
        ContentType = parsed.get_value_from_header_name("Content-Type", tag="__defaule__")
        ContentName = parsed.get_value_from_header_name("Content-Type", tag="Name")
        #self.body_mime_hdrs.append(header_list)
        #print("ContentType=%s, ContentName=%s" % (ContentType, ContentName))
        ContentExt = None
        if ContentName != None and '.' in ContentName:
            ContentExt = "." + ContentName.lower().split(".")[-1]

        if ContentType == None:
            ContentType = "text/plain"
            ContentName =  None
        else:
            ContentType = ContentType.lower()

        for map_at in content_type_map:
            CATEGORY        = map_at[0]
            SUB_CATEGORY    = map_at[1]
            text_type       = map_at[2]
            text_ext        = map_at[3]
            if type(text_type) == str:
                text_type = (text_type,)
            if text_type == None:
                text_type = [None,]
            if type(text_ext) == str:
                text_ext = (text_ext,)
            if text_ext == None:
                text_ext = [None,]
            '''
            for type_at in text_type:
                #print("%s, %s" % (type_at, ContentType))
                if type_at == ContentType.lower() or (type_at == None and text_ext != None):
                    if text_ext != None:
                        if ContentName == None:
                            continue
                        if len(ContentName) <= len(text_ext):
                            continue
                        if ContentName.lower()[-1 * len(text_ext):] != text_ext: # 정의 된 확장자가 있는데, 다를 경우
                            continue
                    return (CATEGORY, SUB_CATEGORY, type_at, ContentName)
            '''
            for ext_at in text_ext:
                if ext_at == None or ContentExt == None:
                    continue
                if ext_at == ContentExt:
                    return (CATEGORY, SUB_CATEGORY, ContentType, ContentName)
            for type_at in text_type:
                if type_at == None:
                    continue
                if type_at == ContentType:
                    return (CATEGORY, SUB_CATEGORY, ContentType, ContentName)
        return (CONTENT_CAT_UNKNOWN, -1, ContentType, ContentName)

    def append_body_header(self,header_list):
        self.body_mime_hdrs.append(header_list)

    def get_body_info_list(self):
        result = []
        if len(self.body_mime_hdrs) == 0:
            return None
        for header_list in self.body_mime_hdrs:
            parsed = emailHeaderParser(header_list)
            ContentType = parsed.get_value_from_header_name("Content-Type", tag="__defaule__")
            ContentName = parsed.get_value_from_header_name("Content-Type", tag="Name")
            body_type = {
                    "Content-Type"  : ContentType,
                    "Attach-Name"   : ContentName,
                }
            result.append(body_type)
        return result

    def __content_text_parser(self, body_block, sub_category):
        report_data = {}

        if sub_category == CONTENT_SUBCAT_TEXT_HTML:
            # 1. 들어온 타입 그대로 변환 시도
            text_block = None
            try:
                text_block = html2text.html2text(body_block)
            except TypeError as e:
                pass
            
            # 2. 실해 했을 경우, 타입 변경하여 그대로 변환 시도 (민간요법 시도 -.-;)
            chdt = None
            if text_block == None:
                if type(body_block) == str:
                    body_block = body_block.encode("utf-8")
                elif type(body_block) == bytes:
                    chdt = chardet.detect(body_block)["encoding"]
                    body_block = body_block.decode(chdt)
                try:
                    text_block = html2text.html2text(body_block)
                except TypeError as e:
                    pass

            if text_block == None:
                if type(body_block) == str:
                    text_block = body_block
                else:
                    if chdt == None:
                        chdt = chardet.detect(body_block)
                    text_block = body_block.decode(chdt)
        else:
            text_block = body_block
        parsed = do_word_split(text_block)

        #report_data["Word-List" ] = parsed[0]
        #report_data["URL-List"  ] = parsed[1]
        #report_data["Domin-List"] = parsed[2]
        for idx, parsed_at in enumerate(parsed):
            report_data[embedding_name_list[idx]] = parsed_at

        return report_data

    def __content_image_parser(self, body_block, sub_category, content_name):
        parsed_image = None
        parsed = imageHandler(body_block, sub_category, content_name)

        parsed.do_analyze_all()
        parsed_image = parsed.get_result()

        report_data = parsed_image

        return report_data

    def __content_doc_parser(self, body_block, sub_category, content_name):
        parsed_doc  = None
        parsed = documentHandler(body_block, sub_category, content_name)
        parsed.do_analyze_all()
        parsed_doc = parsed.get_result()

        if parsed_doc == None:
            return None
    
        # Text 토큰화
        if parsed_doc["content"] != None:
            text_dict = {}
            if "text" in parsed_doc["content"].keys():
                parsed_text = do_word_split(parsed_doc["content"]["text"])
                for idx, parsed_at in enumerate(parsed_text):
                    text_dict[embedding_name_list[idx]] = parsed_at
                parsed_doc["content"]["text"] = text_dict

        # Image 정보추출
        if parsed_doc["content"] != None:
            if "attachment_list" in parsed_doc["content"].keys():
                for image_item in parsed_doc["content"]["attachment_list"]:
                    _format      = image_item["format"]
                    sub_category = synap_image_type_to_sub_cat(_format)
                    _data        = image_item["data"]
                    del image_item["data"]
                    image_item["MD5-Sum"] = md5sum(_data)
                    if sub_category == None:
                        continue
                    image_parsor = imageHandler(_data, sub_category, "AAA%s" % _format)
                    image_parsor.do_analyze_all()
                    parsed_image = image_parsor.get_result()
                    image_item.update(parsed_image)

        # Script 정보 추출
        if parsed_doc["content"] != None:
            if "ole_meta" in parsed_doc["content"]:
                ole_meta = parsed_doc["content"]["ole_meta"]

        report_data = parsed_doc
        return report_data

    def __content_archive_parser(self, body_block, sub_category, content_name):
        parsed_archive = None
        parsed = archiveHandler(body_block, sub_category, content_name)
        try:
            parsed.do_analyze_all()
            parsed_archive = parsed.get_result()
        except Exception as e:
            LOG(LOG_WAR, "fail in handling archive", exception=e)
            pass
        report_data = {
                "File-List" : parsed_archive
            }

        return report_data

    def __is_inline_image(self, part_item):
        inline_type = ["text/x-vcard", "text/*", "message/rfc822", "message/delivery-status"]
        for part in part_item:
            field_value = None
            field_name  = part[0].lower()
            if len(part) > 1:
                field_value = part[1].lower()
            if "content-id" == field_name:
                if field_value == None or field_value == "null" or field_value == "none":
                    return False
                return True
            elif "content-disposin" == field_name:
                if "inline" in field_value:
                    return True
            elif "content-type" == field_name:
                for check_type in inline_type:
                    if check_type in field_value:
                        return True
        return False

    def do_content_parser(self, headers, body_block, category, sub_category, content_type, content_name, part_item):
        report = {
            "Category"      : category,
            "Sub-Category"  : sub_category,
            "Content-Type"  : content_type,
        }

        __content_name = content_name if content_name != None else ""
    
        if category == CONTENT_CAT_TEXT:
            data = self.__content_text_parser(body_block, sub_category)
        elif category == CONTENT_CAT_IMAGE:
            data = self.__content_image_parser(body_block, sub_category, __content_name)
            report["is-inline"] = self.__is_inline_image(part_item) 
        elif category == CONTENT_CAT_DOC:
            data = self.__content_doc_parser(body_block, sub_category, __content_name)
        elif category == CONTENT_CAT_ARCHIVE:
            data = self.__content_archive_parser(body_block, sub_category, __content_name)
        else:
            data = None

        if category != CONTENT_CAT_TEXT:
            report["Attach-Name"]   = content_name
            report["Attach-Size"]   = len(body_block)
            report["MD5-Sum"]       = md5sum(body_block)

        if data != None:
            for key in data.keys():
                report[key] = data[key]    
        return report





