#!/bin/python
# -*- coding: utf-8 -*-

import os
import json
import time
import gzip
import base64
import socket
import random
import requests
from time import *
import threading

virustotal_apikeys = [
    ["test1@nomail.co.kr" ,"b119b2fd123ff6b3fe803dc2cc5e17d22303b501e9bb007c4fbb282615377471"],
    ["test2@nomail.co.kr" ,"6a2693dfaddae3565a622f7c28d9cb8eba3c648e94a8fc0ef27e9119e21735c6"],
    ["test3@nomail.co.kr" ,"05928767455bead67003ca688103ac8bf2d7f5c971c211f40b2ab24a50ac9787"],
    ["test4@nomail.co.kr" ,"1c34edb1f70b257929cd126efa1fa78955aaf30624b6ec00d0c95012f497221d"],
    ["test5@nomail.co.kr" ,"c30a1a5b6714cbcee5636b41eeecadbf9570d87b13cf8636123664dad0e59c7b"],
    ["test6@nomail.co.kr" ,"5f819f53c78f8b8a8103e5288812cabe2a0affd85c54f0c6938a35fde191e225"],
    ["test7@nomail.co.kr" ,"af0d8c6cde447fbdcb86af6ff11f8d75da39c48eee0c305e20c8ac84082004fc"],
    ["test8@nomail.co.kr" ,"fb354821b6b07ef74c82639691396762537b95266f4c4e8cacbc3be2b2f4cb80"],
    ["test9@nomail.co.kr" ,"1c0a3e465517d806da7644503cb0efa883cb31a3885126af0a8d56b9715d195f"],
    ["test10@nomail.co.kr","ff1fd80f9a146ea1c534d0af9429f96fe7560f7c17cf349f6bcbcf157d888adb"]]

N_WORK_THREAD = int(len(virustotal_apikeys) / 1)

class VirustotalHelper:
    def __init__(self):
        self.async_lock = threading.Semaphore(1)
        self.rr_index = random.randrange(0,len(virustotal_apikeys))
        self.have_script_list = []

    def navigate_day_json_files(self, input_yyyymmdd=None):
        self.yyyymmdd = input_yyyymmdd
        dirname = "%s/%s" % ("/home/mailadm/data/report", input_yyyymmdd)
        json_files = os.listdir(dirname)
        result_list = []
        for item in json_files:
            if ".json" not in item:
                continue
            json_fullname = "%s/%s" % (dirname, item)
            with open(json_fullname, "rb") as fd:
                jsonraw = fd.read()
            json_dict = json.loads(jsonraw)
            if "attachments" not in json_dict.keys():
                continue
            n_vba_file = 0
            for attachment in json_dict["attachments"]:
                if "report" not in attachment.keys():
                    continue
                if attachment["report"] == None:
                    continue
                if "n_vba_file" not in attachment["report"].keys():
                    continue
                n_vba_file += attachment["report"]["n_vba_file"]
            if n_vba_file == 0:
                continue
            #print(json_fullname)
            result_at = {
                "json_file" : json_fullname,
                "json_dict" : json_dict,
            }
            result_list.append(result_at)
        return result_list

    def __query_virus_scan(self, api_key, json_dict):
        qs_name = json_dict["qs_name"]
        header = {"x-apikey" : api_key}
        url = "https://www.virustotal.com/api/v3/files"
        with open(qs_name, "rb") as fd:
            upload = {'file':fd}
            response = requests.post(url, files=upload, headers=header)

        status_code = response.status_code
        response_dict = response.json()

        if status_code != 200:
            print("status_code:%d, response_dict:%s" % (status_code, response_dict))
            return None
        resource_id = response_dict['data']['id']

        url = "https://www.virustotal.com/api/v3/analyses/" + resource_id
        sleep(2.0)
        response_dict = None
        for idx in range(10):
            response = requests.get(url, headers=header)
            status_code = response.status_code
            print("status_code : %d, url:%s" % (status_code, url))
            if status_code == 200:
                response_dict = response.json()
                print(response_dict)
                break
            else:
                sleep(5.0)
        if response_dict == None:
            return None
        return response_dict

    def __get_api_key(self):
        self.async_lock.acquire()
        self.rr_index += 1
        self.async_lock.release()
        ln_key = len(virustotal_apikeys)
        return virustotal_apikeys[self.rr_index % ln_key][1]

    def set_have_script_list(self, have_script_list):
        self.have_script_list = have_script_list
        return

    def get_script_qs(self):
        self.async_lock.acquire()
        if len(self.have_script_list) == 0:
            self.async_lock.release()
            return None
        item = self.have_script_list[0]
        self.have_script_list = self.have_script_list[1:]
        self.async_lock.release()
        return item

    def work_thread(self, th_idx, api_key):
        while True:
            item = self.get_script_qs()
            if item == None:
                break
            json_file = item["json_file"]
            json_dict = item["json_dict"]
            virus_result = self.__query_virus_scan(api_key, json_dict)
            print(virus_result)
            break
            print("%s --> %s" % (json_file, json_dict["qs_name"])) 
        return

    def do_append_virustotal_result(self, have_script_list):
        self.set_have_script_list(have_script_list)

        lt_thread = []
        for idx in range(N_WORK_THREAD):
            api_key = self.__get_api_key()
            hThread = threading.Thread(target=self.work_thread, args=(idx,api_key))
            hThread.daemon = True
            hThread.start()
            lt_thread.append(hThread)

        for item in lt_thread:
            item.join()

        return

        #for item in have_script_list:
        #    json_file = item["json_file"]
        #    json_dict = item["json_dict"]
        #    #response = requests.post(url, data=datas)
        #    #print("%s --> %s" % (json_file, json_dict["qs_name"]))
        #    self.__query_virus_scan(api_key, json_dict)
        #    break

def main():
    yyyymmdd = "20200720"
    e = VirustotalHelper()
    have_script = e.navigate_day_json_files(yyyymmdd)
    e.do_append_virustotal_result(have_script)
    return





if __name__ == "__main__":
    main()


