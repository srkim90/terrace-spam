#include <map>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>
#include <pthread.h>

using namespace std;

#define _Bool int
typedef char tr_char;
typedef unsigned char tr_uchar;

/* define - color */
#define COL_RED "\033[0;31m"
#define COL_GRN "\033[0;32m"
#define COL_YEL "\033[0;33m"
#define COL_BLU "\033[0;34m"
#define COL_PUR "\033[0;35m"
#define COL_CIA "\033[0;36m"
#define COL_DIF "\033[0;37m"

#define COL_BRED "\033[1;31m"
#define COL_BGRN "\033[1;32m"
#define COL_BYEL "\033[1;33m"
#define COL_BBLU "\033[1;34m"
#define COL_BPUR "\033[1;35m"
#define COL_BCIA "\033[1;36m"


#include "snf.h"
#include "json.h"
#include <sys/stat.h>
#include <sys/types.h>

#define TRSRC __FILE__,__LINE__

/* define - lines */
//               012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
#define line40  "----------------------------------------"
#define LINE40  "========================================"
#define line50  "--------------------------------------------------"
#define LINE50  "=================================================="
#define line60  "------------------------------------------------------------"
#define LINE60  "============================================================"
#define line70  "----------------------------------------------------------------------"
#define LINE70  "======================================================================"
#define line80  "--------------------------------------------------------------------------------"
#define LINE80  "================================================================================"
#define line90  "------------------------------------------------------------------------------------------"
#define LINE90  "=========================================================================================="
#define line100 "----------------------------------------------------------------------------------------------------"
#define LINE100 "===================================================================================================="
#define line110 "--------------------------------------------------------------------------------------------------------------"
#define LINE110 "=============================================================================================================="
#define line120 "------------------------------------------------------------------------------------------------------------------------"
#define LINE120 "========================================================================================================================"

#if 1
#define ERRN(str, ...)   printf(str, ## __VA_ARGS__); printf("\n")
#define WARNN(str, ...)  printf(str, ## __VA_ARGS__); printf("\n")
#define INFON(str, ...)  printf(str, ## __VA_ARGS__); printf("\n")
#define DEBUGH(str, ...) printf(str, ## __VA_ARGS__); printf("\n")
#else
#define ERRN(str, ...)   printf("[ERRN]   : "); printf(str, ## __VA_ARGS__); printf("")
#define WARNN(str, ...)  printf("[WARNN]  : "); printf(str, ## __VA_ARGS__); printf("")
#define INFON(str, ...)  printf("[INFON]  : "); printf(str, ## __VA_ARGS__); printf("")
#define DEBUGH(str, ...) printf("[DEBUGH] : "); printf(str, ## __VA_ARGS__); printf("")
#define ERRN(str, ...)   printf(""); printf(str, ## __VA_ARGS__); printf("\n")
#define WARNN(str, ...)  printf(""); printf(str, ## __VA_ARGS__); printf("\n")
#define INFON(str, ...)  printf(""); printf(str, ## __VA_ARGS__); printf("\n")
#define DEBUGH(str, ...) printf(""); printf(str, ## __VA_ARGS__); printf("\n")
#endif

#define OUTPUT_PATH     "./out"

#define MAX_MD5         256
#define MAX_ATTACHMENT  64
#define MAX_FILE_NAME   128

#define MAX_INPUT       128

extern int f_write;
extern int f_spamfinger;
extern int f_information;
extern int n_input_file;
extern char * input_list[MAX_INPUT];
extern char * input_root[MAX_INPUT];
extern void  *Logh;



int mtrace_start_report(int ms_report_duration);
string mtrace_set_option_report_threshold_size(uint64_t n_size);
string mtrace_set_option_report_threshold_count(int n_count);

typedef struct document_attachment_info
{
    int           size;
    char          MD5[MAX_MD5];
    const char   *strFileFormat;
    int           formatCode;
#define MAX_SPAMFINGER  512
    char          SPAMFINGER[MAX_SPAMFINGER];
    char         *lsAttachmentData;  /* memory will be allocated by on_detectImage. */
} document_attachment_info_t;

typedef struct document_info
{
    char        root[MAX_FILE_NAME]; /* documents internal eml file */
    char        name[MAX_FILE_NAME];
    int         size;
    char        MD5[MAX_MD5];
    int         n_attachment;
    char       *text_buf;           /* memory will be allocated. */
    int         n_text_size;
    document_attachment_info_t attachments[MAX_ATTACHMENT];
} document_info_t;

typedef struct emlAttachmentInfo
{
    int             nData;
    unsigned char * pData;
    char            lsfileName[256];
} emlAttachmentInfo_t;


extern "C" { 
void synap_golbal_init();
const char * synap_get_attachments(char * pDocumentBuf, int iLen, char * lsFailName);
const char * synap_get_vba_script(char * pDocumentBuf, int iLen);
}

