#include "main.h"

int f_write;
int f_spamfinger;
int f_information;
int n_input_file;
char * input_list[MAX_INPUT];
char * input_root[MAX_INPUT];

void  *Logh = NULL;

static const unsigned char base64_table[65] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/**
 * base64_encode - Base64 encode
 * @src: Data to be encoded
 * @len: Length of the data to be encoded
 * @out_len: Pointer to output length variable, or %NULL if not used
 * Returns: Allocated buffer of out_len bytes of encoded data,
 * or %NULL on failure
 *
 * Caller is responsible for freeing the returned buffer. Returned buffer is
 * nul terminated to make it easier to use as a C string. The nul terminator is
 * not included in out_len.
 */
unsigned char * base64_encode(const unsigned char *src, size_t len,
                  size_t *out_len)
{
    unsigned char *out, *pos;
    const unsigned char *end, *in;
    size_t olen;
    int line_len;

    olen = len * 4 / 3 + 4; /* 3-byte blocks to 4-byte */
    olen += olen / 72; /* line feeds */
    olen++; /* nul termination */
    if (olen < len)
        return NULL; /* integer overflow */
    out = (unsigned char *)malloc(olen);
    if (out == NULL)
        return NULL;

    end = src + len;
    in = src;
    pos = out;
    line_len = 0;
    while (end - in >= 3) {
        *pos++ = base64_table[in[0] >> 2];
        *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
        *pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
        *pos++ = base64_table[in[2] & 0x3f];
        in += 3;
        line_len += 4;
        if (line_len >= 72) {
            *pos++ = '\n';
            line_len = 0;
        }
    }

    if (end - in) {
        *pos++ = base64_table[in[0] >> 2];
        if (end - in == 1) {
            *pos++ = base64_table[(in[0] & 0x03) << 4];
            *pos++ = '=';
        } else {
            *pos++ = base64_table[((in[0] & 0x03) << 4) |
                          (in[1] >> 4)];
            *pos++ = base64_table[(in[1] & 0x0f) << 2];
        }
        *pos++ = '=';
        line_len += 4;
    }

    if (line_len)
        *pos++ = '\n';

    *pos = '\0';
    if (out_len)
        *out_len = pos - out;
    return out;
}


/**
 * base64_decode - Base64 decode
 * @src: Data to be decoded
 * @len: Length of the data to be decoded
 * @out_len: Pointer to output length variable
 * Returns: Allocated buffer of out_len bytes of decoded data,
 * or %NULL on failure
 *
 * Caller is responsible for freeing the returned buffer.
 */
unsigned char * base64_decode(const unsigned char *src, size_t len,
                  size_t *out_len)
{
    unsigned char dtable[256], *out, *pos, block[4], tmp;
    size_t i, count, olen;
    int pad = 0;

    memset(dtable, 0x80, 256);
    for (i = 0; i < sizeof(base64_table) - 1; i++)
        dtable[base64_table[i]] = (unsigned char) i;
    dtable['='] = 0;

    count = 0;
    for (i = 0; i < len; i++) {
        if (dtable[src[i]] != 0x80)
            count++;
    }

    if (count == 0 || count % 4)
        return NULL;

    olen = count / 4 * 3;
    pos = out = (unsigned char *)malloc(olen);
    if (out == NULL)
        return NULL;

    count = 0;
    for (i = 0; i < len; i++) {
        tmp = dtable[src[i]];
        if (tmp == 0x80)
            continue;

        if (src[i] == '=')
            pad++;
        block[count] = tmp;
        count++;
        if (count == 4) {
            *pos++ = (block[0] << 2) | (block[1] >> 4);
            *pos++ = (block[1] << 4) | (block[2] >> 2);
            *pos++ = (block[2] << 6) | block[3];
            count = 0;
            if (pad) {
                if (pad == 1)
                    pos--;
                else if (pad == 2)
                    pos -= 2;
                else {
                    /* Invalid padding */
                    free(out);
                    return NULL;
                }
                break;
            }
        }
    }

    *out_len = pos - out;
    return out;
}

const char * enum_synap_format_code_to_text(int formatCode)
{
    const char *ext;

    if(formatCode==23100) {
        ext = ".png";
    } else if(formatCode==22800 || formatCode==22900) {
        ext = ".tiff";
    } else if(formatCode>=23200 && formatCode<=23202) {
        ext = ".gif";
    } else if(formatCode>=26600 && formatCode<=26602) {
        ext = ".jpg";
    } else if(formatCode==24600) {
        ext = ".bmp";
    } else if(formatCode==32000 || formatCode==32001) {
        ext = ".wmf";
    } else if(formatCode==192900 && formatCode<=192902) {
        ext = ".emf";
    } else if(formatCode==192910) {
        ext = ".wdp";
    } else {
        ext = "";
    }
    
    return ext;
}

bool on_detectImage(void* pUserData, const __uint8* pStream, const size_t len, const __int32 imgIndex)
{
    int formatCode      =  0;
    char lsName[1024]   = {0,};
    SN3MFI* pMFI        = NULL;
    document_info_t*            pDocument   = (document_info_t *)pUserData;
    document_attachment_info_t* pAttachment = NULL;

    if(pStream==NULL || pUserData == NULL) {
        ERRN("SC:%s SL:%d FC:on_detectImage imgIndex:%d DS:filter initization error (pStream=%p, pUserData=%p)", 
                TRSRC, imgIndex, pStream, pUserData);
        return false;
    }

    if (imgIndex >= MAX_ATTACHMENT -2 || imgIndex <= 0 || pDocument->n_attachment >= MAX_ATTACHMENT -2) {
        //DEBUGH("[%s] attechment too many, skip it (imgIndex=%d)", __func__, imgIndex);
        return true;
    }
    sn3mfi_fopen_m((__uint8*)pStream, len, &pMFI);
    sn3fmt_detect_m(pMFI, &formatCode);
    sn3mfi_fclose(pMFI);

    pAttachment = &pDocument->attachments[imgIndex-1];
    pDocument->n_attachment++;

    memset(pAttachment, 0x00, sizeof(document_attachment_info_t));

    pAttachment->size               = len;
    //pAttachment->MD5 
    //pAttachment->SPAMFINGER 
    pAttachment->strFileFormat      = enum_synap_format_code_to_text(formatCode);
    pAttachment->formatCode         = formatCode;
    pAttachment->lsAttachmentData   = (char *)malloc(len + 32);
    memset(pAttachment->lsAttachmentData, 0x00, len+32);
    memcpy(pAttachment->lsAttachmentData, pStream, len);

    return true;
}

int analyzer_parseAttach(char *cpAtt, int iSize, document_info_t * pResult)
{
    //DEBUGH("analyzer_parseAttach : cpAtt=%p, iSize=%d", cpAtt, iSize);
    int iRet;
    int iSN3Size;
    int iConv;
    SN3MFI *ptMfi = NULL;
    SN3BUF *ptBuf = NULL;
    __uint8 *cpSN3Buf = NULL;

    iRet = sn3buf_init(&ptBuf);
    if (iRet != SN3OK)
    {   
        ERRN("SC:%s SL:%d FC:sn3buf_init RE:%d DS:filter initization error", TRSRC, iRet);
        sn3mfi_fclose(ptMfi);
        return  -1;
    }   

    snf_buf_set_user_data(ptBuf, pResult);

    iRet = snf_buf_set_img_flt_func(ptBuf, on_detectImage);
    if (iRet != SN3OK) {
        sn3mfi_fclose(ptMfi);
        sn3buf_free(ptBuf);
        ERRN("SC:%s SL:%d FC:snf_buf_set_img_flt_func RE:%d DS:filter get image error", TRSRC, iRet);
        return -1;
    }

    iRet = sn3mfi_fopen_m((__uint8 *)cpAtt, iSize, &ptMfi);
    if (iRet)
    {   
        ERRN("SC:%s SL:%d FC:sn3mfi_fopen_m RE:%d DS:filter open error", TRSRC, iRet);
        return  -1;
    }   

    iRet = sn3flt_filter_m(ptMfi, ptBuf, 1); // TODO: remove 1 --> 
    if (iRet != SN3OK)
    {   
        ERRN("SC:%s SL:%d FC:sn3flt_filter_m RE:%d DS:filter text in attachment error", TRSRC, iRet);
        sn3mfi_fclose(ptMfi);
        sn3buf_free(ptBuf);
        return  -1;
    }   

    iSN3Size = (sn3buf_size(ptBuf)+1)*3;
    cpSN3Buf = (__uint8*)calloc(iSN3Size+1024, sizeof(char));
    memset(cpSN3Buf, 0x00, iSN3Size);
    iConv = sn3buf_get_text(ptBuf, cpSN3Buf, iSN3Size-2, SN3UCS_UTF8);
    if (iConv <= 0)
    {   
        ERRN("SC:%s SL:%d FC:sn3buf_get_text RE:%d DS:filter get text error", TRSRC, iConv);
        sn3mfi_fclose(ptMfi);
        sn3buf_free(ptBuf);
        return -1;
    }

    pResult->n_text_size = strlen((char *)cpSN3Buf);
    pResult->text_buf    = (char *)cpSN3Buf;

    sn3mfi_fclose(ptMfi);
    sn3buf_free(ptBuf);

 
   return 0;
}

int get_attachment_info(char *fileName, char **ptBuffer, int *lpSize) {

    FILE *fp            =  NULL;
    int fileLength      =  0;
    char strTemp[1024]  = {0,};
    char *allocBuffer   = NULL;

    if(access( fileName, 0 ) == -1) {
        return -1;
    }
    fp = fopen(fileName, "rb");

    if(fp == NULL) {
        return -1;
    }

    fseek(fp, 0, SEEK_END);
    fileLength = ftell(fp);
    fseek(fp, 0, SEEK_SET );

    if (fileLength < 100) {
        return -1;
    }

    printf("fileLength=%d\n", fileLength);

    allocBuffer = (char *)malloc(fileLength + 1024);
    if (allocBuffer == NULL) {
        return -1;
    }
    memset(allocBuffer, 0x00, fileLength + 1024);

    fread(allocBuffer, fileLength, 1, fp); 

    fclose(fp);

    *lpSize     = fileLength;
    *ptBuffer   = allocBuffer;

    return 0;
}

int print_usage()
{
    WARNN("usage        : ./image_extractor <options> <document-file-path or base64-data>");
    WARNN(" [options]");
    WARNN("       -w, --image-write         : save image to file");
    WARNN("       -p, --image-spamfinger    : print spamfinger each attachments");
    WARNN("       -i, --image-information   : print detail information of attachments");
    WARNN("       -b, --input-base64        : set input data base64 encoded data");
    WARNN("\n [example]");
    WARNN("       ./image_extractor -w example.doc");
    WARNN("       ./image_extractor -w example.xls");
    WARNN("       ./image_extractor -w example.qs       # detect all of documents in qs file");
    WARNN("       ./image_extractor -w example.qm       # detect all of documents in qm file");
    WARNN("       ./image_extractor -w example.eml      # detect all of documents in eml file");
    WARNN("       ./image_extractor -w /srkim/workspace/py_heuristic/data/link/example_0053.eml");
    WARNN("\n");

    return 0;
}

int write_extracted_attachment(document_info_t * pDocInfo)
{//  save at ./out/[document_name]/image_file_[idx].[extension]
 //          ./out/[email_name]/document_name:image_file_[idx].[extension] 
    int i;
    FILE * fd;
    int total_byte      = 0;
    char prefix[256]    = {0,};
    char lsName[1024]   = {0,};
    char lsPath[1024]   = {0,};
    document_attachment_info_t* pAttachment = NULL;

    if (pDocInfo->n_attachment == 0)
    {
        return 0;
    }

    if(access(OUTPUT_PATH, F_OK)) {
        if(mkdir( OUTPUT_PATH, 0755 )) {
            INFON("[%s] Error. Fail to create dit : %s\n", __func__, OUTPUT_PATH);
            return -1;
        }
    }


    int idx_separator = 0;
    if(*pDocInfo->root) {
        for(i=0;pDocInfo->name[i];i++) {
            if (pDocInfo->name[i] == '/') {
                idx_separator = i;
            }
        }
        sprintf(prefix, "%s:", &pDocInfo->name[idx_separator+1]);
        idx_separator = 0;
        for(i=0;pDocInfo->root[i];i++) {
            if (pDocInfo->root[i] == '/') {
                idx_separator = i;
            }
        }
        sprintf(lsPath, "%s/%s", OUTPUT_PATH, &pDocInfo->root[idx_separator+1]);
    }
    else {
        for(i=0;pDocInfo->name[i];i++) {
            if (pDocInfo->name[i] == '/') {
                idx_separator = i;
            }
        }
        sprintf(lsPath, "%s/%s", OUTPUT_PATH, &pDocInfo->name[idx_separator+1]);
    }

    for(i=0;lsPath[i];i++) {
        if (lsPath[i] == ' ') {
            lsPath[i] = '_';
        }
    }

    if(access(lsPath, F_OK)) {
        if(mkdir( lsPath, 0755 )) {
            INFON("[%s] Error. Fail to create dit : %s\n", __func__, lsPath);
            return -1;
        }
    }
    DEBUGH("in %s", lsPath);
    for(i=0;i<pDocInfo->n_attachment && i<MAX_ATTACHMENT;i++) {
        pAttachment = &pDocInfo->attachments[i];
        sprintf(lsName, "%s/%simage_file_%03d%s", lsPath, prefix, i, pAttachment->strFileFormat);
        DEBUGH(" -- save image : %s (%0.2fKB)", lsName, (pAttachment->size/1024.0));
        fd = fopen(lsName, "wb");
        fwrite(pAttachment->lsAttachmentData, pAttachment->size, 1, fd);
        fclose(fd);
        total_byte += pAttachment->size;
    }
    DEBUGH("Total %0.2fKB saved\n", total_byte/1024.0);

   
}

int param_parser(int argc, char * argv[]) 
{
    int i,j;
    int nDocument;
    char *at,*sDocumentName;
    char * pExt;
    struct stat bufStat;

    for(i=1;i<argc;i++) {
        at = argv[i];

        if(strcmp("-w", at) == 0 || strcmp("--image-write", at) == 0) {
            f_write = 1;
            continue;
        }
        if(strcmp("-p", at) == 0 || strcmp("--image-spamfinger", at) == 0) {
            f_spamfinger = 1;
            continue;
        }
        if(strcmp("-i", at) == 0 || strcmp("--image-information", at) == 0) {
            f_information = 1;
            continue;
        }

        sDocumentName = at;
        nDocument     = strlen(sDocumentName);
        if(access(sDocumentName, F_OK)) {
            WARNN("document file %s not exist", sDocumentName);
            continue;
        }

        if (lstat(sDocumentName, &bufStat) < 0) {
            WARNN("document file %s not exist", sDocumentName);
            continue;
        }

        if(S_ISDIR(bufStat.st_mode)) {
            WARNN("skip. %s is directory", sDocumentName);
            continue;
        }

        if(strstr(sDocumentName, "./") == sDocumentName) {
            sDocumentName += 2;
        }

        if (nDocument >= 5) {
            pExt = sDocumentName + (nDocument - 4);
            if (strstr(pExt, ".eml") || strstr(pExt, ".EML") || strstr(pExt, ".qs") || strstr(pExt, ".QS") || strstr(pExt, ".qm") || strstr(pExt, ".QM")) {
                /* 이메일 파일 일 경우, 그안에 document를 파싱해서 입력해준다. (파이썬 스크립트를 호출 한다.) */
                //input_list[n_input_file] = (char *)"";
                //input_root[n_input_file] = sDocumentName;
#define MAX_ATTACHMENTS 32
                emlAttachmentInfo_t result_ptrs[MAX_ATTACHMENTS];
                memset(&result_ptrs, 0x00, sizeof(result_ptrs));
                int nAttatch = 0;//= get_eml_attachment_document(sDocumentName, result_ptrs, MAX_ATTACHMENTS);

                for(j=0;j<nAttatch;j++) {
                    emlAttachmentInfo_t * pTmp = (emlAttachmentInfo_t *)malloc(sizeof(emlAttachmentInfo_t));
                    memcpy(pTmp, &result_ptrs[j], sizeof(emlAttachmentInfo_t));
                    input_list[n_input_file] = (char *)pTmp;
                    input_root[n_input_file] = sDocumentName;
                    //printf("%d pTmp=%d\n", n_input_file,pTmp->nData);
                    n_input_file++;
                }
                continue;
            }
        }
        input_list[n_input_file++] = sDocumentName;
    }

    if(n_input_file == 0) {
        WARNN("Error. No input document\n");
        return -1;
    }
    else if(f_write+f_spamfinger+f_information == 0) {
        WARNN("Error. No input options\n");
        return -1;
    }
    return 0;
}

char * convert_result_to_json(document_info_t * pDocInfo)
{
/*
{
    attachment_list : [
        {
            format  : "jpg",
            size    : 88144,
            date    : "...................."
        },
        {
            format  : "png",
            size    : 418131,
            date    : "...................."
        }
    ]
    text : "................"
}

*/

    int     _size;
    char   *_date;
    int     ln_result;
    char   *pJsonBuff = NULL;

    if(pDocInfo == NULL) {
        return NULL;
    }

    json_object *jInner;
    json_object *jObj    = json_object_new_object();
    json_object *jAttach = json_object_new_array();
    json_object_object_add(jObj, "text", json_object_new_string(pDocInfo->text_buf));

    for(int i=0;i<pDocInfo->n_attachment;i++) {
        document_attachment_info_t * pDoc = &pDocInfo->attachments[i];
        _size   = pDoc->size;
        _date   = pDoc->lsAttachmentData;
        const char *_format = pDoc->strFileFormat;

        if(_format == NULL || _size <= 0 || _date == NULL || _date < (void *)0xFFFF)
            continue;

        size_t out_len = 0;
        const unsigned char * _b64_date = base64_encode((unsigned char *)_date, _size, &out_len);

        jInner = json_object_new_object();
        json_object_object_add(jInner, "format", json_object_new_string(_format)); 
        json_object_object_add(jInner, "size", json_object_new_int(_size)); 
        json_object_object_add(jInner, "data", json_object_new_string((char *)_b64_date)); 
        json_object_array_add(jAttach, jInner);

        if(_b64_date) {
            free((void *)_b64_date);
        }
    }
    json_object_object_add(jObj, "attachment_list", jAttach);
   
    ln_result = strlen(json_object_to_json_string(jObj));
    pJsonBuff = (char *)malloc(ln_result + 32);
    strcpy(pJsonBuff, json_object_to_json_string(jObj));

    json_object_put(jObj);

    return pJsonBuff;
}


void free_document_info(document_info_t * pDocInfo)
{
    if(pDocInfo->text_buf) {
        free(pDocInfo->text_buf);
        pDocInfo->text_buf = NULL;
    }
    for(int i=0;i<pDocInfo->n_attachment;i++) {
        document_attachment_info_t * pDoc = &pDocInfo->attachments[i];
        free(pDoc->lsAttachmentData);
        pDoc->lsAttachmentData = NULL; 
    }
}

void __synap_golbal_init(uint64_t __add_options)
{
    uint64_t options = SN3OPTION_ARCHIVE_EXTRACT | 
                SN3OPTION_EXTENSION_NO_CHECK     | 
                SN3OPTION_PPT_EXTRACTALL         |
                __add_options;

    snf_gbl_setcfg("",  // snf_gbl_setcfg_ex
                SN3FILETYPE_ALL,
                options,
                512 * 1024); 
}

extern "C" {
void synap_golbal_init() {
    __synap_golbal_init(0);
}

void synap_ole_golbal_init() {
    __synap_golbal_init(SN3OPTION_EMBEDED_OLE_FILTER | SN3OPTION_EMBEDED_OLE_SEPARATE);
}



const char * synap_get_attachments(char * pDocumentBuf, int iLen, char * lsFileName)
{
    char * jsonBuf;
    document_info_t doc_info;
    memset(&doc_info, 0x00, sizeof(doc_info));

    strcpy(doc_info.name, lsFileName);
    doc_info.size = iLen;

    if (analyzer_parseAttach(pDocumentBuf, iLen, &doc_info) == -1) { 
        return NULL;
    }
    
    jsonBuf = convert_result_to_json(&doc_info);
    free_document_info(&doc_info);
    return jsonBuf;
}
} /* end : extern "C" */
