#!/bin/env python2
import os
import re
import sys
import json
import base64
import chardet
import subprocess
from synap_helper import *
from cyren_helper import *
from datetime import datetime

class SaasSatatistic():
    def __init__(self):
        self.virus_total_result = {
            #"yyyymmdd" : { 
            #    "2020-07-20-09:45:10:741017.json" : 1
            #}
        }
    
        self.statistic = {
            "virus"  : {
                "auto_exec_keywords" : {
                    #"Open" : 1,
                },
                "suspicious_keywords" : {
                },
            },
            "normal" : {
                "auto_exec_keywords" : {
                },
                "suspicious_keywords" : {
                },
            },
        }

    def __navigate_day_json_files(self, input_yyyymmdd):
        base_dir = "/home/mailadm/data/report"
        total_list = []
        for yyyymmdd in os.listdir(base_dir):
            in_day = "%s/%s" % (base_dir, yyyymmdd)
            if input_yyyymmdd != None and input_yyyymmdd != yyyymmdd:
                continue
                #yield qs_file
            for json_file in os.listdir(in_day):
                if ".json" != json_file[-5:]:
                    continue
                json_file = "%s/%s" % (in_day, json_file)
                total_list.append(json_file)
        return total_list

    def check_virus_count(self, json_dict):
        '''
        n_vba_file = 0
        if "attachments" not in json_dict.keys():
            return 0
        for attachment in json_dict["attachments"]:
            if "report" not in attachment.keys():
                continue
            if attachment["report"] == None:
                continue
            if "n_vba_file" not in attachment["report"].keys():
                continue
            n_vba_file += attachment["report"]["n_vba_file"]
        if n_vba_file == 0:
            return 0
        '''
        def parser_logfile(self, file_name, yyyymmdd):
            with open(file_name, "r") as fd:
                while True:
                    line = fd.readline()
                    if line == None or len(line) == 0:
                        break
                    if "RESULT" not in line:
                        continue
                    line = line.replace("\n", "")
                    line = line.split(" ")
                    if len(line) == 4:
                        in_qs_name = line[-2].split("/")[-1]
                        in_virus = line[-1].split(":")[-1]
                    else:
                        in_qs_name = line[-6].split("/")[-1]
                        in_virus = line[-1].split(":")[-1]
                    #print("%s : %s" % (in_qs_name, in_virus))
                    try:
                        in_virus = int(in_virus)
                    except ValueError:
                        in_virus = 0
                    self.virus_total_result[yyyymmdd][in_qs_name] = in_virus
        qs_name = json_dict["qs_name"].split("/")[-1]
        yyyymmdd = json_dict["qs_name"].split("/")[-1].split(":")[0].replace("-", "")[:-2]
        #n_virus_total_checked = 0
        if yyyymmdd not in self.virus_total_result.keys():
            file_name = "/home/mailadm/data/virustotal_result/virus_check_%s.log" % (yyyymmdd,)
            self.virus_total_result[yyyymmdd] = {}
            if os.path.exists(file_name) == True:
                parser_logfile(self, file_name, yyyymmdd)
        file_name = "/home/mailadm/data/virustotal_result/virus_check___twbr__.log" 
        if os.path.exists(file_name) == True and "__twbr__" not in self.virus_total_result.keys():
            self.virus_total_result["__twbr__"] = {}
            parser_logfile(self, file_name, "__twbr__")
        if qs_name not in self.virus_total_result[yyyymmdd].keys():
            if "__twbr__" in self.virus_total_result.keys():
                try:
                    return self.virus_total_result["__twbr__"][qs_name]
                except:
                    return None
        try:
            return self.virus_total_result[yyyymmdd][qs_name]
        except KeyError as e:
            return None

    def do_calc_statistic(self, json_dict):
        n_inc = 0
        qs_name = json_dict["qs_name"].split("/")[-1]
        n_virus_total_detected = json_dict["n_virus_total_detected"]
        if n_virus_total_detected == None:
            return
        check_list = ["auto_exec_keywords", "suspicious_keywords"]
        #print("n_virus_total_detected : %s" % (n_virus_total_detected,))
        for item in json_dict["attachments"]:
            item = item["report"]
            if n_virus_total_detected <= 0:
                selected_dict = self.statistic["normal"]
            else:
                selected_dict = self.statistic["virus"]

            for keyword_class in check_list:
                if keyword_class not in item.keys():
                    continue

                if keyword_class not in selected_dict.keys():
                    print("Error. Not exist keyword class to sum : %s" % (keyword_class,))
                    continue

                for keyword in item[keyword_class].keys():
                    #print(selected_dict[keyword_class])
                    n_inc += item[keyword_class][keyword]
                    try:
                        selected_dict[keyword_class][keyword] += item[keyword_class][keyword]
                    except KeyError:
                        selected_dict[keyword_class][keyword] = item[keyword_class][keyword]
        print("qs_name : %s %d" % (qs_name, n_inc))

    def print_statistic(self):
        json_stat = json.dumps(self.statistic, indent=4)
        compared_dict = {}
        for virus_type in self.statistic.keys():
            for keyword_class in self.statistic[virus_type].keys():
                if keyword_class not in compared_dict.keys():
                    compared_dict[keyword_class] = {}
                for keyword in self.statistic[virus_type][keyword_class].keys():
                    try:
                        compared_dict[keyword_class][keyword][virus_type] += self.statistic[virus_type][keyword_class][keyword]
                    except KeyError:
                        compared_dict[keyword_class][keyword] = {"virus" : 0, "normal" : 0}
                        compared_dict[keyword_class][keyword][virus_type] = self.statistic[virus_type][keyword_class][keyword]
        #print(json_stat)
        compared_json = json.dumps(compared_dict, indent=4)
        #print(compared_json)
        for keyword_class in compared_dict.keys():
            print("\n==%s==" % keyword_class)
            print("%-20s, %10s, %10s, %10s" % ("[keyword]", "[n_virus]", "[n_normal]", "[rate]"))
            keyword_dict = compared_dict[keyword_class]
            for keyword in keyword_dict.keys():
                n_virus  = keyword_dict[keyword]["virus"]
                n_normal = keyword_dict[keyword]["normal"]
                rate     = (n_virus / float(n_virus + n_normal)) * 100.0
                print("%-20s, %10d, %10d, %.10f" % (keyword, n_virus, n_normal, rate))
        


    def do_report(self, input_yyyymmdd=None):
        json_all = self.__navigate_day_json_files(input_yyyymmdd)
        for idx, json_file in enumerate(json_all):
            with open(json_file, "rb") as fd:
                json_string = fd.read()
            json_dict = json.loads(json_string)
            n_virus = self.check_virus_count(json_dict)
            json_dict["n_virus_total_detected"] = n_virus
            self.do_calc_statistic(json_dict)
            if idx % 100 == 0:
                print("check [%d/%d]" % (idx, len(json_all)))
        self.print_statistic()

def main():
    e = SaasSatatistic()
    e.do_report()

if __name__ == "__main__":
    main()
