#include "main.h"
#include <regex.h>

extern void free_document_info(document_info_t * pDocInfo);
extern char * convert_result_to_json(document_info_t * pDocInfo);
extern int analyzer_parseAttach(char *cpAtt, int iSize, document_info_t * pResult);


const char * AUTOEXEC_KEYWORDS[] = {"AutoExec", "AutoOpen", "DocumentOpen", "AutoExit", "AutoClose", "Document_Close", "DocumentBeforeClose", "DocumentChange", "AutoNew", "Document_New", "NewDocument", "Document_Open", "Document_BeforeClose", "Auto_Open", "Workbook_Open", "Workbook_Activate", "Auto_Ope", "Auto_Close", "Workbook_Close", NULL};

const char * AUTOEXEC_KEYWORDS_REGEX[] = { 
        "\\w+Painted",          "\\w+Painting", // 'Runs when the file is opened (using InkPicture ActiveX object)'
        "\\w+GotFocus",         "\\w+LostFocus",        "\\w+MouseHove",        "\\w+Click",
        "\\w+Change",           "\\w+Resize",           "\\w+BeforeNavigate2",  "\\w+BeforeScriptExecute", 
        "\\w+DocumentComplete", "\\w+DownloadBegin",    "\\w+DownloadComplete", 
        "\\w+FileDownload",     "\\w+NavigateComplete2","\\w+NavigateErro",
        "\\w+ProgressChange",   "\\w+PropertyChange",   "\\w+SetSecureLockIcon", 
        "\\w+StatusTextChange", "\\w+TitleChange",      "\\w+MouseMove",        "\\w+MouseEnte",
        "\\w+MouseLeave",       "\\w+Layout",  // 'Runs when the file is opened and ActiveX objects trigger events'
        NULL};


const char * SUSPICIOUS_KEYWORDS[] = {"Environ", "Open", "Write", "Put", "Output", "Print #", "Binary", "FileCopy", "CopyFile", "Kill", "CreateTextFile", "ADODB.Stream", "WriteText", "SaveToFile", "Shell", "vbNormal", "vbNormalFocus", "vbHide", "vbMinimizedFocus", "vbMaximizedFocus", "vbNormalNoFocus", "vbMinimizedNoFocus", "WScript.Shell", "Run", "ShellExecute", "ShellExecuteA", "shell32", "MacScript", "PowerShell", "noexit", "ExecutionPolicy", "noprofile", "command", "EncodedCommand", "invoke-command", "scriptblock", "Invoke-Expression", "AuthorizationManager", "Start-Process", "EXEC", "REGISTER", "CALL", "Application.Visible", "ShowWindow", "SW_HIDE", "MkDir", "ActiveWorkbook.SaveAs", "Application.AltStartupPath", "CreateObject", "New-Object", "Shell.Application", "ExecuteExcel4Macro", "Windows", "FindWindow", "Lib", "libc.dylib", "dylib", "REGISTER", "CreateThread", "CreateUserThread", "VirtualAlloc", "VirtualAllocEx", "RtlMoveMemory", "WriteProcessMemory", "SetContextThread", "QueueApcThread", "WriteVirtualMemory", "VirtualProtect", "SetTimer", "URLDownloadToFileA", "Msxml2.XMLHTTP", "Microsoft.XMLHTTP", "MSXML2.ServerXMLHTTP", "User-Agent", "Net.WebClient", "DownloadFile", "DownloadString", "SendKeys", "AppActivate", "CallByName", "Chr", "ChrB", "ChrW", "StrReverse", "Xor", "RegOpenKeyExA", "RegOpenKeyEx", "RegCloseKey", "RegQueryValueExA", "RegQueryValueEx", "RegRead", "SYSTEM\\ControlSet001\\Services\\Disk\\Enum", "VIRTUAL", "VMWARE", "VBOX", "GetVolumeInformationA", "GetVolumeInformation", "1824245000", "HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\ProductId", "76487-337-8429955-22614", "andy", "sample", "C:\\exec\\exec.exe", "popupkiller", "SbieDll.dll", "SandboxieControlWndClass", "C:\\file.exe", "currentuser", "Schmidti", "Afx:400000:0", "AccessVBOM", "VBAWarnings", "ProtectedView", "DisableAttachementsInPV", "DisableInternetFilesInPV", "DisableUnsafeLocationsInPV", "blockcontentexecutionfrominternet", "VBProject", "VBComponents", "CodeModule", "AddFromString", NULL};

const char * SUSPICIOUS_KEYWORDS_REGEX[] = {
    //'May use Word Document Variables to store and hide data':
        "\\.\\s*Variables",
    //'May run a shellcode in memory':
        "EnumSystemLanguageGroupsW?",
        //"EnumDateFormats(?:W|(?:Ex){1,2})?",
    //'May run an executable file or a system command on a Mac (if combined with libc.dylib)':
        "system", "popen", "exec[lv][ep]?",
    NULL};

const char * VPA_RESERVED_WORD[] = {"#If", "#Else", "#ElseIf", "#EndIf", "#Const", "Alias", "And", "As", "Base", "Boolean", "Byte", "ByRef", "ByVal", "Call", "Case", "CBool", "CByte", "CCur", "CDate", "CDbl", "CInt", "CLng", "CLngLng", "CLngPtr", "Compare", "Const", "CSng", "CStr", "Currency", "CVar", "Database", "Date", "Declare", "DefBool", "DefByte", "DefDate", "DefDec", "DefDouble", "DefInt", "DefLng", "DefLngLng", "DefLngPtr", "DefObj", "DefSng", "DefStr", "Dim", "Do", "Double", "Each", "Else", "ElseIf", "Empty", "End", "Enum", "Erase", "Error", "Event", "Exit", "Explicit", "FALSE", "For", "Friend", "Function", "Get", "Global", "GoTo", "If", "IIf", "Implements", "Integer", "Is", "Let", "LBound", "Lib", "Like", "Long", "LongLong", "Loop", "LSet", "Me", "Mod", "New", "Next", "Not", "Nothing", "Null", "Object", "On", "Option", "Optional", "Or", "ParamArray", "Preserve", "Private", "Property", "Public", "RaiseEvent", "ReDim", "Resume", "Return", "RSet", "Select", "Set", "Single", "Static", "Step", "Stop", "String", "Sub", "Text", "Then", "To", "TRUE", "Type", "TypeOf", "UBound", "Until", "Variant", "Wend", "While", "With", "WithEvents", NULL};

const char ** PP_CHECK_LIST[] = {
    AUTOEXEC_KEYWORDS, 
    SUSPICIOUS_KEYWORDS, 
    VPA_RESERVED_WORD,
    AUTOEXEC_KEYWORDS_REGEX,
    SUSPICIOUS_KEYWORDS_REGEX,
    NULL,
};

char allow_word_list[] = {
    '#',
    '$',
    ':',
    '.',
    '-',
    '\\',
    '`',
    '!',
    '@',
    '%',
    '^',
    '&',
    '*',
    '(',
    ')',
    '_',
    '+',
    '=',
    '`',
    ',',
    '/',
    '?',
     0,
};

#include <map>

regex_t * query_reg_chash(const char * regexp)
{
    static map<const char *, regex_t *> reg_chash;
    map<const char *, regex_t *>::iterator iter;
    iter = reg_chash.find(regexp);
    regex_t * pCompiled = NULL;
    if (iter == reg_chash.end()) {
        pCompiled = (regex_t *)malloc(sizeof(regex_t));
        if( regcomp( pCompiled, regexp, REG_EXTENDED ) != 0 ) {
            printf("[%s] Error, Fail to regcomp!! keywd : %s\n", __func__, regexp);
            return NULL;
        }
        pair<const char *, regex_t *> p(regexp, pCompiled);  
        reg_chash.insert(p);
        iter = reg_chash.find(regexp);
    }
    return iter->second;
}

extern "C" {

const char * synap_get_vba_script_text(char * pDocumentBuf, int iLen)
{

    document_info_t doc_info;
    memset(&doc_info, 0x00, sizeof(doc_info));
    doc_info.size = iLen;
    if (analyzer_parseAttach(pDocumentBuf, iLen, &doc_info) == -1) { 
        return NULL;
    }

    int i;
    int ln_vba = 0;
    int is_in_old = 0;
    char * ls_text = doc_info.text_buf;
    int len_word = strlen(ls_text);
    char * ptrVBAScripts[1024] = {NULL,};

    for(i=0;i < len_word - 15;i++) {
        if (is_in_old == 0 &&  !memcmp((void *)"~*~OLE START~*~", (void *)(ls_text + i), 15)) {
            is_in_old = 1;
            i += 15;
            ptrVBAScripts[ln_vba] = ls_text + i;
            ln_vba++;
            continue;
        }
        if (is_in_old == 1 && !memcmp((void *)"*~OLE END~*~", (void *)(ls_text + i + 1), 12)) {
            is_in_old = 0;
            memset(ls_text + i, 0x00, 13);
            i += 13;
            continue;
        }
    }
    if(!ln_vba) {
        return NULL;
    }

    json_object *jObj = json_object_new_object();
    json_object *ScriptArray = json_object_new_array();
    json_object_object_add(jObj, "n_vba_file", json_object_new_int(ln_vba));
    for(i=0;i<ln_vba;i++) {
         json_object_array_add(ScriptArray, json_object_new_string(ptrVBAScripts[i]));
    }
    json_object_object_add(jObj, "vba_files", ScriptArray);

    int ln_result = strlen(json_object_to_json_string(jObj)); 
    char * ppResult = (char *)malloc(ln_result + 16);
    strcpy(ppResult, json_object_to_json_string(jObj));

    json_object_put(jObj);

    return ppResult;
}

#define MAX_WORD_CHECK 1024 * 32
const char * synap_get_vba_script(char * pDocumentBuf, int iLen)
{
    char * word_ptrs[MAX_WORD_CHECK] = {NULL,};
    document_info_t doc_info;
    memset(&doc_info, 0x00, sizeof(doc_info));
    
    doc_info.size = iLen;

    if (analyzer_parseAttach(pDocumentBuf, iLen, &doc_info) == -1) { 
        return NULL;
    }
 
    int i;
    int j;
    int k;
    int n_autoexec_keyword      = 0;
    int n_suspicious_keyword    = 0;
    int n_vpa_reserved_word     = 0;
    int n_script_words          = 0;
    char * ls_text = doc_info.text_buf;
    int is_pass;
    int len_word = strlen(ls_text);
    int is_in_old = 0;
    int ln_vba = 0;
    //printf(ls_text);
    for(i=0;i < len_word - 15;i++) {
        if (is_in_old == 0 &&  !memcmp((void *)"~*~OLE START~*~", (void *)(ls_text + i), 15)) {
            is_in_old = 1;
            i += 15;
            ln_vba++;
            continue;
        }
       // char tmp[2000] = {0,};
        //memcpy(tmp, ls_text + i + 1, 12);
       // printf("%s\n", tmp);
        if (is_in_old == 1 && !memcmp((void *)"*~OLE END~*~", (void *)(ls_text + i + 1), 12)) {
            is_in_old = 0;
            memset(ls_text + i, 0x00, 13);
            i += 13;
            continue;
        }
        if (is_in_old != 1) {
            ls_text[i] = 0;
            continue;
        }
        //printf("%c",ls_text[i]);
        n_script_words++;
        if((ls_text[i] >= 'A' && ls_text[i] <= 'Z') || (ls_text[i] >= 'a' && ls_text[i] <= 'z')) {
            continue;
        }
        is_pass = 0;
        for(j=0; allow_word_list[j]; j++) {
            if(allow_word_list[j] == ls_text[i]) {
                is_pass = 1;
                break;
            }
        }
        if(is_pass == 0) {
            ls_text[i] = 0x00;
        }
    }
    j = 0;
    char * ptr_word;
    for(i=1;i<len_word;i++) {
        if(ls_text[i] != 0 && ls_text[i-1] == 0) {
            ptr_word = ls_text + i ;
            //printf("uuu %s\n", ptr_word);
            word_ptrs[j++] = ptr_word;
            if (j >= MAX_WORD_CHECK) {
                break;
            }
        }
    }
    int n_word_token = j;
#define MAX_KEYWORD_VECTOR  512
    int keyword_autoexec_vector[MAX_KEYWORD_VECTOR]         = {0,};
    int keyword_suspicious_vector[MAX_KEYWORD_VECTOR]       = {0,};
    int keyword_reserved_vector[MAX_KEYWORD_VECTOR]         = {0,};
    int keyword_autoexec_reg_vector[MAX_KEYWORD_VECTOR]     = {0,};
    int keyword_suspicious_reg_vector[MAX_KEYWORD_VECTOR]   = {0,};
    for(k=0;PP_CHECK_LIST[k];k++) {
        for(i=0;PP_CHECK_LIST[k][i];i++) {
            const char *keywd = PP_CHECK_LIST[k][i];
            //printf("!!!!  <%s>\n", keywd);
            if(keywd == NULL) {
                continue;
            }
            for(j=0;j<MAX_WORD_CHECK && j < n_word_token;j++) {
                ptr_word = word_ptrs[j];
                //printf("ptr_word=%s, keywd=%s\n", ptr_word, keywd);
                if (strcasestr(ptr_word, keywd)) {
                    if(k == 0) { /* AUTOEXEC_KEYWORDS */
                        keyword_autoexec_vector[i] += 1;
                        n_autoexec_keyword++;
                    }
                    else if(k == 1) { /* SUSPICIOUS_KEYWORDS */
                        //printf("1 : %s --> %s\n", keywd, ptr_word);
                        keyword_suspicious_vector[i] += 1;
                        n_suspicious_keyword++;
                    }
                    else if(k == 2) { /* VPA_RESERVED_WORD */
                        //printf("2 : %s --> %s\n", keywd, ptr_word);
                        keyword_reserved_vector[i] += 1;
                        n_vpa_reserved_word++;
                    }
                }
                else if( k >= 3 && k <= 4) { /* 정규식 매칭 */
                    int rc;
                    size_t nmatch = 1;
                    regmatch_t pmatch[1];
#if 0
                    regex_t compiled;
                    if( regcomp( &compiled, keywd, REG_EXTENDED ) != 0 ) {
                        printf("[%s] Error, Fail to regcomp!! keywd : %s\n", __func__, keywd);
                        continue;
                    }
#else               // 매번 정규식을 컴파일 하려니까 느리더라.. 캐싱을 하도록 하자!
                    regex_t *compiled = query_reg_chash(keywd);
#endif

                    rc = regexec(compiled, ptr_word, nmatch, pmatch, 0);

                    //if(strstr(ptr_word, "Click") != NULL) {
                    //    printf("<<<%s>>> %s rc=%d %d\n" , ptr_word, keywd, rc, pmatch[0].rm_so);
                    //}

                    if (0 != rc) {
                        if(REG_NOMATCH != rc) {
                            printf("Error. regexec return != REG_NOMATCH : rc=%d, keywd=%s\n", rc, keywd);
                        }
                        //regfree(&compiled);
                        continue;
                    }
                    else {
                        if(k == 3) { /* AUTOEXEC_KEYWORDS_REGEX */
                            keyword_autoexec_reg_vector[i] += 1;
                            n_vpa_reserved_word++;
                        }
                        else if(k == 4) { /* SUSPICIOUS_KEYWORDS_REGEX */
                            keyword_suspicious_reg_vector[i] += 1;
                            n_suspicious_keyword++;
                        }
                    }
                    //regfree(&compiled);
                }
            }
        }
    }

    //printf("ls_text : %d <%d,%d,%d>\n", len_word, n_autoexec_keyword, n_suspicious_keyword, n_vpa_reserved_word);
#if 0
    char result_buf[1024] = {0,};
    sprintf(result_buf, "{\n"
           "    \"n_autoexec_keyword\"     : %d,\n"
           "    \"n_suspicious_keyword\"   : %d,\n"
           "    \"n_vpa_reserved_word\"    : %d,\n"
           "    \"n_vba_file\"             : %d,\n"
           "    \"n_script_words\"         : %d,\n"
           "    \"n_total_words\"          : %d\n"
        "}", n_autoexec_keyword, n_suspicious_keyword, n_vpa_reserved_word, ln_vba, n_script_words, len_word);
#endif

    json_object *jObj           = json_object_new_object();
    json_object *jAutoExec      = json_object_new_object();
    json_object *jSuspicious    = json_object_new_object();
    json_object *jReservedWord  = json_object_new_object();

    json_object_object_add(jObj, "n_autoexec_keyword"   , json_object_new_int(n_autoexec_keyword));
    json_object_object_add(jObj, "n_suspicious_keyword" , json_object_new_int(n_suspicious_keyword));
    json_object_object_add(jObj, "n_vpa_reserved_word"  , json_object_new_int(n_vpa_reserved_word));
    json_object_object_add(jObj, "n_vba_file"           , json_object_new_int(ln_vba));
    json_object_object_add(jObj, "n_script_words"       , json_object_new_int(n_script_words));
    json_object_object_add(jObj, "n_total_words"        , json_object_new_int(len_word));

    for(i=0;i<MAX_KEYWORD_VECTOR;i++) {
        int n_idx_auto      = keyword_autoexec_vector[i];
        int n_idx_susp      = keyword_suspicious_vector[i];
        int n_idx_keyw      = keyword_reserved_vector[i];
        int n_idx_auto_reg  = keyword_autoexec_reg_vector[i];
        int n_idx_susp_reg  = keyword_suspicious_reg_vector[i];

        if (n_idx_auto > 0) {
            json_object_object_add(jAutoExec, AUTOEXEC_KEYWORDS[i]   , json_object_new_int(n_idx_auto));
        }
        if (n_idx_susp > 0) {
            json_object_object_add(jSuspicious, SUSPICIOUS_KEYWORDS[i]   , json_object_new_int(n_idx_susp));
        }
        if (n_idx_keyw > 0) {
            json_object_object_add(jReservedWord, VPA_RESERVED_WORD[i]   , json_object_new_int(n_idx_keyw));
        }
        if (n_idx_auto_reg > 0) {
            json_object_object_add(jAutoExec, AUTOEXEC_KEYWORDS_REGEX[i]   , json_object_new_int(n_idx_auto_reg));
        }
        if (n_idx_susp_reg > 0) {
            json_object_object_add(jSuspicious, SUSPICIOUS_KEYWORDS_REGEX[i]   , json_object_new_int(n_idx_susp_reg));
        }
    }

    json_object_object_add(jObj, "auto_exec_keywords"   , jAutoExec);
    json_object_object_add(jObj, "suspicious_keywords"  , jSuspicious);
    json_object_object_add(jObj, "reserved_keywords"    , jReservedWord);

    int ln_result = strlen(json_object_to_json_string(jObj)); 
    char * ppResult = (char *)malloc(ln_result + 16);
    strcpy(ppResult, json_object_to_json_string(jObj));

    json_object_put(jObj);


    //printf("%s\n",result_buf);
    free_document_info(&doc_info);
    if(n_autoexec_keyword*5 + n_suspicious_keyword*2 + n_vpa_reserved_word > 50) {
        return ppResult;
    }

    return ppResult;
}

}


