# -*- coding: utf-8 -*-

import os
import json
import time
import gzip
import base64
import socket

# cyran 이 설치 된 서버 방화벽에서 예외처리 한다.
#   --> iptables -I INPUT  1 -p tcp --dport 17777 -j ACCEPT
#   --> iptables -I OUTPUT 1 -p tcp --dport 17777 -j ACCEPT


ICAP_CMD_1=b"OPTIONS icap://127.0.0.1/avscan ICAP/1.0\r\n\r\n"
ICAP_CMD_2=b"RESPMOD icap://127.0.0.1:17777/AVSCAN?action=SCANREPAIRDELETE ICAP/1.0\r\n"
ICAP_CMD_3= b"HOST: 127.0.0.1:17777\r\n"+\
            b"ALLOW: 0\r\n"+\
            b"Encapsulated: req-hdr=0, res-hdr=1, res-body=122\r\n"+\
            b"\r\n"+\
            b"GET http://www.symantec.com/mime HTTP/1.1\r\n"+\
            b"\r\n"+\
            b"HTTP/1.1 200 OK\r\n"+\
            b"Content-Type: message/rfc822\r\n"+\
            b"Transfer-Encoding: chunked\r\n"


#            b"Encapsulated: req-hdr=0, res-hdr=45, res-body=122\r\n"+\

class CyrenHelper():
    def __init__(self, ipaddr="192.168.0.18", port=17777):
        self.port   = port
        self.ipaddr = ipaddr

    def __read_from_file(self, path):
        if ".gz" == path[-3:]:
            fd = gzip.open(path, "rb")
        else:
            fd = open(path, "rb")

        all_data = fd.read()
        fd.close()

        if ".eml" in path.lower():
            return all_data

        for idx, at in enumerate(all_data):
            if at == ord('\n'):
                if b'%trqfile2%' in all_data[:idx]:
                    all_data = all_data[idx+1:]
                break
            elif idx == 1000:
                break
        return all_data

    def __response_parse(self, response):
        response = response.split(b"\r\n")
        f_start_detect = False
        n_found_virus  = 0
        virus_attachment = []
        '''
        X-Violations-Found: 1
            /mime/DOCS.doc->(objdata)
            RTF/CVE1711882
            553524
            1
        Encapsulated: res-hdr=0, res-body=107
        '''

        for item in response:
            if b"X-Violations-Found" in item:
                f_start_detect = True
                try:
                    n_found_virus = int(item.split(b":")[-1].strip())
                except ValueError as e:
                    return n_found_virus, virus_attachment
                continue
            if f_start_detect == True:
                file_name = item.split(b"->")[0].strip()
                try:
                    file_name = file_name.decode("utf-8")
                except:
                    try:
                        file_name = file_name.decode("euc-kr")
                    except:
                        file_name = None
                virus_attachment.append(file_name)
                break
        return n_found_virus, virus_attachment

    def queryCyran(self, data):
        if type(data) == str and os.path.exists(data) == True:
            data = self.__read_from_file(data)
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((self.ipaddr, self.port))

        try:
            client_socket.send(ICAP_CMD_1)

            response = client_socket.recv(65535).decode("utf-8")

            client_socket.send(ICAP_CMD_2)

            data2 = b"%s\r\n%x\r\n" % (ICAP_CMD_3, len(data)+2)
            client_socket.send(data2)
            client_socket.send(data)
            client_socket.send(b"\r\n\r\n\r\n\r\n0\r\n\r\n")
        except socket.error as e:
            client_socket.close()
            return None

        response = b""
        while True:
            try:
                data = client_socket.recv(65535)
                response += data
            except UnicodeDecodeError as e:
                pass
            except Exception as e:
                return 0, None
            if b"\r\n\r\n" in response:
                break
            elif len(data) == 0:
                print("len == 0")
                break
        client_socket.close()
        
        return self.__response_parse(response)


def main():
    test_qs = "/data/maildata/raw_mails/inbound/20200618/1120/2020-06-18-11:21:14:031818.qs"
    test_qs = "/data/maildata/raw_mails/twbr/terracespamadm/20190821/1250/2019-08-21-12:57:47:374264.qs.gz"
    test_qs = "/data/maildata/raw_mails/twbr/terracespamadm/20190909/1130/2019-09-09-11:34:40:614070.qs.gz"
    test_qs = "/maildata/archive/user/ole_xls/20200716/0350/2020-07-16-03:54:45:840958.qs"
    e = CyrenHelper()
    n_found_virus, virus_attachment = e.queryCyran(test_qs)
    print("n_found_virus=%d, virus_attachment=%s" %(n_found_virus, virus_attachment))

if __name__ == "__main__":
    main()   
