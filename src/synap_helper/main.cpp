#include "main.h"

int print_usage();
int param_parser(int argc, char * argv[]);
int write_extracted_attachment(document_info_t * pDocInfo);
int get_attachment_info(char *fileName, char **ptBuffer, int *lpSize);
int analyzer_parseAttach(char *cpAtt, int iSize, document_info_t * pResult);

int main( int argc, char * argv[] ) {
    int    i                = 0;
    int    iLen             = 0;
    int    iRet             = 0;
    char * pDocumentBuf     = NULL;
    char * sDocumentName    = NULL;
    char * sEmailName       = NULL;
    document_info_t           doc_info;
    emlAttachmentInfo_t *     pAttachInfo;
    document_attachment_info_t * attach_info;

    /* 1. check parameter */
    if (param_parser(argc, argv) == -1) {
        print_usage();
        return -1; 
    }

    /* 2. global init */
    synap_golbal_init();

    for(i=0; i<n_input_file; i++) {
        sEmailName = input_root[i];
        memset(&doc_info, 0x00, sizeof(doc_info));

        /* 3. load email file */
        if (sEmailName != NULL) { /* 입력 file이 eml 형식이라면, 전처리 단계에서 이미 첨부 자료를 로딩 하였다. */
            pAttachInfo     = (emlAttachmentInfo_t *)input_list[i];
            if (pAttachInfo == NULL) {
                INFON("Error. pAttachInfo is NULL");
                continue;
            }
            if (pAttachInfo->pData == NULL) {
                INFON("Error. pAttachInfo->pData is NULL");
                continue;
            }
            iLen            = pAttachInfo->nData;
            pDocumentBuf    = (char *)pAttachInfo->pData;
            sDocumentName   = pAttachInfo->lsfileName;
            strcpy(doc_info.name, pAttachInfo->lsfileName);
        }
        else {
            sDocumentName = input_list[i];
            iRet = get_attachment_info(sDocumentName, &pDocumentBuf, &iLen);
            strcpy(doc_info.name, input_list[i]);
        }

        doc_info.size = iLen;
        if (sEmailName != NULL) {
            strcpy(doc_info.root, sEmailName);
        }

        if (analyzer_parseAttach(pDocumentBuf, iLen, &doc_info) == -1) {
            continue;
        }

        /* 4. doing report */
        if (f_write != 0) {
            write_extracted_attachment(&doc_info);
        }

        if (f_spamfinger != 0) {

        }

        if (f_information != 0) {

        }

        /* 5. free */
        free(pDocumentBuf);
        for(int j=0; j < doc_info.n_attachment; j++) {
            attach_info = &doc_info.attachments[j];
            if(attach_info->lsAttachmentData) {
                free(attach_info->lsAttachmentData);
                attach_info->lsAttachmentData = NULL;
            }
        }
        pDocumentBuf = NULL;
    }

    return 0;
}
