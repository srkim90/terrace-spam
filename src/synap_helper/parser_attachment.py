#!/bin/env python2
import os
import re
import sys
import json
import base64
import chardet
import subprocess
from synap_helper import *
from cyren_helper import *
from datetime import datetime

class parserAttachment():
    def __init__(self, ps_path, target=None):
        self.target = target
        if target == None:
            self.target = ["doc", "hwp", "ppt", "xls",]
        self.ps_path = ps_path
 
    def parser_content_block(self, lines):
        #print("!!! parser_content_block")
        def parser_hdr_at(line):
            #print("!!! parser_content_block 2 : %s" % line)
            if "content-type" not in line.lower() and "content-disposition" not in line.lower():
                return None
            if "name=" not in line.lower():
                return None
            line = line.split("name=")
            try:
                line = line[1]
            except IndexError as e:
                return None
            if "\"" != line[0]:
                line = "\"" + line
                line = line.replace(";", "\"")
            file_name = line.split("\"")[1]
            file_name = file_name.replace(" ", "")
            file_name = file_name.replace("\t", "")
            #print("file_name : %s" % file_name)

            if "=?utf-8?b?" in file_name.lower():
                file_name = file_name.strip()
                #file_name = file_name.split("=?UTF-8?B?")
                #file_name = file_name[10:]
                #print(file_name)
                file_name = re.split("=\?UTF-8\?B\?", file_name, flags=re.IGNORECASE)
                new_name = ""
                #print(file_name)
                for item in file_name:
                    item = item.replace("?=", "")
                    if len(item) == 0:
                        continue
                    try:
                        new_name += base64.b64decode(item)
                    except TypeError as e:
                        new_name += item
                file_name = new_name

            ext = file_name.split(".")[-1].lower()
            if ext in self.target:
                return file_name
            return None
        f_end_head = False
        now_hdr_item = ""
        file_name = None
        file_info = {

        }
        for line in lines:
            if f_end_head == True:
                if file_name != None:
                    file_info["base64"] += line
                continue
            if len(line) == 0:
                result = parser_hdr_at(now_hdr_item)
                if result != None:
                    file_name = result
                f_end_head = True
                if file_name != None:
                    file_info["base64"] = ""
                    file_info["binary"] = b""
                    file_info["filename"] = file_name
                continue
            if line[0] == b" " or line[0] == b"\t":
                now_hdr_item += line
            else:
                result = parser_hdr_at(now_hdr_item)
                if result != None:
                    file_name = result
                now_hdr_item = line

        if len(file_info.keys()) == 0:
            return None
        try:
            file_info["binary"] = base64.b64decode(file_info["base64"])
        except TypeError as e:
            return None
        return file_info
    def navigate_file(self):
        if self.ps_path[-3:].lower() == ".gz":
            fd = gzip.open(self.ps_path , 'rb')
        else:
            fd = open(self.ps_path , 'rb')

        rawAll = fd.read()
        rawAll = rawAll.replace("\r", "")
        rawAll = rawAll.split("\n")
        fd.close()
        boundary_list = []
        idx = 0
        last_boundary = None
        last_boundary_idx = None
        while idx+1 < len(rawAll):
            idx += 1
            if idx < 5:
                 continue
            line = rawAll[idx]
            if b"boundary=" in line.lower():
                line = line.replace(b"boundary=", b"")
                line = line.replace(b"BOUNDARY=", b"")
                line = line.replace(b" ", b"")
                line = line.replace(b"\t", b"")
                if b'"' in line:
                    boundary = line.split(b"\"")[-2]
                else:
                    boundary = line.replace(b"\"", b"")
                boundary_list.append(boundary)
                continue
            for boundary in boundary_list:
                if boundary in line:
                    if last_boundary == boundary:
                        #print("boundary: %s" % boundary)
                        ret = self.parser_content_block(rawAll[last_boundary_idx+1:idx])
                        if ret != None:
                            yield ret
                    last_boundary = boundary
                    last_boundary_idx = idx


class inboundUtils:
    def __init__(self, cyren_ipaddr="222.99.178.9"):
        self.cyren_ipaddr = cyren_ipaddr
        self.parsed_log = {
                #"20200717" : {
                #       "2020-07-17-07:42:02:149828.qs" : ["[07:42:02 6467.139861926598400.5036] -INFO ! MC:audit ACT:archived...", ]
                #}
            }
    '''   
    def __wmtad_log_parser(self, yyyymmdd):
        log_name = "/tmwdata/log/wmtad/%s.log" % (yyyymmdd,)
        print("Start indexing log file : %s " % log_name)
        if os.path.exists(log_name) == True:
            fd = open(log_name, "rb")
        else:
            log_name += ".gz"
            if os.path.exists(log_name) == False:
                return None
            fd = gzip.open(log_name, "rb")

        self.parsed_log[yyyymmdd] = {}
        while True:
            line = fd.readline()
            if line == None or len(line) == 0:
                break
            line = line.replace("\n", "")
            if ".qs" not in line or "QF:" not in line:
                continue
            qs_name = "%s.qs" % line.split("QF:")[1].split(".qs ")[0].split("/")[-1]

            line = line.split("SJ:")[0]

            try:
                self.parsed_log[yyyymmdd][qs_name].append(line)
            except:
                self.parsed_log[yyyymmdd][qs_name] = [line,]
        fd.close()
        print("End indexing")
    '''

    def __wmtad_log_parser(self, yyyymmdd):
        log_name = "/tmwdata/log/wmtad/%s.log" % (yyyymmdd,)
        print("Start indexing from log : %s " % log_name)
        #return ##########################
        command = "/home/mailadm/terrace-spam/script/parse_audit_log.pl -d %s" % (yyyymmdd, )
        popen = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (stdoutdata, stderrdata) = popen.communicate()
        stdoutdata = stdoutdata.split("\n")
        self.parsed_log[yyyymmdd] = {}
        for line in stdoutdata:
            if "RESULT: " not in line:
                continue
            line = line.split(" ")
            if len(line) != 4:
                continue
            #print(line)
            qs_name = line[1].split("/")[-1]
            mc = line[2]
            rn = line[3]
            #print("%s %s %s" % (qs_name, mc, rn))
            self.parsed_log[yyyymmdd][qs_name] = {"MC":mc,"RN":rn}
        print("End indexing")

    def __grep_mta_log_for_qs(self, qs_name):
        name = qs_name.split("/")[-1]
        yyyymmdd = "%s%s%s" % (name[0:4], name[5:7], name[8:10])
        if yyyymmdd not in self.parsed_log:
            self.__wmtad_log_parser(yyyymmdd)
        try:
            return self.parsed_log[yyyymmdd][name]
        except KeyError as e:
            return None

    def ole_attachment_report(self, qs_name):
        now = datetime.now()
        now_timestring = "%s-%02d-%02d %02d.%02d.%02d" % (now.year, now.month, now.day, now.hour, now.minute, now.second)
        log_result = self.__grep_mta_log_for_qs(qs_name)
        MC = None
        RN = None
        if log_result != None:
            MC = log_result["MC"]
            RN = log_result["RN"]
        result = {
            "attachments"   : [],
            "analyzed_time" : now_timestring,
            "qs_name"       : qs_name,
            "MC"            : MC,
            "RN"            : RN,
        }
        cyren = CyrenHelper(ipaddr=self.cyren_ipaddr)
        try:
            result["n_found_virus"], result["virus_attachment"] = cyren.queryCyran(qs_name)
        except TypeError as e:
            result["n_found_virus"] = None
            result["virus_attachment"] = None
        objEml = parserAttachment(qs_name)
        for item in objEml.navigate_file():
            binary     = item["binary"]
            filename   = item["filename"]
            str_base64 = item["base64"]
            #print(str_base64)
            #with open(filename, "wb") as fd:
            #    fd.write(binary)
            report   = synap_get_vbs_info(binary)
            script   = synap_get_vbs_text(binary)

            charset = chardet.detect(filename)["encoding"]
            filename = filename.decode(charset)
            attachment = {
                "filename"  : filename,
                "report"    : report,
                "script"    : script,
                "file_data" : binary,
            }
            result["attachments"].append(attachment)
        return result
    
    def navigate_day_qs_files(self, input_yyyymmdd=None):
        base_dir = "/maildata/archive/user"
        total_list = []
        for attach_class in os.listdir(base_dir):
            in_class = "%s/%s" % (base_dir, attach_class)
            for yyyymmdd in os.listdir(in_class):
                in_day = "%s/%s" % (in_class, yyyymmdd)
                if input_yyyymmdd != None and input_yyyymmdd != yyyymmdd:
                    continue
                for hhmm in os.listdir(in_day):
                    in_hhmm = "%s/%s" % (in_day, hhmm) 
                    for qs_file in os.listdir(in_hhmm):
                        qs_file = "%s/%s" % (in_hhmm, qs_file)
                        #yield qs_file
                        total_list.append(qs_file)
        return total_list

    def save_result(self, qs_name, report, is_save_file=True):
        save_base = "/home/mailadm/data/report"
        name = qs_name.split("/")[-1]
        yyyymmdd = "%s%s%s" % (name[0:4], name[5:7], name[8:10])
        #json_report = json.dumps(report, indent=4, ensure_ascii=False)
        day_dir = "%s/%s" % (save_base, yyyymmdd)
        if os.path.exists(day_dir) == False:
            os.makedirs(day_dir)

        for idx, attachment in enumerate(report["attachments"]):
            file_data = attachment["file_data"]
            del attachment["file_data"]
            if is_save_file == True:
                attachment_dir = "%s/attachments" % day_dir
                if os.path.exists(attachment_dir) == False:
                    os.makedirs(attachment_dir)
                new_attachment_name = "%s/%s_%d.%s" % (attachment_dir, name.split(".")[0], idx, attachment["filename"].split(".")[-1])
                with open(new_attachment_name, "w") as fd:
                    fd.write(file_data)
            attachment["saved_attach_name"] = new_attachment_name
        json_report = json.dumps(report, indent=4)
        
        full_path = "%s/%s.json" % (day_dir, name.split(".")[0])
        with open(full_path, "w") as fd:
            fd.write(json_report)
        return qs_name, full_path

#/maildata/archive/user/ole_xls/20200720/2310/2020-07-20-23:19:40:992967.qs
def main():
    day_list = []
    qs_list  = []
    for param in sys.argv[1:]:
        if len(param) == 8 and '.qs' not in param:
            day_list.append(param)
        elif '.qs' in param:
            qs_list.append(param)

    if len(day_list) + len(qs_list) == 0:
        print("Invalid input parameter")
        print("  python parser_attachment.py [yyyymmdd1] [yyyymmdd2] ...")
        return None

    e = inboundUtils(cyren_ipaddr="127.0.0.1")
    for idx, qs_name in enumerate(qs_list):
        #print(qs_name)
        report = e.ole_attachment_report(qs_name)
        qs_name, file_name = e.save_result(qs_name, report)
        print("[%d/%d] %s --> %s" % (idx + 1, len(qs_list), qs_name, file_name))

    for yyyymmdd in day_list:
        total_list = e.navigate_day_qs_files(yyyymmdd) 
        for idx, qs_name in enumerate(total_list):
            report = e.ole_attachment_report(qs_name)
            qs_name, file_name = e.save_result(qs_name, report)
            print("[%d/%d] %s --> %s" % (idx + 1, len(total_list), qs_name, file_name))

if __name__ == "__main__":
    main()
