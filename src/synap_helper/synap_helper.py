import os
import json
import base64
from ctypes import *

#a = libsynap.synap_get_attachments(b"111111", 10, b"dasdsdas")

g_libc   = None
g_halper = None
MAX_TEXT_SIZE = 1024 * 8

def __get_helper_path():
    check_name = "libsynap_helper.so"
    check_path = ["./", "../", "./synap_helper", "../synap_helper", "../../synap_helper", "../../../synap_helper"]
    for item in check_path:
        full_path = "%s/%s" % (item, check_name)
        if os.path.exists(full_path) == True:
            return full_path
    return None

def __synap_init():
    global g_libc
    global g_halper
    libc     = CDLL("libc.so.6") 
    libsynap = CDLL(__get_helper_path()) # libsynap_helper.so
    libc.free.argtypes = [c_void_p,]
    libsynap.synap_ole_golbal_init()
    libsynap.synap_get_attachments.restype = c_void_p #c_char_p
    libsynap.synap_get_vba_script.restype  = c_void_p #c_char_p
    libsynap.synap_get_vba_script_text.restype  = c_void_p #c_char_p
    g_libc   = libc
    g_halper = libsynap
   
def synap_get_document_info(doc_bytes):
    '''
# format : [".png", ".tiff", ".gif", ".jpg", ".bmp", ".wmf", ".emf", ".wdp"]
    {
        attachment_list : [
            {
                format  : ".jpg",
                size    : 88144,
                date    : "...................."
            },
            {
                format  : ".png",
                size    : 418131,
                date    : "...................."
            }
        ]
        text : "................"
    }
    '''
    if g_halper == None:
        __synap_init()
    if doc_bytes == None or type(doc_bytes) != bytes:
        return None
    lpvoid_result = g_halper.synap_get_attachments(doc_bytes, len(doc_bytes), b"")
    #int_result    = g_halper.synap_get_vba_script(doc_bytes, len(doc_bytes))

    if lpvoid_result == None:
        return None
    json_result   = c_char_p(lpvoid_result).value
    g_libc.free(lpvoid_result)
    
    result_dict = json.loads(json_result)

    if "attachment_list" in result_dict.keys():
        for item in result_dict["attachment_list"]:
            b64_data = item["data"]
            item["data"] = base64.b64decode(b64_data)
    result_text = result_dict["text"]

    if len(result_text) > MAX_TEXT_SIZE:
        result_dict["text"] = result_text[:MAX_TEXT_SIZE-1]

    return result_dict

def synap_get_vbs_info(doc_bytes):
    if g_halper == None:
        __synap_init()
    if doc_bytes == None or type(doc_bytes) != bytes:
        return None
    lpvoid_result = g_halper.synap_get_vba_script(doc_bytes, len(doc_bytes))
    if lpvoid_result == None:
        return None
    json_result = c_char_p(lpvoid_result).value
    json_result = json_result.replace(b" ", b"")
    json_result = json_result.replace(b"\n", b"")
    json_dict = json.loads(json_result)
    g_libc.free(lpvoid_result)
    
    return json_dict

def synap_get_vbs_text(doc_bytes):
    if g_halper == None:
        __synap_init()
    if doc_bytes == None or type(doc_bytes) != bytes:
        return None
    lpvoid_result = g_halper.synap_get_vba_script_text(doc_bytes, len(doc_bytes))
    if lpvoid_result == None:
        return None
    json_result = c_char_p(lpvoid_result).value
    json_result = json_result.replace(b" ", b"")
    json_result = json_result.replace(b"\n", b"")
    json_dict = json.loads(json_result)
    g_libc.free(lpvoid_result)
    
    return json_dict

def main():
    doc_data = None
    #file_name = "/data/maildata/attachment/data/20200211/0620.doc"
    #file_name = "/data/maildata/attachment/data/20191209/1752.doc"
    file_name = "/data/maildata/attachment/data/twbr_virus/1429.doc"
    with open(file_name, "rb") as fd:
        doc_data = fd.read()
    #result = synap_get_document_info(doc_data)
    result = synap_get_vbs_info(doc_data)
    print("%s" % (json.dumps(result, indent=4)))

if __name__ == "__main__":
    main()   
