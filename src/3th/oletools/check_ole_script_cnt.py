#!/bin/env python

import sys, os, optparse

import olefile
import colorclass

from oletools.thirdparty.tablestream import tablestream
from oletools.thirdparty.xglob import xglob
from oletools.common.clsid import KNOWN_CLSIDS



def main():
    list_file="/home/mailadm/data/attachment/all.txt"
    with open(list_file, "r") as fd:
        lines = fd.read().split("\n",)
    lines = ["/data/maildata/attachment/data/20191121/1156.doc"]
    for filename in lines:
        try:
            ole = olefile.OleFileIO(filename)
        except OSError as e:
            pass
            #print("!!")
            continue
        for id in range(len(ole.direntries)):
            d = ole.direntries[id]
            #if d == None:
            #    continue
            try:
                name = d.name
            except AttributeError as e:
                continue
            print(name)
        print(filename)

if __name__ == "__main__":
    main()
