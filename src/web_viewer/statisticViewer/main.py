#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import common
from common import *

# import IPython
from ipywidgets import widgets, interact, interactive, fixed, interact_manual
#from matplotlib.backends.backend_wxagg import FigureCanvasWxAgg as FigureCanvas
from IPython.display import display

# import grapic libs
import matplotlib.pyplot as plt

# import combination typedef
sys.path.append("../../find_attribute")
from combination_typedef import *
from IPython.display import clear_output

base_dir = os.environ['MAIL_DATA_HOME']
base_archive = "%s/attribute_data/archive" % (base_dir,)

class ShowChart():
    def __init__(self, item_list):
        self.item_list = item_list
   
    def __load_data(self, file_name):
        with open(file_name, "rb") as fd:
            raw_json = fd.read()
            parsed = json.loads(raw_json)
        return parsed

    def show_graph(self):
        group_names    = ['NORM', 'SPAM']
        group_colors   = ['yellowgreen', 'lightcoral']
        group_explodes = (0.1, 0,) # explode 1st slice

        plt.close()
        for file_name in self.item_list:
            json_name = file_name.split("/")[-1].split(".")[0]
            parsed = self.__load_data(file_name)
            for idx,item in enumerate(parsed):
                norm_count = item["norm-count"]
                spam_count = item["spam-count"]
                key_list   = item["key"]
                group_sizes = [norm_count, spam_count]
                group_title = "%s:%s" % (json_name, key_list)
                plt.pie(group_sizes, 
                        explode=group_explodes, 
                        labels=group_names, 
                        colors=group_colors, 
                        autopct='%1.2f%%', # second decimal place
                        shadow=True, 
                        startangle=90,
                        textprops={'fontsize': 14}) # text font size
                plt.axis('equal') #  equal length of X and Y axis
                plt.title(group_title, fontsize=10)
                #canvas = FigureCanvas(panel, -1, fig)
                #canvas.Show(True)
                plt.show()
                #plt.set_visible(not fig.get_visible())

    def show_graph2(self):
        dd

class ItemSelector(): 
    def __init__(self, base_dir):
        self.base_archive = "%s/attribute_data/archive" % (base_dir,)
        self.items_all = self.__listup_result_path()
        self.yyyymmdd  = None
        self.hhmmss    = None
        self.report_json_name = None
        self.attribute_name_dict = {}
        self.drop_box3 = None

    def __listup_result_path(self):
        item_dict = {}
        file_list = os.listdir(self.base_archive)
        for yyyymmdd in file_list:
            item_list = []
            yyyymmdd_path = "%s/%s" % (self.base_archive, yyyymmdd)
            in_list = os.listdir(yyyymmdd_path)
            for hhmmss in in_list:
                full_path = "%s/%s" % (yyyymmdd_path, hhmmss)
                item_list.append(full_path)
            item_dict[yyyymmdd] = item_list
        return item_dict

    def __update_dropbox3_attribute(self):
        if self.drop_box3 == None:
            return
        json_dir = "%s/%s/%s" % (self.base_archive, self.yyyymmdd, self.hhmmss)
        file_list = os.listdir(json_dir)
        json_list = []
        self.attribute_name_dict = {}
        for idx, item in enumerate(file_list):
            if len(item) < 6 or ".json" not in item[-5:]:
                continue
            full_path = "%s/%s" % (json_dir, item)
            item = item.split(".")[0].split("_")[-1].split("+")
            description = ""
            for attrb in item:
                attribute_short = get_attribute_short_description(attrb)
                if attribute_short == None:
                    continue
                description += "%s + " % (attribute_short,)
            description = description[:-2]
            if len(description) == 0:
                continue
            self.attribute_name_dict[description] = full_path
            json_list.append((description, idx))
        self.drop_box3.options = json_list
        self.report_json_name = None
        if len(json_list) > 0:
            self.report_json_name = json_list[0][0]
        return

    def __update_dropbox2_hhmmss(self):
        file_list = self.items_all[self.yyyymmdd]
        options = []
        for idx, hhmmss in enumerate(file_list):
            hhmmss = hhmmss.split("/")[-1]
            options.append((hhmmss, idx))
        self.drop_box2.options = options
        self.hhmmss = None
        if len(options) > 0:
            self.hhmmss = options[0][0]
        return

    def __drop_box1_callback(self, *args):
        if args[0]["name"] == "label":
            new_value = args[0]["new"]
            self.yyyymmdd = new_value
            self.__update_dropbox2_hhmmss()
            #print("yyyymmdd : %s" % new_value)

    def __drop_box2_callback(self, *args):
        if args[0]["name"] == "label":
            new_value = args[0]["new"]
            self.hhmmss = new_value
            self.__update_dropbox3_attribute()
            #print("hhmmss : %s" % new_value)

    def __drop_box3_callback(self, *args):
        if args[0]["name"] == "label":
            new_value = args[0]["new"]
            self.report_json_name = self.attribute_name_dict[new_value]
            #print("hhmmss : %s" % self.report_json_name )

    def __show_graph_callback(self, arg):
        json_dir = "%s/%s/%s" % (self.base_archive, self.yyyymmdd, self.hhmmss)
        file_list = os.listdir(json_dir)
        json_list = []
        #for idx, item in enumerate(file_list):
        #    if len(item) < 6 or ".json" not in item[-5:]:
        #        continue
        #    full_path = "%s/%s" % (json_dir, item)
        #    json_list.append(full_path)
        #clear_output(wait=True)
        #self.select_item()
        json_list.append(self.report_json_name)
        self.e = ShowChart(json_list)
        self.e.show_graph()
        #self.interactive_plot = interactive(self.e.show_graph)
        #self.interactive_plot

    def select_item(self):
        yyyymmdd_list = []
        for idx, yyyymmdd in enumerate(self.items_all.keys()):
            yyyymmdd_list.append((yyyymmdd, idx))
        drop_box1 = widgets.Dropdown(
                options=yyyymmdd_list,
                value=0,
                description='YYYYMMDD:',
                )
        drop_box1.observe(self.__drop_box1_callback)
        display(drop_box1)

        self.hhmmdd_list = [("None", 0), ]
        self.drop_box2 = widgets.Dropdown(
                options=self.hhmmdd_list,
                value=0,
                description='HHMMSS:',
                )
        self.drop_box2.observe(self.__drop_box2_callback)
        if len(yyyymmdd_list) > 0:
            self.yyyymmdd = yyyymmdd_list[0][0]
            self.__update_dropbox2_hhmmss()
        display(self.drop_box2)

        self.file_list = [("None", 0), ]
        self.drop_box3 = widgets.Dropdown(
                options=self.file_list,
                value=0,
                description='Attribute:',
                )
        self.drop_box3.observe(self.__drop_box3_callback)
        self.__update_dropbox3_attribute()
        display(self.drop_box3)

        button = widgets.Button(description="show-graph")
        button.on_click(self.__show_graph_callback)
        display(button)

    def get_seleted_item(self):
        return self.yyyymmdd, self.hhmmss

def main():
    e = ItemSelector(base_dir)
    e.select_item()

if "__main__" == __name__:
    main()
