import os
import sys
sys.path.append("../../mail_tool")
sys.path.append("../../3th/chilkat-9.5.0-python-3.8-x86_64-linux")
from eml_to_html import *
from django.shortcuts import render
 
def index(request):
    msg  = 'My Message'
    template_path="home/templates"
    test_eml = None
    for item in request.GET.keys():
        #test_eml="/home/mailadm/data/raw_mails/twbr/terracespamadm/20200506/1150/2020-05-06-11:51:57:657629.qs"
        test_eml = item
        break
    if test_eml == None:
        msg  = '파라미터 없음; http://172.22.1.138:9000/?[QS 파일 이름]'
        return render(request, '%s/%s' % (template_path, "index.html"), {'message': msg})
    save_path="./home/templates/home/templates"
    conv = eml2Html_Converter(test_eml, save_path)
    html_file = conv.do_convert()
    if html_file == None:
        msg  = 'QS 파일 없음; not exist file "%s"' % test_eml
        return render(request, '%s/%s' % (template_path, "index.html"), {'message': msg})
    nErr =  render(request, '%s/%s' % (template_path, html_file.split("/")[-1]), {'message': msg})
    os.remove(html_file)
    return nErr
