#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_type_def import *

class queryBase():
    def __init__(self):
        self.start_time     = None
        self.query_time     = None
        self.parse_time     = None
        self.sort_time      = None
        self.mail_info_dict = {}
        self.attribute_count = 0

    def report_result(self):
        str_report = ""
        if self.sort_time == None:
            CRT("[report_time_consume] : Error. Not exist time record")
            return None
        query_time = self.query_time - self.start_time
        parse_time = self.parse_time - self.query_time
        sort_time  = self.sort_time  - self.parse_time
        total_time = self.sort_time  - self.start_time

        str_report += "==============================================================================\n"
        str_report += " Mongo DB query time      : %0.4f sec\n" % (query_time,)
        #str_report += " query result parse time  : %0.4f sec\n" % (parse_time,)
        str_report += " result sort time         : %0.4f sec\n" % (sort_time,)
        str_report += "------------------------------------------------------------------------------\n"
        str_report += " total time consume       : %0.4f sec\n" % (total_time,)
        str_report += "------------------------------------------------------------------------------\n"
        str_report += " attribute_count          : %s\n" % (self.attribute_count,)
        str_report += "==============================================================================\n"
        PRINT(str_report)
        return total_time

    # attribute_list
    def _result_reshape_to_sorted_list(self, attribute_dict):
        # TODO: {SPAM : {}, NORMAL: {}} --> {SPAM : [{},], NORMAL: [{},]}
        for spam_type in [SPAM, NORMAL]:
            attribute_dict[spam_type]=sorted(attribute_dict[spam_type].items(), key=operator.itemgetter(1), reverse=True)
            for idx,value in enumerate(attribute_dict[spam_type]):
                item  = value[0]
                count = value[1]
                attribute_dict[spam_type][idx] = {item:count}
        return attribute_dict

    # attribute_list
    def _result_reshape_to_dict(self, attribute_dict):
        spam = attribute_dict[SPAM]
        norm = attribute_dict[NORMAL]
        spam_dict = {}
        norm_dict = {}
        for item in spam:
            key     = list(item.keys())[0]
            count   = item[key]
            spam_dict[key] = count
        for item in norm:
            key     = list(item.keys())[0]
            count   = item[key]
            norm_dict[key] = count

        all_dict = {SPAM : spam_dict, NORMAL: norm_dict}

        return all_dict


    def _result_reshape_to_sorted_rank(self, attribute_ranked):
        #attribute_ranked=sorted(attribute_ranked.items(), key=operator.itemgetter(1)["rate"], reverse=True)
        sorted_rank = []
        for item in attribute_ranked.items():
            _name = item[0]
            _dict = item[1]
            item_dict = _dict
            item_dict["key"] = _name
            #print(item_dict)
            sorted_rank.append(item_dict)

        sorted_rank = sorted(sorted_rank, key=operator.itemgetter("rate", "spam-count"), reverse=True)

        for item in sorted_rank:
            try:
                key = item["key"]
            except Exception as e:
                continue
            try:
                mail_info_list = self.mail_info_dict[key]["qs_name"]
                last_occurred  = self.mail_info_dict[key]["last_occurred"] 
                first_occurred = self.mail_info_dict[key]["first_occurred"]
                report_domain  = self.mail_info_dict[key]["report_domain"]
                n_domain_kind  = len(list(set(report_domain)))
                item["mail-list"]      = sorted(mail_info_list)
                item["last_occurred"]  = last_occurred.strftime("%Y-%m-%d_%H:%M:%S")
                item["first_occurred"] = first_occurred.strftime("%Y-%m-%d_%H:%M:%S")
                item["n-domain-kind"]  = n_domain_kind
                item["report-domain"]  = list(set(report_domain))
            except Exception as e:
                item["mail-list"] = None
            item["key"] = decode_result(key) # 값이 base64 으로 인코딩 되었다면, 디코딩을 해준다.

        return sorted_rank
    





