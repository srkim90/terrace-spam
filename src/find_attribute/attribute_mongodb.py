#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *

# import querys
from query_default import *
from query_yyyymmdd import *
from attribute_type_def import *

base_dir = os.environ['MAIL_DATA_HOME']

class workBase:
    def __init__(self):
        self.work_func       = None
        self.query_func      = None
        self.query_cond      = None
        self.query_target    = None

        self.start_time      = None
        self.query_time      = None
        self.parse_time      = None
        self.sort_time       = None
        self.query_obj       = None
        self.attribute_count = 0
        save_file            = self._make_outname_by_class_name()
        self.save_name       = self._get_save_name(save_file)

    def _make_outname_by_class_name(self):
        # worker_IsAttachDoubleExtension --> is_attach_executable
        class_name = self.__class__.__name__
        class_name = class_name.replace("worker_", "")
        return "%s.json" % (class_name,)
        
    def do_query(self, db):
        if self.work_func == None or self.query_func == None:
            ERR("Error. Not exist work func : self.work_func=%s, self.query_func=%s" % (self.work_func, self.query_func))
            return None, None
    
        if self.query_cond == None or self.query_target == None:
            ERR("Error. Not exist query : self.query_cond=%s, self.query_target=%s"  % (self.query_cond, self.query_target))
            return None, None
        attribute_list, attribute_ranked = self.query_func(db, self.query_cond, self.query_target, self.work_func)

        #print("%s" % len(attribute_list))
        return attribute_list, attribute_ranked
        
    def run(self, db):
        attribute_list, attribute_ranked = self.do_query(db)
        
        if self.save_result(attribute_list, attribute_ranked) == False:
            return False
        return True

    def report_time_consume(self):
        if self.query_obj != None:
            return self.query_obj.report_result()
        return

    def get_query(self):
        return self.query_cond, self.query_target

    def _enroll_query_handling_func(self, work_func, save_name, query_cond, query_target, query_func_name):
        self.work_func      = work_func
        self.save_name      = save_name
        self.query_cond     = query_cond
        self.query_target   = query_target
        try:
            query_class     = getattr(sys.modules[__name__], query_func_name)
            self.query_obj  = query_class()
            self.query_func = self.query_obj.callback_func
        except Exception as e:
            print("_enroll_query_handling_func: %s" % e)
            return False
        return True

    def _get_base_dir(self, option_dir=None):
        save_dir  = "%s/attribute_data" % base_dir
        if option_dir != None:
            save_dir = "%s/%s" % (save_dir, option_dir)
            if os.path.exists(save_dir) == False:
                os.makedirs(save_dir)       
        return save_dir

    def _get_save_name(self, file_name, option_dir=None):
        save_dir  = self._get_base_dir(option_dir)
        save_name = "%s/%s" % (save_dir, file_name)
        return save_name

    def _check_is_spam(self, one_result):
        if "file-name" in one_result["mail-feature"].keys():
            eml_name = one_result["mail-feature"]["file-name"]
            if len(eml_name) > 4 and eml_name[-4:] == ".eml":
                one_result["mail-feature"]["is-spam"] = True
        is_spam = one_result["mail-feature"]["is-spam"]
        if is_spam == True:
            return SPAM
        return NORMAL

    def save_result(self, attribute_dict, attribute_ranked):
        save_file  = self.save_name.split("/")[-1]
        save_dir   = self.save_name.replace(save_file, "")
        save_meta  = "%s/qs_path" % (save_dir, )
        if os.path.exists(save_meta) == False:
            os.makedirs(save_meta)

        # attribute_dict
        if attribute_dict != None:
            new_name = "%s/attribute_dict_%s" % (save_dir, save_file,)
            attribute_json = json.dumps(attribute_dict, indent=4, ensure_ascii=False)
            with open(new_name, "w") as fd:
                fd.write(attribute_json)

        # attribute_rank
        if attribute_ranked != None:
            new_name = "%s/attribute_rank_%s" % (save_dir, save_file,)
            for idx, item in enumerate(attribute_ranked):
                if "mail-list" not in item.keys():
                    continue
                meta_name = "%s/%s_%05d.json" % (save_meta, save_file[:-5], idx)
                mail_list_json = json.dumps(item["mail-list"], indent=4, ensure_ascii=False)
                item["mail-list"] = meta_name
                with open(meta_name, "w") as fd:
                    fd.write(mail_list_json)
            try:
                attribute_json = json.dumps(attribute_ranked, indent=4, ensure_ascii=False)
            except TypeError as e: # ???
                #print(e)
                #print(attribute_ranked)
                for item in attribute_ranked:
                    #print(item)
                    json.dumps(item, indent=4, ensure_ascii=False)
                pass
            with open(new_name, "w") as fd:
                fd.write(attribute_json)
            print("saved : %s" % new_name)
        return True
