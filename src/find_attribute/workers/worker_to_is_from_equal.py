#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

class worker_toIsFromEqual(workBase):
    def __init__(self):
        super().__init__()
        save_name       = self._get_save_name("to_is_from_equal.json")
        query_cond      = {"head-items.From"   : {"$exists": True},
                           "head-items.To"     : {"$exists": True},}
        query_target    = {"head-items.From"   :  True,
                           "head-items.To"     :  True,
                            }
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        is_same = False
        dict_from = one_result["head-items"]["From"]
        dict_to   = one_result["head-items"]["To"]
        is_spam = self._check_is_spam(one_result)

        if dict_to == None or dict_from == None:
           return None, None

        if "uid" in dict_from.keys() and "uid" in dict_to.keys():
            to_uid      = dict_to["uid"]
            from_uid    = dict_from["uid"]
            if to_uid == from_uid:
                is_same = True
        return is_spam, is_same

def main():
    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 3. Init attribute parsor & run
    worker = worker_toIsFromEqual()
    worker.run(db)
    worker.report_time_consume()

    return

if __name__ == "__main__":
    main()   
