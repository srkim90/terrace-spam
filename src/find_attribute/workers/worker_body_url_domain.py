#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

class worker_BodyUrlDomain(workBase):
    def __init__(self):
        super().__init__()
        save_name       = self._get_save_name("body_url_domain.json")
        query_cond      = {"body-items.Domin-List"   : {"$exists": True}}
        query_target    = {"body-items.Domin-List"   :  True,}
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        domain_list = []
        attachs = one_result["body-items"]
        is_spam = self._check_is_spam(one_result)

        for item in attachs:
            if 'Domin-List' not in item.keys():
                continue
            domain_list_at = item["Domin-List"]
            for domain in domain_list_at:
                if domain not in domain_list:
                    domain_list.append(domain)
        return is_spam, domain_list

def main():
    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 3. Init attribute parsor & run
    worker = worker_BodyUrlDomain()
    worker.run(db)
    worker.report_time_consume()

    return

if __name__ == "__main__":
    main()   
