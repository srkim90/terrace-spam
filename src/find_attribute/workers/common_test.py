#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

def testcase_main():
    # 1. find worker
    worker_class = None
    for item in dir(sys.modules["__main__"]):
        if "worker_" not in item:
            continue
        worker_class = getattr(sys.modules["__main__"], item)
        break

    # 2. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 3. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 4. Init attribute parsor & run
    worker = worker_class()
    worker.run(db)
    worker.report_time_consume()

    return



