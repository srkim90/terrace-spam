#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

class worker_Cc_Count(workBase):
    def __init__(self):
        super().__init__()
        save_name       = self._get_save_name("cc_count.json")
        query_cond      = {"head-items.Cc.domain" : {"$exists": True}}
        query_target    = {"head-items.Cc.domain" : True,}
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
#        PRINT(one_result)
        domain_list = []
        cc_domain = one_result["head-items"]["Cc"]
        is_spam = self._check_is_spam(one_result)

        domain_list_at = cc_domain["domain"]
        if domain_list_at == None:
            return None, None
        dom_count = len(domain_list_at)

#        print("LIST : %s Dom_Count : %d"%(domain_list_at, dom_count))
        return is_spam, dom_count

def main():
    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 3. Init attribute parsor & run
    worker = worker_Cc_Count()
    worker.run(db)
    worker.report_time_consume()

    return

if __name__ == "__main__":
    main()   
