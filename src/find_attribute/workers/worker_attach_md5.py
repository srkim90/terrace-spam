#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

class worker_AttachMD5(workBase):
    def __init__(self):
        super().__init__()
        save_name       = self._get_save_name("attach_md5.json")
        query_cond      = {"body-items.MD5-Sum"   : {"$exists": True}}
        query_target    = {"body-items.MD5-Sum"   :  True,}
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        md5_list = []
        attachs = one_result["body-items"]
        is_spam = self._check_is_spam(one_result)

        for item in attachs:
            if 'MD5-Sum' not in item.keys():
                continue
            md5sum = item["MD5-Sum"]
            md5_list.append(md5sum)
        return is_spam, md5_list

def main():
    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 3. Init attribute parsor & run
    worker = worker_AttachMD5()
    worker.run(db)
    worker.report_time_consume()

    return

if __name__ == "__main__":
    main()   
