#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

class worker_ArchiveInnerFilesName(workBase):
    def __init__(self):
        super().__init__()
        save_name       = self._get_save_name("archive_inner_files_name.json")
        query_cond      = {"body-items.File-List"   : {"$exists": True}}
        query_target    = {"body-items.File-List"   :  True,}
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        md5_list = []
        attachs = one_result["body-items"]
        is_spam = self._check_is_spam(one_result)

        for item in attachs:
            if 'File-List' not in item.keys():
                continue
            file_list = item["File-List"]
            if file_list == None:
                continue
            for in_file in file_list:
                in_file = in_file["filename"]
                md5_list.append(in_file)
        return is_spam, md5_list

def main():
    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 3. Init attribute parsor & run
    worker = worker_ArchiveInnerFilesName()
    worker.run(db)
    worker.report_time_consume()

    return

if __name__ == "__main__":
    main()   
