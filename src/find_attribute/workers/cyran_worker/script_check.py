#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *
sys.path.append("../../../synap_helper")
from synap_helper import *

class worker_HaveOleDocuments(workBase):
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.synap_vba"       : {"$exists": True}}
        query_target    = {"body-items.synap_vba"       :  True,
                           "mail-feature.cyren-result"  :  True,
                           "mail-feature.sip-country"   :  True,
                           "mail-feature.file-name"     :  True,
                           "mail-feature.source_site"   :  True,
                           "body-items.Attach-Name"     :  True,
                          }
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise

        self.ole_qs_dict = {}
        self.__load_qs_file_list()
        self.stat_dict = {
                "auto_exec"     : {
                    # A : {
                    #   virus   : 10,
                    #   normal  : 7,
                    # },
                },
                "suspicious"    : {
                },
            }
        self.n_call = 0
        return
    def print_result(self):
        #print(self.stat_dict)
        print("[auto_exec]")
        print("%-20s %-10s %-10s" % ("[keyword]", "[n_virus]", "[n_normal]"))
        for item in self.stat_dict["auto_exec"].keys():
            n_virus     = self.stat_dict["auto_exec"][item]["virus"]
            n_normal    = self.stat_dict["auto_exec"][item]["normal"]
            print("%-20s %-10d %-10d" % (item, n_virus, n_normal))

        print("\n\n[suspicious]")
        print("%-20s %-10s %-10s" % ("[keyword]", "[n_virus]", "[n_normal]"))
        for item in self.stat_dict["suspicious"].keys():
            n_virus     = self.stat_dict["suspicious"][item]["virus"]
            n_normal    = self.stat_dict["suspicious"][item]["normal"]
            print("%-20s %-10d %-10d" % (item, n_virus, n_normal))


    def __load_qs_file_list(self):
        cyran_classfy    = "/home/mailadm/data/attachment/index"
        self.virus_list  = []
        self.normal_list = []
        last_qs_file = None
        for item in os.listdir(cyran_classfy):
            if item.split(".")[-1] != "json" or "index_" not in item:
                continue
            with open("%s/%s" % (cyran_classfy, item), "r") as fd:
                lines = fd.readlines()
                for line in lines:
                    if "qsPath" in line:
                        qs_name = "%s" % line.split("\"")[3].split("/")[-1]
                        #qs_name = "%s" % line.split("\"")[3]
                        if "virus" in item:
                            self.virus_list.append(qs_name)
                        else:
                            self.normal_list.append(qs_name)
                        last_qs_file = qs_name
                    elif "SavePath" in line:
                        ole_path = "%s" % line.split("\"")[3]
                        self.ole_qs_dict[last_qs_file] = ole_path

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
        ####################################################
        #result = b_result # TODO : set result
        source_site     = one_result["mail-feature"]["source_site"]
        file_name       = one_result["mail-feature"]["file-name"].split("/")[-1]

        is_virus = False
        if file_name in self.virus_list:
            is_virus = True
            #print("virus  : %s" % (file_name,))
        elif file_name in self.normal_list:
            #print("normal : %s" % (file_name,))
            pass
        else:
            return is_spam, result

        file_name = file_name.split("/")[-1] 

        body_item = one_result["body-items"]
        for item in body_item:
            if "synap_vba" in item.keys():

                self.n_call += 1
                ole_path = self.ole_qs_dict[file_name]
                print("%d %s" % (self.n_call, ole_path))
                ##########################
                with open(ole_path, "rb") as fd:
                    doc_data = fd.read()
                synap_vba = synap_get_vbs_info(doc_data)
                #########################
                #synap_vba = item["synap_vba"]
                #########################
                if synap_vba == None:
                    break
                n_autoexec_keyword   = synap_vba["n_autoexec_keyword"]
                n_suspicious_keyword = synap_vba["n_suspicious_keyword"]
                auto_exec_keywords   = synap_vba["auto_exec_keywords"]
                suspicious_keywords  = synap_vba["suspicious_keywords"]
                #print("%s %s %s" % (is_virus, n_autoexec_keyword, n_suspicious_keyword))
                for word in auto_exec_keywords:
                    if word not in self.stat_dict["auto_exec"]:
                        self.stat_dict["auto_exec"][word] = {"virus" : 0, "normal" : 0}
                    if is_virus == True:
                        self.stat_dict["auto_exec"][word]["virus"] += 1
                    else:
                        self.stat_dict["auto_exec"][word]["normal"] += 1
                for word in suspicious_keywords:
                    if word not in self.stat_dict["suspicious"]:
                        self.stat_dict["suspicious"][word] = {"virus" : 0, "normal" : 0}
                    if is_virus == True:
                        self.stat_dict["suspicious"][word]["virus"] += 1
                    else:
                        self.stat_dict["suspicious"][word]["normal"] += 1
                break

        ####################################################
        return is_spam, result


def main():
    # 1. find worker
    worker_class = None
    for item in dir(sys.modules["__main__"]):
        if "worker_" not in item:
            continue
        worker_class = getattr(sys.modules["__main__"], item)
        break

    # 2. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 3. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 4. Init attribute parsor & run
    worker = worker_class()
    worker.run(db)
    worker.print_result()

    return

if __name__ == "__main__":
    main()
