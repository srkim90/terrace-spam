#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

class worker_HaveOleDocuments(workBase):
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items"                 : {"$exists": True}}
        query_target    = {"body-items.synap_vba"       :  True,
                           "mail-feature.cyren-result"  :  True,
                           "mail-feature.sip-country"   :  True,
                           "mail-feature.file-name"     :  True,
                           "mail-feature.source_site"   :  True,
                           "body-items.Attach-Name"     :  True,
                          }
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise

        self.combination_list = []
        self.ext_list = ["0xe", "73k","89k","a6p","a7r","ac","acc","ace","acr","action","actm","ahk","air","apk","app","applescript","arscript","asb","azw2","ba_","bat","beam","bin","btm","cab","caction","cel","celx","cgi","cmd","cof","coffee","com","command","csh","cyw","dek","dld","dmc","ds","dxl","e_e","ear","ebm","ebs","ebs2","ecf","eham","elf","es","esh","ex_","ex4","exe","exe1","exopc","ezs","ezt","fas","fky","fpi","frs","fxp","gadget","gpe","gpu","gs","ham","hms","hpf","hta","htm","icd","iim","img","ipa","ipf","iso","isu","ita","jse","jsx","kix","ksh","lnk","lo","ls","m3g","mam","mcr","mel","mem","mio","mm","mpx","mrc","mrp","ms","msi","msl","mxe","n","nexe","ore","osx","otm","out","paf","pex","phar","pif","plsc","plx","prc","prg","ps1","pvd","pwc","pyc","pyo","qit","qpx","rar","rbx","rfu","rgs","rox","rpj","run","rxe","s2a","sbs","sca","scar","scb","scpt","scptd","scr","script","sct","seed","shb","smm","spr","tcp","thm","tms","u3p","udf","upx","vb","vbe","vbs","vbscript","vdo","vexe","vlx","vpm","vxp","wcm","widget","wiz","workflow","wpk","wpm","ws","wsf","wsh","x86","xap","xbap","xlm","xqt","xys","zl9","src"]
        return

    def __check_executable(self, file_name):
        if file_name == None:
            return False
        ext_name = file_name.split(".")[-1].lower()
        if ext_name in self.ext_list:
            return True
        return False

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
        ####################################################
        #result = b_result # TODO : set result
        synap_vba       = False
        have_script     = False
        cyren_result    = False
        source_site     = one_result["mail-feature"]["source_site"]
        file_name       = one_result["mail-feature"]["file-name"]
        if source_site == None:
            source_site = "twbr"
        if "cyren-result" in one_result["mail-feature"].keys():
            cyren_result = one_result["mail-feature"]["cyren-result"]
        try:
            sip_country  = one_result["mail-feature"]["sip-country"]
            if sip_country == None:
                sip_country = "PRIVATE"
            elif sip_country != "KR":
                sip_country = "ABROAD"
        except KeyError as e:
            sip_country = "PRIVATE"
        if cyren_result == None:
            cyren_result = False
        else:
            cyren_result = True
        body_item = one_result["body-items"]
        have_executable = False
        for item in body_item:
            if "synap_vba" in item.keys():
                synap_vba = True
                if item["synap_vba"] and item["synap_vba"]["n_vba_file"] != 0:
                    have_script = True
            if "Attach-Name" in item.keys():
                if have_executable == False:
                    have_executable = self.__check_executable(item["Attach-Name"])
        combination_dict = {
            #"synap_vba"      : synap_vba,
            "cyren_result"    : cyren_result,
            "sip_country"     : sip_country,
            #"have_executable" : have_executable,
            "is_spam"         : is_spam,
            "have_script"     : have_script,
            "source_site"     : source_site,
        }

        if have_executable == False and synap_vba == False:
            return is_spam, result
        f_ok = False
        for item in self.combination_list:
            if item["combination"] == combination_dict:
                item["count"] += 1
                item["list"].append(file_name)
                f_ok = True
                break
        if f_ok == False:
            item = {
                "combination" : combination_dict,
                "count"       : 1,
                "list"        : [file_name,],
            }
            self.combination_list.append(item)
        ####################################################
        return is_spam, result

    def print_result(self):
        key_list = [
                    "source_site",
                    "have_script", 
                    "cyren_result", 
                    "sip_country", 
                    #"have_executable", 
                    "is_spam"]
        result_str ="idx "
        for item in key_list:
            result_str  += "%s " % item
        print(result_str)
        for idx, item in enumerate(self.combination_list):
            combination = item["combination"]
            count       = item["count"]
            file_list   = item["list"]
            result_str  = "%d " % idx
            my_combi    = ""
            now_rate    = 0.0
            for key in key_list:
                result_str += "%s " % (combination[key])
                if key != "is_spam":
                    my_combi += "%s" % combination[key]
            for jdx, item2 in enumerate(self.combination_list):
                file_list = item2["list"]
                combination2 = item2["combination"]
                count2       = item2["count"]
                now_combi = ""
                for key in key_list:
                    if key != "is_spam":
                        now_combi += "%s" % combination2[key]
                if now_combi == my_combi and combination2["is_spam"] != "SPAM":
                    #print("[%s] [%s]" % (now_combi, my_combi))
                    now_rate = (float(count) / (float(count + count2))) * 100.0
            #if combination["is_spam"] != "SPAM":
            #    continue
                           
            print("%s %d %f" % (result_str, count, now_rate))
            file_name = "%s.txt" % (result_str.replace(" ", "_"),)
            with open(file_name, "w") as fd:
                for qs_name in file_list:
                    fd.write("%s\n" % qs_name)

def main():
    # 1. find worker
    worker_class = None
    for item in dir(sys.modules["__main__"]):
        if "worker_" not in item:
            continue
        worker_class = getattr(sys.modules["__main__"], item)
        break

    # 2. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 3. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 4. Init attribute parsor & run
    worker = worker_class()
    worker.run(db)
    worker.print_result()
    #worker.report_time_consume()

    return

if __name__ == "__main__":
    main()
