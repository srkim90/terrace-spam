# -*- coding: utf-8 -*-

'''
  Author   : Kim, Seongrae
  Filename : __init__.py
  Release  : 1
  Date     : 2020-03-26
 
  Description : common module entry
 
  Notes :
  ===================
  History
  ===================
  2020/03/26  created by Kim, Seongrae
'''

# common package import
import re
import os
import sys
import imp
import pytz
import json
import redis
import timeit
import threading
import inspect
import pkgutil
from time import *
from datetime import *

g_mod_list = []

def get_all_worker_class():
    return g_mod_list

def load_from_file(filepath):
    class_inst = None
    expected_class = 'worker_'
    mod_name,file_ext = os.path.splitext(os.path.split(filepath)[-1])

    if file_ext.lower() == '.py':
        py_mod = imp.load_source(mod_name, filepath)

    elif file_ext.lower() == '.pyc':
        py_mod = imp.load_compiled(mod_name, filepath)
    for attribute in dir(py_mod):
        if len(attribute) <= len(expected_class) + 1:
            continue
        if attribute[:len(expected_class)] == expected_class:
            class_inst = getattr(py_mod, attribute)
            break
    return class_inst

def import_workers_pakag():
    global g_mod_list
    base_dir = "./workers"
    filenames  = os.listdir(base_dir)
    _filenames = os.listdir("%s/ysw_worker" % base_dir)
    for file_at in _filenames:
        filenames.append("%s/%s" % ("ysw_worker", file_at))
    _filenames = os.listdir("%s/srkim_worker" % base_dir)
    for file_at in _filenames:
        filenames.append("%s/%s" % ("srkim_worker", file_at))
    _filenames = os.listdir("%s/cyran_worker" % base_dir)
    for file_at in _filenames:
        filenames.append("%s/%s" % ("cyran_worker", file_at))
    #_filenames = os.listdir("%s/ysw_worker/immutability" % base_dir)
    #for file_at in _filenames:
    #    filenames.append("%s/%s" % ("ysw_worker/immutability", file_at))
    for file_name in filenames:
        # worker_*.py
        if len(file_name) < 10:
            continue
        last_file_name = file_name.split("/")[-1]
        if last_file_name[-3:] != ".py" or last_file_name[:7] != "worker_":
            continue
        file_name = "%s/%s" % (base_dir, file_name)
        class_inst = load_from_file(file_name)
        g_mod_list.append(class_inst)

import_workers_pakag()
