#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

class worker_IsNoVisitedCountry(workBase):
    def __init__(self):
        super().__init__()
        query_cond      = {"head-items.Received"     : {"$exists": True},
                           "mail-feature.sip"        : {"$exists": True}
                          }
        query_target    = {"head-items.Received"     :  True,
                           "mail-feature.sip"        :  True,
                           "mail-feature.file-name"  : True,
                          }
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
        start_out       = False
        ln_country      = 0
        sip             = one_result["mail-feature"]["sip"]
        Receiveds       = one_result["head-items"]["Received"]
        receiveds_list  = []
        internal_list   = []

        if Receiveds != None:
            for idx,item in enumerate(Receiveds):
                ipaddr  = item["ipaddr"]
                country = item["country"]
                if start_out == False and sip == ipaddr:
                    start_out = True
                if start_out == False:
                    internal_list.append("%d:%s" % (idx,country))
                    continue
                #print("ipaddr=%s, country=%s" % (ipaddr, country))
                receiveds_list.append("%d:%s" % (idx,country))
        else:
            receiveds_list = []
        ####################################################
        result = True if len(receiveds_list) < 2 else False # TODO : set result
        ####################################################

        if len(receiveds_list) == 0:
            #file_name = one_result["mail-feature"]["file-name"]
            #print("%-5s : %-65s / %s, %s %s" % (result, receiveds_list, internal_list, sip, file_name))
            pass
        return is_spam, result


if __name__ == "__main__":
    testcase_main()
