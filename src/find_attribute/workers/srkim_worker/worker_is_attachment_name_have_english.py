#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

class worker_IsAttachmentNameHaveEnglish(workBase):                             # TODO : change class name
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.Attach-Name"   : {"$exists": True}}      # TODO : change query condition
        query_target    = {"body-items.Attach-Name"   :  True,}                 # TODO : change query target
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        self.__init_regexp()
        return

    def __init_regexp(self):
        self.re_dict = { 
            "n_include_num"   : re.compile('[0-9]'),
            "n_include_en"    : re.compile('[a-zA-Z]'),
            "n_include_ko"    : re.compile('[\u1100-\u11FF\u3130-\u318F\uAC00-\uD7AF]'),
            "n_include_jp"    : re.compile('[\u3040-\u309F\u30A0-\u30FF\u31F0-\u31FF]'),
            "n_include_ch"    : re.compile('[\u2e80-\u2eff\u31c0-\u31ef\u3200-\u32ff\u3400-\u4dbf\u4e00-\u9fbf\uf900-\ufaff]'),
            "n_include_url"   : re.compile('((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)'),
            "n_include_email" : re.compile('[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}'),
        }

    def __check_regexp(self, check_type, string):
        if check_type not in self.re_dict.keys():
            return False
        regexp = self.re_dict[check_type]
        return regexp.findall(string)
        
    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
        bool_list   = []
        attachNames = one_result["body-items"]
        for item in attachNames:
            try:
                AttachName = item["Attach-Name"]
            except KeyError:
                continue
            if AttachName == None:
                continue
            AttachName = AttachName.split(".")[0]
            result_at = self.__check_regexp("n_include_en", AttachName)
            if len(result_at) == 0:
                bool_at = False
            else:
                bool_at = True
            bool_list.append(bool_at)
        ####################################################
        result = bool_list # TODO : set result
        ####################################################
        return is_spam, result


if __name__ == "__main__":
    testcase_main()
