#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

class worker_IsNotExistSubject(workBase):
    def __init__(self):
        super().__init__()
        query_cond      = {"head-items"     : {"$exists": True}}
        query_target    = {"head-items"     :  True}
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
        hdrItems = one_result["head-items"]
        b_is_null = False
        if hdrItems != None:
            if "Subject" not in hdrItems.keys():
                b_is_null = True

        ####################################################
        result = b_is_null# TODO : set result
        ####################################################
        return is_spam, result


if __name__ == "__main__":
    testcase_main()
