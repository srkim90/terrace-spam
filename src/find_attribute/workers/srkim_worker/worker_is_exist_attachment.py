#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

class worker_IsExistAttachment(workBase):
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.Attach-Name" : {"$exists": True}}
        query_target    = {"body-items.Category"    :  True,
                           "body-items.Attach-Name" :  True}
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        have_attach = False
        ####################################################
        for item in one_result["body-items"]:
            if "Category" not in item.keys():
                continue
            if item["Category"] == 1000:
                continue
            have_attach = True
        ####################################################
        result = have_attach
        ####################################################
        return is_spam, result


if __name__ == "__main__":
    testcase_main()
