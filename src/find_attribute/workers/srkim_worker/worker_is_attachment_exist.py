#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

class worker_IsAttachmentExist(workBase):                                        # TODO : change class name
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.Content-Type"   : {"$exists": True}}      # TODO : change query condition
        query_target    = {"body-items.Content-Type"   :  True,}                 # TODO : change query target
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
        b_have_attach = False
        attachNames = one_result["body-items"]
        for item in attachNames:
            try:
                AttachType = item["Content-Type"]
            except KeyError:
                continue
            if AttachType == None:
                continue
            if "text" not in AttachType and "multi" not in AttachType:
                b_have_attach = True
                break
                
        ####################################################
        result = b_have_attach # TODO : set result
        ####################################################
        return is_spam, result


if __name__ == "__main__":
    testcase_main()
