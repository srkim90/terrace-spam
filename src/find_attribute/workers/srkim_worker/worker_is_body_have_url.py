#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

class worker_IsBodyHaveUrl(workBase):
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.URL-List"     : {"$exists": True}}
        query_target    = {"body-items.URL-List"     :  True,}                 
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
        ln_url  = 0
        attachNames = one_result["body-items"]
        b_result = False
        for item in attachNames:
            try:
                urlList = item["URL-List"]
            except KeyError:
                continue
            if urlList == None:
                continue
            ln_url += len(urlList)
        if ln_url > 0:
            b_result = True
        ####################################################
        result = b_result # TODO : set result
        ####################################################
        return is_spam, result


if __name__ == "__main__":
    testcase_main()
