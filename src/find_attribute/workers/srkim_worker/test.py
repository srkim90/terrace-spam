#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

'''
class worker_Test(workBase):                                                     # TODO : change class name
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.content.ole_meta"        : {"$exists": True}}
        query_target    = {"body-items.content.ole_meta"        :  True,
                           "body-items.Attach-Name"             :  True,
                           "mail-feature.file-name"             :  True,
                           "mail-feature.source_site"           :  True,
                           }

        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
 
        if one_result["mail-feature"]["source_site"] == "twbr":
            if one_result["mail-feature"]["file-name"][-3:] == "eml":
                is_spam = SPAM
            print("%-4s : %s" % (is_spam, one_result["mail-feature"]["file-name"], ))
        ####################################################
        result = None # TODO : set result
        ####################################################
        return is_spam, result
'''

class worker_Test(workBase):                                                     # TODO : change class name
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.Attach-Name"             : {"$exists": True}}
        query_target    = {"body-items.File-List"                :  True,
                           "body-items.Category"                :  True,
                           "body-items.Attach-Name"             :  True,
                           "mail-feature.file-name"             :  True,
                           "mail-feature.source_site"           :  True,
                           }

        self.ext_list = ["0xe", "73k","89k","a6p","a7r","ac","acc","ace","acr","action","actm","ahk","air","apk","app","applescript","arscript","asb","azw2","ba_","bat","beam","bin","btm","cab","caction","cel","celx","cgi","cmd","cof","coffee","com","command","csh","cyw","dek","dld","dmc","ds","dxl","e_e","ear","ebm","ebs","ebs2","ecf","eham","elf","es","esh","ex_","ex4","exe","exe1","exopc","ezs","ezt","fas","fky","fpi","frs","fxp","gadget","gpe","gpu","gs","ham","hms","hpf","hta","htm","icd","iim","img","ipa","ipf","iso","isu","ita","jse","jsx","kix","ksh","lnk","lo","ls","m3g","mam","mcr","mel","mem","mio","mm","mpx","mrc","mrp","ms","msi","msl","mxe","n","nexe","ore","osx","otm","out","paf","pex","phar","pif","plsc","plx","prc","prg","ps1","pvd","pwc","pyc","pyo","qit","qpx","rar","rbx","rfu","rgs","rox","rpj","run","rxe","s2a","sbs","sca","scar","scb","scpt","scptd","scr","script","sct","seed","shb","smm","spr","tcp","thm","tms","u3p","udf","upx","vb","vbe","vbs","vbscript","vdo","vexe","vlx","vpm","vxp","wcm","widget","wiz","workflow","wpk","wpm","ws","wsf","wsh","x86","xap","xbap","xlm","xqt","xys","zl9","src"]

        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
        qs_file_name        = one_result["mail-feature"]["file-name"]
        source_site         = one_result["mail-feature"]["source_site"]

        if one_result["mail-feature"]["file-name"][-3:] == "eml":
            is_spam = SPAM

        for item in one_result["body-items"]:
            try:
                attach_name = item["Attach-Name"].lower()
            except:
                attach_name = None
            try:
                file_list   = item["File-List"]
            except:
                file_list   = None
            category        = item["Category"]

            #if source_site != "inbound.daou": 
            if source_site != "twbr": 
                break
            
            ext_name = None
            if attach_name != None:
                ext_name = attach_name.split(".")[-1]
            if attach_name != None and len(attach_name) > 4 and ext_name in self.ext_list:
                #print("%s (%-3s) : %s" % (is_spam, ext_name, qs_file_name))
                break
            is_break = False
            if category == 4000 and type(file_list) == list:
                for item in file_list:
                    file_name = item["filename"].lower()
                    ext_name = file_name.split(".")[-1]
                    if file_name != None and len(file_name) > 4 and ext_name in self.ext_list:
                        #print(file_name)
                        is_break = True
                        break
            if is_break == True:
                print("%s (%-3s) : %s" % (is_spam, ext_name, qs_file_name))
                break

        ####################################################
        result = None # TODO : set result
        ####################################################
        return is_spam, result


if __name__ == "__main__":
    testcase_main()
