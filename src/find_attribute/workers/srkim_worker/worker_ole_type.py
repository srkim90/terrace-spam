#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

class worker_OleType(workBase):
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.content.ole_meta"        : {"$exists": True}}
        query_target    = {"body-items.content.ole_meta"        :  True,
                           "body-items.Attach-Name"     :  True}
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ext         = None
        ####################################################
        for item in one_result["body-items"]:
            if "content" not in item.keys():
                continue
            if "ole_meta" not in item["content"].keys():
                continue
            if "Attach-Name" not in item.keys():
                continue
            attach_name = item["Attach-Name"]
            ext = attach_name.split(".")[-1]
        ####################################################
        result = ext
        ####################################################
        return is_spam, result


if __name__ == "__main__":
    testcase_main()
