#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

class worker_IsAttachmentHaveDocument(workBase):                                # TODO : change class name
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.Attach-Name"   : {"$exists": True}}      # TODO : change query condition
        query_target    = {"body-items.Attach-Name"   :  True,}                 # TODO : change query target
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
        bool_list   = []
        check_list  = ["xlsx", "docx", "pptx", "pdf", "hwp", "xls", "doc", "ppt", "pdf"]
        attachNames = one_result["body-items"]
        for item in attachNames:
            try:
                AttachName = item["Attach-Name"]
            except KeyError:
                continue
            if AttachName == None:
                continue
            attachExt = AttachName.split(".")[-1].lower()
            if attachExt not in check_list:
                bool_list.append(False)
            else:
                bool_list.append(True)
        ####################################################
        result = bool_list # TODO : set result
        ####################################################
        return is_spam, result


if __name__ == "__main__":
    testcase_main()
