#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

class worker_IsExistVbaSplit(workBase):
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.content.vba_detected"    : {"$exists": True}}
        query_target    = {"body-items.content.vba_detected"    :  True            }
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        have_vba    = False
        ####################################################
        for item in one_result["body-items"]:
            if "content" not in item.keys():
                continue
            item = item["content"]
            if "vba_detected" not in item.keys():
                continue
            vba_detected = item["vba_detected"]
            if len(vba_detected) > 0:
                #print(vba_detected)
                have_vba = True
                
        ####################################################
        result = have_vba
        ####################################################
        return is_spam, result


if __name__ == "__main__":
    testcase_main()
