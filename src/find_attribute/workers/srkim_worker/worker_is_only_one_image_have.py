#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

class worker_IsOnlyOneImageHave(workBase):                                      # TODO : change class name
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.Attach-Name"   : {"$exists": True},
                           "body-items.Word-List"     : {"$exists": True}}
        query_target    = {"body-items.Attach-Name"   :  True,
                           "body-items.Word-List"     :  True,}                 
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
        ln_image = 0
        ln_word  = 0
        check_list  = ["jpg", "png", "gif", "bmp"]
        attachNames = one_result["body-items"]
        b_result = False
        for item in attachNames:
            try:
                AttachName = item["Attach-Name"]
            except KeyError:
                continue
            if AttachName == None:
                continue
            attachExt = AttachName.split(".")[-1].lower()
            if attachExt in check_list:
                ln_image += 1
        for item in attachNames:
            try:
                word_list = item["Word-List"]
            except KeyError:
                continue
            ln_word += len(word_list)

        if ln_image == 1 and ln_word < 15:
            b_result = True
        ####################################################
        result = b_result # TODO : set result
        ####################################################
        return is_spam, result


if __name__ == "__main__":
    testcase_main()
