#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

class worker_subjectWordCount(workBase):
    def __init__(self):
        super().__init__()
        save_name       = self._get_save_name("subject_word_count.json")
        query_cond      = {"head-items.Subject" : {"$exists": True}}
        query_target    = {"head-items.Subject" : True,}
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        #PRINT(one_result)
        subject = one_result["head-items"]["Subject"]
        if subject == None:
            return None, None
        is_include_num      = True if subject["n_include_num"]   > 0 else False
        is_include_en       = True if subject["n_include_en"]    > 0 else False
        is_include_ko       = True if subject["n_include_ko"]    > 0 else False
        is_include_jp       = True if subject["n_include_jp"]    > 0 else False
        is_include_ch       = True if subject["n_include_ch"]    > 0 else False
        is_include_url      = True if subject["n_include_url"]   > 0 else False
        is_include_email    = True if subject["n_include_email"] > 0 else False
        is_spam             = self._check_is_spam(one_result)

        charset_dict = {
            "EN"    : is_include_en,
            "KO"    : is_include_ko,  
            "JP"    : is_include_jp,
            "CH"    : is_include_ch,
            "URL"   : is_include_url,
            "NUM"   : is_include_num,
            "EMAIL" : is_include_email,
        }
        charset_strs = charset_dict #"%s" % charset_dict
        return is_spam, charset_strs
     
def main():
    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 3. Init attribute parsor & run
    worker = worker_subjectWordCount()
    worker.run(db)
    worker.report_time_consume()

    return

if __name__ == "__main__":
    main()   
