#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

ext_list = ["exe", "dll", "so"]



ext_list = ["0xe", "73k","89k","a6p","a7r","ac","acc","ace","acr","action","actm","ahk","air","apk","app","applescript","arscript","asb","azw2","ba_","bat","beam","bin","btm","cab","caction","cel","celx","cgi","cmd","cof","coffee","com","command","csh","cyw","dek","dld","dmc","ds","dxl","e_e","ear","ebm","ebs","ebs2","ecf","eham","elf","es","esh","ex_","ex4","exe","exe1","exopc","ezs","ezt","fas","fky","fpi","frs","fxp","gadget","gpe","gpu","gs","ham","hms","hpf","hta","htm","icd","iim","img","ipa","ipf","iso","isu","ita","jse","jsx","kix","ksh","lnk","lo","ls","m3g","mam","mcr","mel","mem","mio","mm","mpx","mrc","mrp","ms","msi","msl","mxe","n","nexe","ore","osx","otm","out","paf","pex","phar","pif","plsc","plx","prc","prg","ps1","pvd","pwc","pyc","pyo","qit","qpx","rar","rbx","rfu","rgs","rox","rpj","run","rxe","s2a","sbs","sca","scar","scb","scpt","scptd","scr","script","sct","seed","shb","smm","spr","tcp","thm","tms","u3p","udf","upx","vb","vbe","vbs","vbscript","vdo","vexe","vlx","vpm","vxp","wcm","widget","wiz","workflow","wpk","wpm","ws","wsf","wsh","x86","xap","xbap","xlm","xqt","xys","zl9","src"]

def extension_ck(ext):
    if ext == None:
        return True
    elif len(ext) > 7:
        return True
    else:
        for node in ext_list:
            if(ext == node):
                return None
    return False

class worker_IsInnerExcutable(workBase):
    def __init__(self):
        super().__init__()
        save_name       = self._get_save_name("inner_excutable.json")
        query_cond      = {"body-items.File-List"   : {"$exists": True}}
        query_target    = {"body-items.File-List"   :  True,}
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        md5_list = []
        attachs = one_result["body-items"]
        is_spam = self._check_is_spam(one_result)

        for item in attachs:
            if 'File-List' not in item.keys():
                continue
            file_list = item["File-List"]
            if file_list == None:
                continue
            for in_file in file_list:
                in_file = in_file["filename"]
                if in_file != None and '.' in in_file:
                    extension = in_file.split(".")[-1]
                    ret = extension_ck(extension)
                    if ret == True:
                        continue
                    elif ret == None:
                        md5_list.append("True")
                    elif ret == False:
                        md5_list.append("False")
#                    print("md5:[%s]"%md5_list)
        return is_spam, md5_list

def main():
    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 3. Init attribute parsor & run
    worker = worker_IsInnerExcutable()
    worker.run(db)
    worker.report_time_consume()

    return

if __name__ == "__main__":
    main()   
