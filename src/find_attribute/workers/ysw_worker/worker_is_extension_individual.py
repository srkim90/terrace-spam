#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

#ext_list = ["exe", "dll", "so"]

ext_list = ["0xe", "73k","89k","a6p","a7r","ac","acc","ace","acr","action","actm","ahk","air","apk","app","applescript","arscript","asb","azw2","ba_","bat","beam","bin","btm","cab","caction","cel","celx","cgi","cmd","cof","coffee","com","command","csh","cyw","dek","dld","dmc","ds","dxl","e_e","ear","ebm","ebs","ebs2","ecf","eham","elf","es","esh","ex_","ex4","exe","exe1","exopc","ezs","ezt","fas","fky","fpi","frs","fxp","gadget","gpe","gpu","gs","ham","hms","hpf","hta","htm","icd","iim","img","ipa","ipf","iso","isu","ita","jse","jsx","kix","ksh","lnk","lo","ls","m3g","mam","mcr","mel","mem","mio","mm","mpx","mrc","mrp","ms","msi","msl","mxe","n","nexe","ore","osx","otm","out","paf","pex","phar","pif","plsc","plx","prc","prg","ps1","pvd","pwc","pyc","pyo","qit","qpx","rar","rbx","rfu","rgs","rox","rpj","run","rxe","s2a","sbs","sca","scar","scb","scpt","scptd","scr","script","sct","seed","shb","smm","spr","tcp","thm","tms","u3p","udf","upx","vb","vbe","vbs","vbscript","vdo","vexe","vlx","vpm","vxp","wcm","widget","wiz","workflow","wpk","wpm","ws","wsf","wsh","x86","xap","xbap","xlm","xqt","xys","zl9","src"]

 
def extension_ck(ext):
    if ext == None:
        return True
    elif len(ext) > 7:
        return True
    else:
        for node in ext_list:
            if(ext == node):
                return None
    return False

class worker_IsExtensionIndividual(workBase):
    def __init__(self):
        super().__init__()
#        save_name       = self._get_save_name("extension_individual.json")
        query_cond      = {"body-items.Attach-Name" : {"$exists": True}}
        query_target    = {"body-items.Attach-Name" : True,}
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        extension_list = []
#        PRINT(one_result)
        attach_list = one_result["body-items"]
        is_spam = self._check_is_spam(one_result)
        for attach_at in attach_list:
            extension = None
            if attach_at and "Attach-Name" in attach_at.keys():
                file_name = attach_at["Attach-Name"]
                if file_name != None and '.' in file_name:
                    extension = file_name.split(".")[-1]
#                    print("Ext1[%s]"%extension)
            ret = extension_ck(extension)
            if ret == True:
                continue
            elif ret == None:
                extension_list.append(extension)
#            elif ret == False:
#                extension_list.append(False)
        extension_strs = extension_list
#        PRINT("%s : %s" % (is_spam, extension_strs))
        return is_spam, extension_strs

def main():
    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 3. Init attribute parsor & run
    worker = worker_IsExtensionExcutable()
    worker.run(db)
    worker.report_time_consume()

    return

if __name__ == "__main__":
    main()   
