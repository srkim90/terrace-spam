#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

class worker_IsAttachDuplication(workBase):
    def __init__(self):
        super().__init__()
        save_name       = self._get_save_name("attach_duplication.json")
        query_cond      = {"body-items.MD5-Sum"   : {"$exists": True}}
        query_target    = {"body-items.MD5-Sum"   :  True,}
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        md5_list = []
        attachs = one_result["body-items"]
        attachs_tmp = attachs
        is_spam = self._check_is_spam(one_result)

        for item in attachs:
            if 'MD5-Sum' not in item.keys():
                continue
            md5sum = item["MD5-Sum"]
            is_true=0;
            for item_tmp in attachs_tmp:
                if item_tmp == item:
                    if is_true == 1:
#                        print("%s :: %s"%(attachs, attachs_tmp))
                        return is_spam, "True"
                    else:
                       is_true = 1;
                       continue;

#            md5_list.append(md5sum)
        return is_spam, "False"

def main():
    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 3. Init attribute parsor & run
    worker = worker_IsAttachDuplication()
    worker.run(db)
    worker.report_time_consume()

    return

if __name__ == "__main__":
    main()   
