#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from common_test import *
from attribute_mongodb import *

## NOTE: 
# body-items
#    file-name
#    qs-info
#    mail-size
#    header-sequence
#    body-sequence
#    is-spam
#    source_site
#    source_host
#    sip
#    sip-country
# head-items
#    Received
# body-items : []

class worker_IsAttachHeaderAddr(workBase):                                 # TODO : change class name
    def __init__(self):
        super().__init__()
        query_cond      = {"body-items.Attach-Name"   : {"$exists": True},      # TODO : change query condition
                           "head-items.From.uid"      : {"$exists": True}}
        query_target    = {"body-items.Attach-Name"   :  True,                 # TODO : change query target
                           "head-items.From.uid"      :  True,}
        query_func_name =  "queryDefault"
        if self._enroll_query_handling_func(self.__func_work, self.save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        result      = None
        is_spam     = self._check_is_spam(one_result)
        ####################################################
        # TODO: change detail of handling one result and setting work result at 'result'
        attachNames = one_result["body-items"]
        headNames = one_result["head-items"]["From"]
        Uid = headNames["uid"]

        if isinstance(Uid,list) == True:
            Uid = Uid[len(Uid) - 1]
        elif Uid == '':
            return is_spam, False

        bool_list = False
        ret_ck = -1
        for item in attachNames:
            try:
                AttachName = item["Attach-Name"]
            except KeyError:
                continue
            if AttachName == None:
                continue

            ret = AttachName.find(Uid)
            if ret != -1:
                ret_ck = 0
                break

        if ret_ck == 0:
            bool_list = True
            print(bool_list)


        ####################################################
        result = bool_list # TODO : set result
        ####################################################
#        print(result)
        return is_spam, result

if __name__ == "__main__":
    testcase_main()
