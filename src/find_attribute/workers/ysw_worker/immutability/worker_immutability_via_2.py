#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

class worker_via2(workBase):
    def __init__(self):
        super().__init__()
        save_name       = self._get_save_name("immutability_via2.json")
        query_cond      = {"head-items.Received.country"   : {"$exists": True}}
        query_target    = {"head-items.Received.country"   : True,}
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        country_list = []
        #PRINT(one_result)
        headers = one_result["head-items"]["Received"]
        is_spam = self._check_is_spam(one_result)
        for country in headers:
            country = country["country"]
            if country != None:
                country = country.split(";")[0]
        if country == "KR":
            return None, None
        return is_spam, country

def main():
    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 3. Init attribute parsor & run
    worker = worker_via2()
    worker.run(db)
    worker.report_time_consume()

    return

if __name__ == "__main__":
    main()   
