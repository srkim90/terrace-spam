#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from attribute_mongodb import *

mongo_dns       = "terrace-spam"
collection_name = "attribute"

def extension_ck(ext):
    if ext == None:
        return True
    elif len(ext) > 7:
        return True
    elif "xls" == ext:
        return None
    else:
        return True


class worker_ImmutabilityXls(workBase):
    def __init__(self):
        super().__init__()
        save_name       = self._get_save_name("immutability_xls.json")
        query_cond      = {"body-items.Attach-Name" : {"$exists": True}}
        query_target    = {"body-items.Attach-Name" : True,}
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        return

    def __func_work(self, one_result):
        extension_list = []
#        PRINT(one_result)
        attach_list = one_result["body-items"]
        is_spam = self._check_is_spam(one_result)
        for attach_at in attach_list:
            extension = None
            if attach_at and "Attach-Name" in attach_at.keys():
                file_name = attach_at["Attach-Name"]
                if file_name != None and '.' in file_name:
                    extension = file_name.split(".")[-1]
#                    print("Ext1[%s]"%extension)
            ret = extension_ck(extension)
            if ret == True:
                continue
            extension_list.append(extension)
        extension_strs = extension_list
#        PRINT("%s : %s" % (is_spam, extension_strs))
        return is_spam, extension_strs

def main():
    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Init MongoDB
    db = mongoConnection(mongo_dns, collection_name)

    # 3. Init attribute parsor & run
    worker = worker_ImmutabilityXls()
    worker.run(db)
    worker.report_time_consume()

    return

if __name__ == "__main__":
    main()   
