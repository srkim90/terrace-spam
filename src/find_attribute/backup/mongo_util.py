#!/bin/env python
import os
import sys
import gzip
import json 
import pymongo


class mongoConnection():
    def __init__(self, mongo_dns, collection_name, mongo_server="127.0.0.1", mongo_port=27017):
        self.mongo_server    = mongo_server
        self.mongo_port      = mongo_port
        self.mongo_dns       = mongo_dns
        self.collection_name = collection_name
        self.h_collection    = None
 
    def __del__(self):
        if self.h_collection != None:
            #self.h_collection.close()
            #self.h_collection = None
            pass

    def __get_db(self):
        if self.h_collection != None:
            return self.h_collection
        try:
            h_db  = pymongo.MongoClient(self.mongo_server, self.mongo_port)
            h_dns = h_db[self.mongo_dns]
            h_collection = h_dns[self.collection_name]
        except Exception as e:
            print("Error. Fail to load db : %s" % (e,))
            return None
        self.h_collection = h_collection
        return self.h_collection

    def do_query(self, query_cond, query_target, is_print=False):
        if self.h_collection == None:
            if self.__get_db() == None:
                return None
        query_result = self.h_collection.find(query_cond, query_target)
        if is_print == True:
            self.print_result(query_result)
        return query_result


    def print_result(self, query_result):
        for item in query_result:
            print(item)
        pass

