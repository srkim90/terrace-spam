#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import common
from common import *

# import querys
from mongo_util import *
from query_default import *
from query_yyyymmdd import *
from attribute_mongodb import *
from attribute_type_def import *
from combination_typedef import *
from workers import *

__g_config = None

def help():
    print("\n\n================================================================")
    print("python combination_attribute.py -h or --help")
    print("python combination_attribute.py -a or --all")
    print("python combination_attribute.py -c [config_path]")
    print("python combination_attribute.py [attribute_name_1] [attribute_name_2] ...")
    display_attribute_define()
    print("================================================================\n")

def show_querys():
    all_attribute_list = get_all_worker_class()
    print("mongo terrace-spam")
    for attribute in all_attribute_list:
        e = attribute()
        class_name   = attribute.__name__
        description  = get_attribute_description(class_name)
        query_cond   = e.query_cond
        query_target = e.query_target

        query_target["mail-feature.is-spam"] = True

        query_string = "db.%s.find(%s,%s)" % (collection_name, query_cond, query_target)

        query_string = query_string.replace(": True", ": true")
        query_string = query_string.replace("'$exists'", "$exists")

        print("%s" % (query_string,))

def get_global_cfg():
    return __g_config

def get_parameter():
    global __g_config
    input_attribute_names = []
    params = sys.argv[1:]
    
    for idx, item in enumerate(params):
        if item == "-h" or item == "--help":
            help()
            quit()
        elif item == "-q" or item == "--query":
            show_querys()
            quit()
        elif item == "-a" or item == "--all":
            input_attribute_names = g_default_attribute_names
            return input_attribute_names
        elif item == "-c" or item == "--cfg" or item == "--config" or item == "--configure":
            try:
                fConfig = params[idx+1]
            except:
                print("Error. Not input config path")
                quit()
            if os.path.exists(fConfig) == False:
                print("Error. Not exist config in path : %s" % (fConfig,))
                quit()
            __g_config = config.cfg_data.instance(fConfig)
            cfg_attribute_list = __g_config.search_data_cfg("attribute_list","attribute_list")
            if cfg_attribute_list == None:
                input_attribute_names = g_default_attribute_names
            else:
                cfg_attribute_list = cfg_attribute_list.replace("\"", "")
                cfg_attribute_list = cfg_attribute_list.replace("'", "")
                cfg_attribute_list = cfg_attribute_list.replace(",", " ")
                cfg_attribute_list = cfg_attribute_list.replace("\t", " ")
                cfg_attribute_list = cfg_attribute_list.split(" ")
                for attribute in cfg_attribute_list:
                    if attribute not in g_default_attribute_names:
                        add_worker = "worker_%s" % attribute
                        if add_worker not in g_default_attribute_names:
                            continue
                        else:
                            attribute = add_worker
                    input_attribute_names.append(attribute)
            return input_attribute_names 
        else:
            if item in g_default_attribute_names:
                print("attribute : %s" % item)
                input_attribute_names.append(item)

    if len(input_attribute_names) == 0:
        help()
        quit()
    return input_attribute_names




