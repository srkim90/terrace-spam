#!/bin/env pypy3
import os
import sys
import gzip
import json 
import time
import operator

# import common
from common import *

# import querys
from mongo_util import *
from query_default import *
from query_yyyymmdd import *
from attribute_mongodb import *
from attribute_type_def import *
from combination_typedef import *
from combination_option import *

# import worker
from workers import *
from itertools import product, combinations

class worker_Combination(workBase):
    def __init__(self, attribute_list, rule_name, min_accuracy, min_sapm_count):
        super().__init__()
        self.n_spam_count = 0
        self.n_norm_count = 0

        if rule_name == None:
            rule_name = ""
            for idx,attribute in enumerate(attribute_list):
                attribute = attribute.__name__
                attribute = attribute.split("worker_")[-1]
                rule_name += "%s+" % attribute
            rule_name = rule_name[:-1]
        self.min_accuracy   = min_accuracy
        self.min_sapm_count = min_sapm_count
        query_cond          = {}
        query_target        = {}
        self.attribute_list = attribute_list
        self.worker_list    = []

        query_cond_list     = []
        query_target_list   = []

        self.rule_name      = rule_name
        self.log_fd       = self.__get_log_fd()

        save_directory_name = None
        if g_config != None:
            save_directory_name = g_config.search_data_cfg("save_directory_name","path")
        save_name = self._get_save_name("%s.json" % rule_name, save_directory_name)

        for attribute in self.attribute_list:
            worker = attribute()
            query_cond, query_target = worker.get_query()
            query_cond_list.append(query_cond)
            query_target_list.append(query_target)
            self.worker_list.append(worker)
        query_target = {
            "mail-feature.file-name"    : True,
            "head-items.Date"           : True,
            #"head-items.To.domain"      : True,
            "head-items.Received.by"   : True,
            }
        query_target_list.append(query_target)

        self.__add_config_cond(query_cond_list)

        for item in query_cond_list:
            query_cond.update(item)

        for item in query_target_list:
            query_target.update(item)
        query_func_name =  "queryDefault"

        if self._enroll_query_handling_func(self.__func_work, save_name, query_cond, query_target, query_func_name) == False:
            CRT("Error. fail to _enroll_query_handling_func")
            raise
        self.call_count = 0
        return


    def __add_config_cond(self, query_cond_list):
        if g_config == None:
            return
        source_host = g_config.search_data_cfg("source_host","target")
        sip_country = g_config.search_data_cfg("sip_country","target")

        if source_host != None:
            #print("source_host: %s" % (source_host,))
            query_cond_list.append({"mail-feature.source_host" : {"$eq": source_host}})
        if sip_country != None:
            query_cond_list.append({"mail-feature.sip-country" : {"$eq": sip_country}})

        return

    def __get_report_domain(self, received_list):
        report_domain = None
        for item in received_list:
            _for = item["by"]
            if _for == None:
                continue
            if "terracetech.com" in _for:
                continue
            if "." not in _for:
                continue
            report_domain = _for.split("@")[-1]
            break
        return report_domain

    def __get_log_fd(self):
        save_directory_name = None
        if g_config != None:
            enable_log          = g_config.search_data_cfg("enable_log","options")
            save_directory_name = g_config.search_data_cfg("save_directory_name","path")
        else: 
            return None
        if enable_log != "True" and enable_log != "true":
            return None
        save_name = self._get_save_name("%s.log" % self.rule_name, save_directory_name)
        return open(save_name, "w")

    def __logging_result(self, one_result, judge):
        if self.log_fd == None:
            return 
        new_dict = {}
        mail_feature = one_result["mail-feature"]
        file_name    = mail_feature["file-name"]
        is_spam      = mail_feature["is-spam"]
        for item in mail_feature.keys():
            if item == "file-name":
                continue
            elif item == "is-spam":
                continue
            if "mail-feature" not in new_dict.keys():
                new_dict["mail-feature"] = {}
            new_dict["mail-feature"][item] = mail_feature[item]

        if "head-items" in one_result.keys() and one_result["head-items"] != None:
            head_items = one_result["head-items"]
            for item in head_items.keys():
                if item == "Date":
                    continue
                if "head-items" not in new_dict.keys():
                    new_dict["head-items"] = {}
                new_dict["head-items"][item] = head_items[item]

        if "body-items" in one_result.keys() and one_result["body-items"] != None:
            new_dict["body-items"] = one_result["body-items"]

        str_query = json.dumps(new_dict, indent=4, ensure_ascii=False)
        report_string  = ""
        report_string += "%s\n" % (LINE80,)
        report_string += "is-spam   : %s\n" % (is_spam,)
        report_string += "file-name : %s\n" % (file_name,)
        for item in judge:
            report_string += "judge     : %s\n" % (item,)
        report_string += "%s\n" % (line80,)
        report_string += "%s\n" % (str_query,)

        #print(report_string)
        self.log_fd.write("%s\n" % report_string)
        return


    def __func_work(self, one_result):
        tmp_list = []
        result_list = []
        is_spam  = self._check_is_spam(one_result)
        qs_file_name = one_result["mail-feature"]["file-name"]

        report_domain = None
        try:
            for_list = []
            report_domain = self.__get_report_domain(one_result["head-items"]["Received"])
        except (KeyError, TypeError) as e:
            pass
        #print("%s: %s" % (report_domain, qs_file_name))
        try:
            send_time = one_result["head-items"]["Date"]
            send_time = datetime.strptime(send_time, "%Y%m%d_%H%M%S")
        except (KeyError, TypeError) as e:
            send_time = None
        #print(one_result)
        for worker in self.worker_list:
            _is_spam, result_at = worker.work_func(one_result)
            if type(result_at) != list:
                result_at = [result_at,]
            #result_at = list(set(result_at)) # 중복을 제거한다.
            result_new_at = []
            exist_dict = {} 
            for item in result_at: #  중복을 제거한다.
                str_item = "%s" % (item,)
                is_remove_same_result = False
                if g_config != None:
                    is_remove_same_result = g_config.search_data_cfg("remove_same_result_a_file","options")
                if is_remove_same_result != None and is_remove_same_result == "True":
                    if str_item not in exist_dict.keys():
                        exist_dict[str_item] = True
                        result_new_at.append(item)
                else:
                    result_new_at.append(item)
            tmp_list.append(result_new_at)

        self.__logging_result(one_result, tmp_list)
        new_list = list(product(*tmp_list))

        #print(self.worker_list)
        #print(new_list)
        #sleep(1)
        #print(one_result)

        for item in new_list:
            #result_list = "%s" % (item,)
            #print(item)
            item = encode_result(item)
            result_list.append(item)
        mail_info = {
                "qs_name"       : qs_file_name,# + ", idx:%d, ln=%d" % (self.call_count, len(result_list)),
                "send_time"     : send_time,
                "report_domain" : report_domain,    
            }
        #if self.call_count == 27407:
        #    print(new_list)
        self.call_count += 1
        if is_spam == SPAM:
            self.n_spam_count += 1
        else:
            self.n_norm_count += 1
        return is_spam, result_list, mail_info

    def do_query(self, db):
        new_ranked = []
        attribute_list, attribute_ranked = super().do_query(db)
        #mail_info_dict = self.query_obj.get_mail_info_data()
        for item in attribute_ranked:
            rate        = item["rate"]
            spam_count  = item["spam-count"]
            mail_list   = item["mail-list"]
            #print("rate:%s, spam_count:%s, mail_list:%s" % (rate, spam_count, mail_list))
            if self.min_accuracy != None and rate < self.min_accuracy:
                #print("%s %s" % (self.min_accuracy, rate))
                continue
            if self.min_sapm_count != None and spam_count < self.min_sapm_count:
                continue
            new_ranked.append(item)
        print("Total spam_count:%d, norm_count:%d" % (self.n_spam_count, self.n_norm_count))
        return attribute_list, new_ranked

class CombinationAttribute():
    def __init__(self, input_attribute_names = None):
        self.all_attribute_list = get_all_worker_class()
        _mongo_server="127.0.0.1"
        _mongo_port=27017
        _mongo_dns=mongo_dns
        _collection_name=collection_name
        
        if g_config != None:
            _mongo_dns       = g_config.search_data_cfg("mongo_dns","Mongo-DB")
            _collection_name = g_config.search_data_cfg("mongo_collection_name","Mongo-DB")
            _mongo_server    = g_config.search_data_cfg("mongodb_ipaddr","Mongo-DB")
            _mongo_port      = int(g_config.search_data_cfg("mongodb_port","Mongo-DB"))
        self.db = mongoConnection(_mongo_dns, _collection_name, _mongo_server, _mongo_port)
        self.input_attribute_names = []
        for attribute_name in input_attribute_names:
            self.input_attribute_names.append(attribute_name)
        self.combination_rule = self.__combination_rule_define()
        self.known_attribute = {}

    def __combination_rule_define(self):
        attribute_name_all = self.input_attribute_names
        if attribute_name_all == None:
            attribute_name_all = g_default_attribute_names

        all_combination = []
        min_combination_item = 1
        max_combination_item = 3
        if g_config != None:
            min_combination_item   = int(g_config.search_data_cfg("min_combination","attribute_list"))
            max_combination_item   = int(g_config.search_data_cfg("max_combination","attribute_list"))
            #print("min_combination_item:%d, max_combination_item:%d" % (min_combination_item, max_combination_item))

        for idx in range(min_combination_item, max_combination_item+1):
            all_combination += list(combinations(attribute_name_all, idx))

        combination_rule = []
        min_accuracy     = 9.99
        min_sapm_count   = 1
        if g_config != None:
            min_accuracy   = float(g_config.search_data_cfg("spam_rate","threshold"))
            min_sapm_count = int(g_config.search_data_cfg("spam_count","threshold"))

        for idx, combination_at in enumerate(all_combination):
            combination_at = {
                "rule_name"         : None,
                "attribute_names"   : combination_at,
                "min_accuracy"      : min_accuracy,
                "min_sapm_count"    : min_sapm_count,
            }
            combination_rule.append(combination_at)

        return combination_rule
        '''
        combination_rule = [
            {
                "rule_name"         : None,
                "attribute_names"   : [ "worker_AttachExtension",
                                        "worker_Cc_Count",
                                        "worker_toIsFromEqual",
                                        "worker_toIsUndiscloseRecipients",
                                      ],
                "min_accuracy"      : 90.0,
                "min_sapm_count"    : 10,
            }
        ]
        return combination_rule
        '''

    def remove_duplicated_item(self, attribute_ranked):
        # 얻어진 조합식에서 중복을 제거한다.
        # 100%, 25개 이상인 스팸 속성은 저장
        # 조합이 (A,B,C,D,E) 라면, 사전에 해당 조합의 부분집합: (A), (A,B), (B,C), (B,D,E) 가 이미 등록 된 패턴 일 경우 SKIP
        new_ranked = []
        for item in attribute_ranked:
            f_break = False
            all_combined_attribute = item["key"]
            ln_combined = len(all_combined_attribute)
            for idx in range(1, ln_combined):
                combination_items = list(combinations(all_combined_attribute, idx))
                for combination_at in combination_items:
                    str_attrb = encode_result(combination_at)
                    try:
                        self.known_attribute[str_attrb] += 1
                        #print("%s" % (str_attrb,))
                        f_break = True
                    except:
                        pass
                if f_break == True:
                    break
            if f_break == False:
                new_ranked.append(item)
                str_attrb = encode_result(all_combined_attribute)
                self.known_attribute[str_attrb] = 1
                #print("!!%s" % (str_attrb,))
        return new_ranked

    def run(self):
        total = len(self.combination_rule)
        for idx, rule_dict in enumerate(self.combination_rule):
            rule_name       = None
            min_accuracy    = 90.0
            min_sapm_count  = 10
            if "rule_name" in rule_dict.keys():
                rule_name = rule_dict["rule_name"]
            if "min_accuracy" in rule_dict.keys() and rule_dict["min_accuracy"] != None:
                min_accuracy = rule_dict["min_accuracy"]
            if "min_sapm_count" in rule_dict.keys() and rule_dict["min_sapm_count"] != None:
                min_sapm_count  = rule_dict["min_sapm_count"]
            attribute_names = rule_dict["attribute_names"]
            attribute_list  = []
            for attribute in self.all_attribute_list:
                if attribute.__name__ in attribute_names:
                    if attribute not in attribute_list:
                        attribute_list.append(attribute)
            str_rule_name = ""
            for item in attribute_list:
                str_rule_name += "%s%s%s + " % (C_RED, item.__name__[7:], C_END)
            str_rule_name = str_rule_name[:-3]
            print("[%d/%d] rule_name: %s" % (idx+1, total, str_rule_name))
            worker = worker_Combination(attribute_list, rule_name, min_accuracy, min_sapm_count)
            attribute_list, attribute_ranked = worker.do_query(self.db)
            #print(attribute_ranked)

            enable_remove_duplicated_item = g_config.search_data_cfg("enable_remove_duplicated_item","options")
            if enable_remove_duplicated_item != None and enable_remove_duplicated_item.lower() == "true":
                attribute_ranked = self.remove_duplicated_item(attribute_ranked) # 이전 조합식에서 찾은 항목이 있으면 제외 (config 으로 제어 가능 : )
            #for item in attribute_ranked:
            #    print(item)
            #print(attribute_list)
            enable_site_name = False
            enable_mail_list = True
            if g_config != None:
                enable_site_name = g_config.search_data_cfg("enable_site_name","options")
                enable_mail_list = g_config.search_data_cfg("enable_mail_list","options")
            for item in attribute_ranked:
                if enable_site_name != "True" and enable_site_name != "true" and "report-domain" in item.keys():
                    del item["report-domain"]
                if enable_mail_list != "True" and enable_mail_list != "true" and "mail-list" in item.keys():
                    del item["mail-list"]
                
            worker.save_result(None, attribute_ranked)

g_config = None

def main():
    global g_config

    # 1. Init LOG
    common_init_log(base_dir, "attribute_parsor", INFO)

    # 2. Input parameter parsor
    input_attribute_names = get_parameter()
    g_config = get_global_cfg()

    # 3. Init attribute parsor & run
    combination = CombinationAttribute(input_attribute_names)
    combination.run()
    return

if __name__ == "__main__":
    main()



