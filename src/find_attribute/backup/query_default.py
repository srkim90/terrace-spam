#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import 
from common import *
from mongo_util import *
from query_base import *
from attribute_type_def import *

class queryDefault(queryBase):
    def __init__(self):
        super().__init__()
  
    def get_mail_info_data(self):
        #for key in self.mail_info_dict.keys():
        #    info_same_attrb = self.mail_info_dict[key]
        #    for item in info_same_attrb:
        #        print("%s : %s" % (key, item))
        return self.mail_info_dict

    def callback_func(self, db, query_cond, query_target, parse_func):
        attribute_list   = self.__query_func(db, query_cond, query_target, parse_func)
        attribute_ranked = self.__attribute_rank(attribute_list)

        return attribute_list, attribute_ranked

    def __query_func(self, db, query_cond, query_target, parse_func):
        self.attribute_count = 0
        total_count = 0
        if "mail-feature.is-spam" not in query_target.keys():
            query_target["mail-feature.is-spam"] = True
        self.start_time   = time.time()
        #print(query_cond)
        #print(query_target)
        query_result = db.do_query(query_cond, query_target)
        attribute_dict = {SPAM : {}, NORMAL: {}}
        for idx, one_result in enumerate(query_result):
            #is_spam, matrix = parse_func(one_result)
            #print(one_result)
            result_list = parse_func(one_result)
            is_spam = result_list[0]
            matrix  = result_list[1]
            mail_info = None
            #print("%s %s %s" % (is_spam, matrix, mail_info))
            if len(result_list) > 2:
                mail_info = result_list[2]
            if is_spam == None or matrix == None:
                continue
            if type(matrix) == tuple or type(matrix) == dict:
                matrix = "%s" % (matrix,)
            if type(matrix) == str or type(matrix) == int or type(matrix) == bool:
                matrix = [matrix,]
            for item in matrix:
                try:
                    attribute_dict[is_spam][item] += 1
                except:
                    self.attribute_count += 1
                    attribute_dict[is_spam][item] = 1
                if mail_info != None:
                    qs_name   = mail_info["qs_name"]
                    send_time = mail_info["send_time"]
                    report_domain = mail_info["report_domain"]
                    try:
                        self.mail_info_dict[item]["qs_name"].append(qs_name)
                        self.mail_info_dict[item]["report_domain"].append(report_domain)
                        last_occurred  = self.mail_info_dict[item]["last_occurred"]
                        first_occurred = self.mail_info_dict[item]["first_occurred"]
                        if send_time < first_occurred:
                            self.mail_info_dict[item]["first_occurred"] = send_time
                        if send_time > last_occurred:
                            self.mail_info_dict[item]["last_occurred"] = send_time
                    except:
                        new_dict = {
                            "qs_name"        : [qs_name,],
                            "last_occurred"  : send_time,
                            "first_occurred" : send_time,
                            "report_domain"  : [report_domain,]
                        }
                        self.mail_info_dict[item] = new_dict
        #print("idx=%d, total_count=%d" % (idx, total_count))
        
        self.query_time = time.time()
        self.parse_time = time.time()
        attribute_list  = self._result_reshape_to_sorted_list(attribute_dict)
        self.sort_time  = time.time()
        #aaa = self.get_mail_info_data()
        return attribute_list
    
    def __attribute_rank(self, attribute_list, min_count=10, min_rate=80.0):
        attribute_dict = self._result_reshape_to_dict(attribute_list)
        attribute_ranked  = {}
        spam_dict = attribute_dict[SPAM]
        norm_dict = attribute_dict[NORMAL]

        for key in spam_dict.keys():
            spam_count = spam_dict[key]
            try:
                norm_count = norm_dict[key]
            except:
                norm_count = 0
            #if spam_count + norm_count < min_count:
            #    continue
            rate = (float(spam_count) / (float(spam_count) + float(norm_count))) * 100.0
            #if rate < min_rate:
            #    continue
            #print("key : %s, norm_count : %d, spam_count : %d" % (key, norm_count, spam_count))
            try:
                mail_list = self.mail_info_dict[key]
            except KeyError as e:
                mail_list = None
            attribute_ranked[key] = {
                "norm-count"    : norm_count,
                "spam-count"    : spam_count,
                "mail-list"     : mail_list,
                "rate"          : rate,
            }
        #print(spam_dict)
        #print(norm_dict)
        attribute_ranked = self._result_reshape_to_sorted_rank(attribute_ranked)
        return attribute_ranked





