#!/bin/env python
import os
import sys
import gzip
import json 
import time
import operator

# import common
from common import *

g_default_attribute_names = []

g_default_attribute_define = [
        ["worker_ArchiveInnerFilesName",            "압축파일 내부 파일 이름",                                          None,  str,],
        ["worker_AttachExtension",                  "첨부 확장자",                                                      None,  str,],
        ["worker_AttachMD5",                        "첨부 MD5",                                                         None,  str,],
        ["worker_BodyUrlDomain",                    "본문 URL",                                                         None,  str,],
        ["worker_Cc_Count",                         "CC 메일계정 개수",                                                 None,  int,],
        ["worker_receivedViaCountryList",           "경유 국가",                                                        None,  list,],
        ["worker_subjectWordCount",                 "제목에서 언어별 단어 포함 여부",                                   None,  bool,],
        ["worker_To_Count",                         "To 메일 계정 개수",                                                None,  int,],
        ["worker_toIsFromEqual",                    "To From 같은지 여부",                                              None,  bool,],
        ["worker_toIsUndiscloseRecipients",         "To 에 UndiscloseRecipients 여부",                                  None,  bool,],
        ["worker_IsBodyUrlSapmCheck",               "Body에 포함된 URL의 Spam 여부",                                    None,  bool,],
#       ["worker_ForeignCountry",	                "외국에서 발신 되었다", ],
#       ["worker_via2",	                        "서로 다른 2개 이상의 경유 국가가 있다.", ],

        # srkim_worker
        ["worker_CountBodyHaveUrl",	                "[불변] Body URL 개수",                                             "BODY-URL"                              , int  ,],
        ["worker_CountHaveArchive",	                "[불변] 첨부에 압축파일 개수",                                      "첨부-압축"                             , int  ,],
        ["worker_isArchiveInnerDoubleExecutable",	"[불변] 압축 내부 파일에 2중확장자포함 여부",                       "압축내부 2중확장자"                    , bool ,],
        ["worker_IsAttachDoubleExtension",	        "[불변] 첨부에 2중확장자포함 여부",                                 "첨부 2중확장자"                        , bool ,],
        ["worker_IsAttachmentBlankExtension",	    "[불변] 확장자에 공백 포함 여부",                                   "확장자 공백"                           , bool ,],
        ["worker_IsAttachmentExist",	            "[불변] 첨부 있음?",                                                "첨부"                                  , bool ,],
        ["worker_IsAttachmentHaveArchive",	        "[불변] 첨부에 압축 있는지 여부",                                   "압축 첨부"                             , bool ,],
        ["worker_IsAttachmentHaveDocument",	        "[불변] 첨부에 문서 여부",                                          "문서 첨부"                             , bool ,],
        ["worker_IsAttachmentHaveImage",	        "[불변] 첨부에 이미지 여부",                                        "이미지 첨부"                           , bool ,],
        ["worker_IsAttachmentHaveOleDocument",	    "[불변] 첨부에 OLE Format 문서 여부",                               "OLE 문서"                              , bool ,],
        ["worker_IsAttachmentHaveUnknownType",	    "[불변] 첨부에 문서/이미지/압축/실행 아닌 파일 존재 여부",          "문서/이미지/압축/실행 아닌 첨부 존재"  , bool ,],
        ["worker_IsAttachmentNameHaveEmailAddrs",	"[불변] 첨부이름에 메일 주소 포함 여부",                            "첨부이름 메일주소 포함"                , bool ,], 
        ["worker_IsAttachmentNameHaveEnglish",	    "[불변] 첨부파일 이름에 영어 포함 여부",                            "첨부이름 영어 포함"                    , bool ,],
        ["worker_IsAttachmentNameHaveHangule",	    "[불변] 첨부파일 이름에 한글 포함 여부",                            "첨부이름 한글 포함"                    , bool ,],
        ["worker_IsBodyHaveUrl",	                "[불변] 본문에 URL 있는지 여부",                                    "본문에 URL 존재"                       , bool ,],
        ["worker_IsNotExistSubject",	            "[불변] 제목 헤더 없음",                                            "Subject 헤더 없음"                     , bool ,],
        ["worker_IsNullSubject",	                "[불변] 제목 헤더에 널값",                                          "Subject 헤더에 널값"                   , bool ,],
        ["worker_IsNoVisitedCountry",	            "[불변] 경유국 없음",                                               "경유국 없음"                           , bool ,],
        ["worker_IsOnlyHaveTextHtml",	            "[불변] 본문이 text/html 만 있음",                                  "본문 text/html 만 존재"                , bool ,],
        ["worker_IsOnlyHaveTextPlain",	            "[불변] 본문이 text/plain 만 있음",                                 "본문 text/plain 만 존재"               , bool ,],
        ["worker_IsOnlyOneImageHave",	            "[불변] 이미지 하나 있고 내용 적음",                                "이미지 하나 있고 내용 적음"            , bool ,],
        ["worker_IsVisited3thCountry",	            "[불변] 한/중/일/미 이외의 국가 거침",                              "한/중/일/미 이외의 국가 경유"          , bool ,],
        ["worker_ListVisitedCountry",	            "[불변] 경유국 목록",                                               "경유국"                                , list ,],
        ["worker_IsExistVbaSplit",                  "[불변] 스크립트 포함 여부",                                        "스크립트 포함"                         , bool ,],
        ["worker_IsExistAttachment",                "[불변] 첨부 포함 여부",                                            "첨부 포함"                             , bool ,],
        ["worker_OleType",                          "[불변] OLE 확장자별",                                              "OLE 확장자"                            , list ,],

        ["worker_IsExtensionExcutable",             "[불변] 첨부파일에 실행 파일 존재 여부",                            "첨부파일에 실행 파일 존재"             , bool ,],
        ["worker_IsInnerExcutable",                 "[불변] 압축 내부 파일에 실행 파일 존재 여부",                      "압축 내부 파일에 실행 파일 존재"       , bool ,],
        ["worker_IsOleType",                        "[불변] xls, doc, ppt 등 OLE Type 문서파일 존재 여부",              "OLE Type 문서파일 존재"                , bool ,],
        ["worker_IsLenExcess",                      "[불변] 확장자 길이기가 7자 이상 일 경우",                          "확장자 길이기가 7자 이상"              , bool ,],
        ["worker_IsAttachHeaderAddr",               "[불변] 헤더에 나온 주소가 첨부파일명에 존재",                      "헤더에 나온 주소가 첨부파일명에 존재"  , bool ,],
        ["worker_IsAttachDuplication",              "[불변] 동일한 첨부파일이 존재",                                    "동일한 첨부파일이 존재"                , bool ,],
        ["worker_IsExtensionIndividual",            "[불변] 확장자 별 통계 ",                                           "확장자 별 통계 "                       , bool ,],

        
    ]

def get_attribute_short_description(class_name):
    if type(class_name) != str:
        class_name = class_name.__name__
    class_name = class_name.replace("worker_", "").lower()
    for item in g_default_attribute_define:
        description = item[1]
        short_description = item[2]
        result_type = item[3]
        if short_description == None:
            short_description = description
        else:
            if result_type == bool:
                short_description += " 여부"
            elif result_type == int:
                short_description += " 개수"
            elif result_type == str:
                short_description += ""
            elif result_type == list:
                short_description += " 목록"
        if class_name in item[0].lower():
            #print("class_name : %s, %s" % (class_name, item[0].lower()))
            #print(short_description)
            return short_description
    return None

def get_attribute_description(class_name):
    if type(class_name) != str:
        class_name = class_name.__name__
    for item in g_default_attribute_define:
        description = item[1]
        if class_name == item[0]:
            return description
    return None

def display_attribute_define():
    print("  --- attribute_name info ---  ")
    for item in g_default_attribute_define:
        class_name = item[0]
        description = item[1]
        print("%s%-45s : %s" % (" "*4, class_name, description))

for item in g_default_attribute_define:
    g_default_attribute_names.append(item[0])

