# -*- coding: utf-8 -*-

'''
  Author   : Kim, Seongrae
  Filename : util.py
  Release  : 1
  Date     : 2018-08-28
 
  Description : utility for python
 
  Notes :
  ===================
  History
  ===================
  2018/08/28  created by Kim, Seongrae
'''
import os
import re
import sys
import pytz
import gzip
import json
import timeit
import datetime
#from log import *
from time import sleep
from ctypes import *
import base64
#import _pickle
import pickle

import socket
import struct

try:
    import psutil
except ImportError as e:
    pass

C_END     = "\033[0m"
C_BOLD    = "\033[1m"
C_INVERSE = "\033[7m"
C_ITALIC  = "\033[3m"
C_UNDERLN = "\033[4m"
 
C_BLACK  = "\033[30m"
C_RED    = "\033[31m"
C_GREEN  = "\033[32m"
C_YELLOW = "\033[33m"
C_BLUE   = "\033[34m"
C_PURPLE = "\033[35m"
C_CYAN   = "\033[36m"
C_WHITE  = "\033[37m"
 
g_local_addrs = None
def echo_print(dummy_str):
    print("[%s]" % dummy_str)

def get_old_time(n_day):
    now = datetime.datetime.now(tz=pytz.timezone("Asia/Seoul"))
    old = now - datetime.timedelta(n_day)
    #print("%s-%02d-%02d %02d:%02d:%02d" % (old.year, old.month, old.day, old.hour, old.minute, old.second ))
    str_day = "%s-%02d-%02d" % (old.year, old.month, old.day)
    return old#str_day, old.weekday()

def get_yyyymmdd_time(yyyy, mm, dd):
    #print("%s %s %s" % (yyyy, mm, dd))
    return datetime.datetime(int(yyyy), int(mm), int(dd), 12, 0, 0, 0)

def memory_unit(n_size):
    if n_size < 1024:
        return "%-4d B" % (n_size)
    if n_size < 1024 * 1024:
        n_unit_byte = (float(n_size) / 1024.0)
        if n_unit_byte > 10.0:
            n_unit_byte = int(n_unit_byte)
            return "%-4d %s%sKB%s" % (n_unit_byte, C_BOLD,C_RED,C_END)
        return "%0.2f %s%sKB%s" % (n_unit_byte, C_BOLD,C_RED,C_END)
    if n_size < 1024 * 1024 * 1024:
        n_unit_byte = (float(n_size) / (1024.0 * 1024.0))
        if n_unit_byte > 10.0:
            n_unit_byte = int(n_unit_byte)
            return "%-4d %s%sMB%s" % (n_unit_byte, C_BOLD,C_YELLOW,C_END)
        return "%0.2f %s%sMB%s" % (n_unit_byte, C_BOLD,C_YELLOW,C_END)
    n_unit_byte = (float(n_size) / (1024.0 * 1024.0 * 1024.0))
    if n_unit_byte > 10.0:
        n_unit_byte = int(n_unit_byte)
        return "-4%d %s%sGB%s" % (n_unit_byte, C_BOLD,C_GREEN,C_END)
    return "0.2%f %s%sGB%s" % (n_unit_byte, C_BOLD,C_GREEN,C_END)

def get_now_time(format_string="%Y-%m-%d", yyyymmdd=None):
    if yyyymmdd != None:
        dd   = yyyymmdd[6:8]
        mm   = yyyymmdd[4:6]
        yyyy = yyyymmdd[0:4]
        now  = get_yyyymmdd_time(yyyy, mm, dd)
    else:
        now = datetime.datetime.now(tz=pytz.timezone("Asia/Seoul"))
    #old = now - datetime.timedelta(n_day)
    #print("%s-%02d-%02d %02d:%02d:%02d" % (old.year, old.month, old.day, old.hour, old.minute, old.second, old.milliseconds ))
    #yyyy:mm:dd:hh:MM:ss
    
    #  get_now_time("%Y-%m-%d %H:%M:%S %f")

    '''
    %Y  Year                        2013, 2014,     ... , 9999
    %y  Year                        17, 18,         ... , 99  
    %m  Month                       01, 02,         ... , 12
    %d  Day                         01, 02,         ... , 31
    %H  Hour (24-hour clock)        00, 01,         ... , 23
    %M  Minute                      00, 01,         ... , 59
    %S  Second                      00, 01,         ... , 59
    %f  Microsecond                 000000, 000001, ... , 999999
    '''

    #if format_string == "yyyy:mm:dd":
    #    str_day = "%s-%02d-%02d" % (old.year, old.month, old.day)
    return now.strftime(format_string)

#def get_old_time_list()

def str_time_to_int(str_time):
    #2019-02-07 09:17:00
    p = re.compile("20[0-9][0-9]-[0-1][0-9]-[0-3][0-9] [0-9][0-9]:[0-9][0-9]:[0-9][0-9]")
    if p.match(str_time) != None:
        str_tok = str_time.split(" ")[1].split(":")
        hh = int(str_tok[0])
        mm = int(str_tok[1])
        ss = int(str_tok[2])
        return hh * 10000 + mm * 100 + ss
        #return hh * 3600 + mm * 60 + ss

    #2019-02-07_09:17:00
    p = re.compile("20[0-9][0-9]-[0-1][0-9]-[0-3][0-9]_[0-9][0-9]:[0-9][0-9]:[0-9][0-9]")
    if p.match(str_time) != None:
        str_tok = str_time.split(" ")[1].split(":")
        hh = int(str_tok[0])
        mm = int(str_tok[1])
        ss = int(str_tok[2])
        return hh * 10000 + mm * 100 + ss
        #return hh * 3600 + mm * 60 + ss

    #09:17:00
    p = re.compile("[0-9][0-9]:[0-9][0-9]:[0-9][0-9]")
    if p.match(str_time) != None:
        str_tok = str_time.split(":")
        hh = int(str_tok[0])
        mm = int(str_tok[1])
        ss = int(str_tok[2])
        #return hh * 3600 + mm * 60 + ss
        # 120000 + 001100 + 000011
        return hh * 10000 + mm * 100 + ss

    #09:17
    p = re.compile("[0-9][0-9]:[0-9][0-9]")
    if p.match(str_time) != None:
        str_tok = str_time.split(":")
        hh = int(str_tok[0])
        mm = int(str_tok[1])
        return hh * 10000 + mm * 100
        #return hh * 3600 + mm * 60

    #09.17.00
    p = re.compile("[0-9][0-9].[0-9][0-9].[0-9][0-9]")
    if p.match(str_time) != None:
        str_tok = str_time.split(".")
        hh = int(str_tok[0])
        mm = int(str_tok[1])
        ss = int(str_tok[2])
        #return hh * 3600 + mm * 60 + ss
        # 120000 + 001100 + 000011
        return hh * 10000 + mm * 100 + ss



    return None


PYTHON_TYPE_CPYTHON = 0
PYTHON_TYPE_PYPY    = 1
g_python_type       =-1

def get_python_runtime():

    global g_python_type
    if g_python_type != -1:
        return g_python_type

    from subprocess import Popen, PIPE
    for line in Popen(['ps', 'aux'], shell=False, stdout=PIPE).stdout:
        if line.find("%s" % os.getpid()) == -1:
            continue
        line = line.split(' ')
        for item in line:
            if item.find('pypy') != -1:
                g_python_type = PYTHON_TYPE_PYPY
                return g_python_type
            elif item.find('python') != -1:
                g_python_type = PYTHON_TYPE_CPYTHON
                return g_python_type
            else:
                continue

    return g_python_type

g_timeit = {}
def SET_TIMEIT(l_timeit=None, description=""):
    if l_timeit == None:
        l_timeit = {}
    
    now_idx    = len(l_timeit.keys())
    timeit_key = "%02d %s" % ( now_idx, get_now_time("%H:%M:%S_%f")) + description
    l_timeit[timeit_key] = timeit.default_timer()

    global g_timeit
    g_timeit = l_timeit

    return l_timeit

def PRINT_TIMEIT(l_timeit=None):
    global g_timeit    
    if l_timeit == None:
        l_timeit = g_timeit

    item_list  = sorted(l_timeit.keys())
    old_timeit = None
    for idx, key in enumerate(item_list):
        prev_timeit = l_timeit[key]
        next_timeit = l_timeit[item_list[idx+1]] if idx+1 < len(item_list) else timeit.default_timer()
        print("%s : %s" % (key, next_timeit - prev_timeit))

    g_timeit = {}

OS_TYPE_LINUX   = 1
OS_TYPE_WINDOW  = 2
OS_TYPE_OTHERS  = 3

def get_os_type():
    if os.name.find("nt") != -1 or os.name.find("indow") != -1 or os.name.find("NT") != -1 or os.name.find("INDOW") != -1:
        return OS_TYPE_WINDOW
    else:
        return OS_TYPE_LINUX

def getConfigPath(stock_home=None, dir_delimiter=None):
    if "CONFIG_HOME" in os.environ:
        return os.environ["CONFIG_HOME"]
    else:
        if dir_delimiter == None:
            dir_delimiter = DIR_DELIMITER
        if stock_home == None:
            stock_home == STOCK_HOME
        if get_os_type() == OS_TYPE_WINDOW:
            return stock_home + "%ssrc%scfg" % (dir_delimiter, dir_delimiter)
        else: # LINUX
            return stock_home + "%scfg" % (dir_delimiter)

def do_demonize():
    pid = os.fork()
    if pid != 0:
        sleep(1)
        quit()
    iam_deamon = True
    return iam_deamon


def calc_rate(base_price, now_price):
    if base_price == 0 or base_price == 0.0:
        return 0.0
    return float((float(now_price) / float(base_price)) * 100.0) - 100.0

def get_directory_path(dir_name=None):
    try:
        base_dir = os.environ['MAIL_DATA_HOME']
    except:
        print("Not exist environment : 'MAIL_DATA_HOME'")
        return None
    if dir_name != None:
        base_dir += "/%s" % (dir_name)
    return base_dir

def open_json_file(json_path):
    if os.path.exists(json_path) == False:
        print("Error. Not exist file: %s" % (json_path,))
        return None
    if ".gz" == json_path[-3:].lower():
        fd = gzip.open(json_path, "rb")
    else:
        fd = open(json_path, "rb")
    json_data = fd.read()
    fd.close()
    try:
        json_odj = json.loads(json_data)
    except Exception as e:
        return None
    return json_odj

def save_json_file(json_path, obj_dict):
    try:
        string_json = json.dumps(obj_dict, indent=4, ensure_ascii=False).encode('utf-8')
    except Exception as e:
        return None
    if ".gz" == json_path[-3:].lower():
        fd = gzip.open(json_path, "wb")
    else:
        fd = open(json_path, "wb")
    fd.write(string_json)
    fd.close()
    return True

def get_env_value(env_name):
    if env_name == "N_WORK_PROC":
        try:
            value = int(os.environ[env_name])
        except:
            value = 2
    elif env_name == "IS_RESULT_GZ":
        try:
            value = int(os.environ[env_name])
            if value.lower() == "true" or value.lower() == "1":
                value = True
            else:
                value = False
        except:
            value = False
    else:
        return None
    return value

def funcname():
    return sys._getframe(1).f_code.co_name + "()"

def callname():
    return sys._getframe(2).f_code.co_name + "()"

g_libc = None
def gettid():
    global g_libc
    if g_libc == None:
        g_libc = CDLL("libc.so.6")
        #g_libc.syscall(186).restype = c_uint
    return g_libc.syscall(186)


def is_ipaddr_is_private(ipaddr):
    # 2. check private addrs
    try:
        f = struct.unpack('!I',socket.inet_pton(socket.AF_INET,ipaddr))[0]
    except OSError as e:
        return False
    private = (
        [ 2130706432, 4278190080 ], # 127.0.0.0,   255.0.0.0   
        [ 3232235520, 4294901760 ], # 192.168.0.0, 255.255.0.0 
        [ 2886729728, 4293918720 ], # 172.16.0.0,  255.240.0.0 
        [ 167772160,  4278190080 ], # 10.0.0.0,    255.0.0.0   
    ) 
    for net in private:
        if (f & net[1]) == net[0]:
            return True
    return False

def is_ipaddr_is_internal(ipaddr):
    def get_ip_addresses():
        family = socket.AF_INET
        for interface, snics in psutil.net_if_addrs().items():
            for snic in snics:
                if snic.family == family:
                    yield snic.address
    global g_local_addrs

    if ipaddr == None or len(ipaddr) > 15 or len(ipaddr) < 7:
        return True

    if g_local_addrs == None:
        g_local_addrs = get_ip_addresses()
    # 1. check local addrs
    if ipaddr in g_local_addrs:
        return True

    return is_ipaddr_is_private(ipaddr)

def is_same_network_range(ipaddr_A, prefix_A, ipaddr_B):
    try:
        int_addr_A = struct.unpack('!I',socket.inet_pton(socket.AF_INET,ipaddr_A))[0]
        int_addr_B = struct.unpack('!I',socket.inet_pton(socket.AF_INET,ipaddr_B))[0]
    except OSError as e:
        return False
    if prefix_A>= 32:
        if ipaddr_A == ipaddr_B:
            return True
        else:
            return False
    bit_mask_A = ((2 << prefix_A - 1) - 1) << (32 - prefix_A)
    int_net_A  = int_addr_A & bit_mask_A
    int_net_B  = int_addr_B & bit_mask_A
    str_start_A = socket.inet_ntoa(struct.pack("!I", int_net_A))
    str_end_A   = socket.inet_ntoa(struct.pack("!I", int_net_A + ( 2 << (32 - prefix_A - 1))))
    #print("prefix: %s, int_addr_A:%s, int_addr_B:%s, subnet: %s ~ %s" % (prefix_A, int_net_A, int_addr_B, str_start_A, str_end_A))
    if int_net_A == int_net_B:
        return True
    return False


def encode_result(result_data):
    pickle_data = pickle.dumps(result_data)
    base64_data = base64.b64encode(pickle_data)
    base64_str  = "base64_encoded:" + base64_data.decode("UTF-8")
    return base64_str

def decode_result(b64_data):
    if type(b64_data) != bytes and type(b64_data) != str:
        return b64_data
    if len(b64_data) <= len("base64_encoded:"):
        return b64_data
    if b64_data[:len("base64_encoded:")] != "base64_encoded:" and b64_data[:len("base64_encoded:")] != b"base64_encoded:":
        return b64_data 
    if type(b64_data) == str:
        b64_data = b64_data.encode("UTF-8")
    b64_data    = b64_data[len("base64_encoded:"):]
    pickle_data = base64.b64decode(b64_data)
    result_data = pickle.loads(pickle_data)
    return result_data




