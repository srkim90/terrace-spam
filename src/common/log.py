# -*- coding: utf-8 -*-

'''
  Author   : Kim, Seongrae
  Filename : log_ex.py
  Release  : 1
  Date     : 2020-04-14
 
  Description : sa_log module for python
 
  Notes :
  ===================
  History
  ===================
  2020/04/14  created by Kim, Seongrae
'''

import os
import sys
import util
import json
import code
import signal
import traceback
import logging as log

from datetime import datetime

DEBUG       = log.DEBUG
INFO        = log.INFO
WARNING     = log.WARNING
ERROR       = log.ERROR
CRITICAL    = log.CRITICAL

LOG_CRT     = log.CRITICAL
LOG_MAJ     = log.ERROR
LOG_ERR     = log.ERROR
LOG_MIN     = log.WARNING
LOG_WAR     = log.WARNING
LOG_INF     = log.INFO
LOG_DBG     = log.DEBUG

# log.debug('debug')
# log.info('info')
# log.warning('warning')
# log.error('error')
# log.critical('critical')

C_END     = "\033[0m"
C_BOLD    = "\033[1m"
C_INVERSE = "\033[7m"
C_ITALIC  = "\033[3m"
C_UNDERLN = "\033[4m"
 
C_BLACK  = "\033[30m"
C_RED    = "\033[31m"
C_GREEN  = "\033[32m"
C_YELLOW = "\033[33m"
C_BLUE   = "\033[34m"
C_PURPLE = "\033[35m"
C_CYAN   = "\033[36m"
C_WHITE  = "\033[37m"
 
C_BGBLACK  = "\033[40m"
C_BGRED    = "\033[41m"
C_BGGREEN  = "\033[42m"
C_BGYELLOW = "\033[43m"
C_BGBLUE   = "\033[44m"
C_BGPURPLE = "\033[45m"
C_BGCYAN   = "\033[46m"
C_BGWHITE  = "\033[47m"

line80="--------------------------------------------------------------------------------"
line90="------------------------------------------------------------------------------------------"
line100="----------------------------------------------------------------------------------------------------"
line110="--------------------------------------------------------------------------------------------------------------"
line120="------------------------------------------------------------------------------------------------------------------------"

LINE80="================================================================================"
LINE90="=========================================================================================="
LINE100="===================================================================================================="
LINE110="=============================================================================================================="
LINE120="========================================================================================================================"


class common_log():
    def __init__(self, base_path, log_name=None, log_level=log.INFO):
        self.log_name  = log_name
        self.base_path = base_path
        self.log_level = log_level
        self.use_terminal = True
        self.debuginfo_dict = {}
        
        if base_path == None:
            print("Print log at TERMINAL")
            return

        self.logfile_name = self.__make_logfile_name()
        if self.logfile_name == None:
            return
        log.basicConfig(filename=self.logfile_name, level=log_level)

        self.use_terminal = False
        return

    def set_debuginfo(self, debuginfo):
        if type(debuginfo) == dict:
            debuginfo = json.dumps(debuginfo, indent=4, ensure_ascii=False)
        else:
            pass
        tid = util.gettid()
        self.debuginfo_dict[tid] = debuginfo

    def del_debuginfo(self):
        tid = util.gettid()
        if tid in self.debuginfo_dict:
            del self.debuginfo_dict[tid]

    def get_debuginfo(self):
        tid = util.gettid()
        if tid in self.debuginfo_dict:
            return self.debuginfo_dict[tid]
        return None

    def __make_logfile_name(self):
        if os.path.exists(self.base_path) == False:
            return None

        save_dir   = self.base_path
        check_list = ["log", self.log_name]

        for item in check_list:
            if item == None:
                continue
            save_dir += "/%s" % (item,)
            if os.path.exists(save_dir) == False:
                try:
                    os.mkdir(save_dir)
                except Exception as e:
                    print(e)
                    return None
        yyyymmdd = datetime.today().strftime("%Y%m%d")
        logfile_name = "%s/%s.log" % (save_dir, yyyymmdd)
        return logfile_name

    def __make_prefix(self, log_level):
        hhmmss = datetime.today().strftime("%H:%M:%S")
        pid = os.getpid()
        tid = util.gettid()
        str_loglevel = "%s" % (log_level,)
        return "[%s %s.%s] |" % (hhmmss, pid, tid)

    def logging(self, log_level, log_string, is_print_debuginfo=False):
        prefix = self.__make_prefix(log_level)
        debuginfo = self.get_debuginfo()
        log_string = "%s %s" % (prefix, log_string)
        if is_print_debuginfo == True and debuginfo != None:
            log_string = "\n%s\n%s" % (debuginfo, log_string)
        if self.use_terminal == True:
            print(log_string)
            return
        if log_level == log.DEBUG:
            return log.debug(log_string)
        elif log_level == log.INFO:
            return log.info(log_string)
        elif log_level == log.WARNING:
            return log.warning(log_string)
        elif log_level == log.ERROR:
            return log.error(log_string)
        else:
            return log.critical(log_string)

    def print_init_log(self):
        CRT("START Process")

g_log_module = None
def common_init_log(base_path, log_name=None, log_level=log.INFO):
    global g_log_module
    if g_log_module != None:
        return
    g_log_module = common_log(base_path, log_name, log_level)
    g_log_module.print_init_log()
    #print("LOG initialize complete")
    signal.signal(signal.SIGUSR1, print_call_stack_as_signal)

def LOG(log_level, log_string, exception=None):
    global g_log_module
    is_print_debuginfo = False
    #print("%s : %s" % (g_log_module, log_string))
    if g_log_module == None:
        print(log_string)
        return False
    if exception != None:
        is_print_debuginfo = True
        str_traceback = traceback.format_exc()
        log_string = "[!!!!!!!!traceback!!!!!!!]\n%s\n%s\n%s\n%s\n%s\n\n" % (LINE80, str_traceback, line80, log_string, LINE80)
    g_log_module.logging(log_level, log_string, is_print_debuginfo)
    return True

def PRINT(log_string):
    if LOG(log.CRITICAL, log_string) == True:
        print(log_string)

def CRT(log_string, exception=None):
    LOG(log.CRITICAL, log_string, exception)

def ERR(log_string, exception=None):
    LOG(log.ERROR, log_string, exception)

def WAR(log_string, exception=None):
    LOG(log.WARNING, log_string, exception)

def INF(log_string, exception=None):
    LOG(log.INFO, log_string, exception)

def DBG(log_string, exception=None):
    LOG(log.DEBUG, log_string, exception)

def SET_DEBUGINFO(dbg_info):
    if g_log_module != None:
        g_log_module.set_debuginfo(dbg_info)

def print_call_stack_as_signal(sig, frame):
    """Interrupt running process, and provide a python prompt for
    interactive debugging."""
    d={'_frame':frame}         # Allow access to frame object.
    d.update(frame.f_globals)  # Unless shadowed by global
    d.update(frame.f_locals)

    i = code.InteractiveConsole(d)
    message  = "Signal received : entering python shell.\nTraceback:\n"
    message += ''.join(traceback.format_stack(frame))
    i.interact(message)

def STACK():
    for line in traceback.format_stack():
        print(line.strip())


def sa_initlog(log_name, log_index, log_path=None, log_level=-1, log_max_size=-1, log_max_num_a_day=-1, is_visible_log=True):
    return common_init_log(log_path, log_name)

def main():
    base_path   = "/srkim/mnt/hdd250G/maildata"
    log_name    = None
    log_level   = log.INFO
    common_init_log(base_path, log_name, log_level)

if __name__ == '__main__':
    main()
