# -*- coding: utf-8 -*-

'''
  Author   : Kim, Seongrae
  Filename : py3_util.py
  Release  : 1
  Date     : 2018-09-11
 
  Description : utility for python3
 
  Notes :
  ===================
  History
  ===================
  2018/09/11  created by Kim, Seongrae
'''
import os
import struct
import hashlib

def mmc_print(string, end=None, flush=False):
    __mmc_print=print
    __mmc_print(string, end=end, flush=flush)


def get_mail_size(email_path):
    if email_path[-3:].lower() == ".gz":
        return getuncompressedsize(email_path)
    else:
        return os.path.getsize(email_path)

def getuncompressedsize(filename):
    with open(filename, 'rb') as f:
        f.seek(-4, 2)
        return int(struct.unpack('I', f.read(4))[0])

def remove_non_ascii_char(org_string):
    bi_string = bytes(org_string)
    bi_string = bi_string.replace(b"\r", b"")
    bi_split  = bi_string.split(b"\n")
    utf8_string = ""
    for bi_line in bi_split:
        try:
            utf8_string += "%s\r\n" % (bi_line.decode("utf-8"),)
        except:
            pass
    return utf8_string

def md5sum(bytes_data):
    if type(bytes_data) == str:
        bytes_data = bytes_data.encode("utf-8")
    return hashlib.md5(bytes_data).hexdigest()
